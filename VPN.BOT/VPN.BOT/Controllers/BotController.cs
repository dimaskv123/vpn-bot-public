using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Telegram.Bot.Types;
using VPN.BOT.Services.Services;

namespace VPN.BOT.Controllers;

[ApiController]
[Route("api")]
public class BotController : ControllerBase
{

    private HandleUpdateService _handleUpdateService;
    private ILogger<BotController> _logger;

    public BotController(HandleUpdateService handleUpdateService, ILogger<BotController> logger)
    {
        _handleUpdateService = handleUpdateService;
        _logger = logger;
    }

    [HttpPost("update")]
    public async Task<IActionResult> Post(object update)
    {
        try
        {
            await _handleUpdateService.EchoAsync(JsonConvert.DeserializeObject<Update>(update.ToString()));
        }
        catch (Exception e)
        {
            _logger.LogError(e ,"Get error on update");
        }
        return Ok();
    }
}