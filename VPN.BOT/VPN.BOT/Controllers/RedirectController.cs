﻿using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;

namespace VPN.BOT.Controllers;

[ApiController]
[Route("api")]
public class RedirectController : ControllerBase
{
    private IUserRepository _userRepository;
    
    public RedirectController(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }
    
    [HttpGet("app/{id}")]
    public async Task<IActionResult> ToApp(int id)
    {
        var user = await _userRepository.FindByTelegramIdAsync(id);

        var config = user.Configs.FirstOrDefault(x => x.IsValid);

        if (config == null)
        {
            return BadRequest();
        }

        var rename = config.VpnKey.Replace("/?outline=1", "#KrotVPN");
        
        return Redirect(rename);
    }
    
    [HttpGet("download")]
    public async Task<IActionResult> Download()
    {
        var ua = Request.Headers["User-Agent"].First();

        var platform = Platform.Other;

        if (ua.Contains("Android"))
            platform = Platform.Android;
        
        else if (ua.Contains("iPad"))
            platform = Platform.IOS;

        else if (ua.Contains("iPhone"))
            platform = Platform.IOS;

        else if (ua.Contains("Mac OS"))
            platform = Platform.MacOS;

        else if (ua.Contains("Windows"))
            platform = Platform.Windows;

        IActionResult result = platform switch
        {
            Platform.IOS => Redirect("https://itunes.apple.com/us/app/outline-app/id1356177741"),
            Platform.Android => Redirect("https://play.google.com/store/apps/details?id=org.outline.android.client"),
            Platform.Windows => Redirect("https://s3.amazonaws.com/outline-releases/client/windows/stable/Outline-Client.exe"),
            Platform.MacOS =>  Redirect("https://itunes.apple.com/us/app/outline-app/id1356178125"),
            Platform.Other => Redirect("https://getoutline.org"),
            _ => throw new ArgumentOutOfRangeException()
        };
        
        return result;
    }
    
}