﻿using System.Collections.Concurrent;
using Domain.Entity;
using Repository.Interfaces;
using VPN.BOT.Services.ExternalService.Model;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.BackGroundServices;

public class ConfigGeneratorBackgroundService : BaseBackgroundService
{
    private readonly IServiceProvider _serviceScopeFactory;
    private readonly ILogger<ConfigGeneratorBackgroundService> _logger;
    private IServiceProvider _scope;

    private const int MinFreeConfigsCount = 50;
    private const int MinPremiumConfigsCount = 20;

    public ConfigGeneratorBackgroundService(IServiceProvider serviceScopeFactory,
        ILogger<ConfigGeneratorBackgroundService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                _logger.LogWarning("Starting pool");
               await using var scope = _serviceScopeFactory.CreateAsyncScope();
                
                var serverRepository = scope.ServiceProvider.GetRequiredService<IServerRepository>();

                List<Server> servers =
                    (await serverRepository.FindIncludeAllValidConfigsAsync(s => s.EndDate == null, false))
                    .ToList();

                IEnumerable<IGrouping<bool, Server>> groupedServers = servers.GroupBy(s => s.IsPremiumServer).ToList();

                List<Server> premiumServers =
                    groupedServers.FirstOrDefault(g => g.Key == true)?.ToList() ?? new List<Server>();
                List<Server> freeServers =
                    groupedServers.FirstOrDefault(g => g.Key == false)?.ToList() ?? new List<Server>();

                ConcurrentDictionary<Server, int> freeServersConfigsCount =
                    new ConcurrentDictionary<Server, int>(freeServers.ToDictionary(server => server,
                        server => server.Configs.Count + server.PoolConfigs.Count));

                ConcurrentDictionary<Server, int> premiumServersConfigsCount =
                    new ConcurrentDictionary<Server, int>(premiumServers.ToDictionary(server => server,
                        server => server.Configs.Count + server.PoolConfigs.Count));

                LinkedList<Task> generateTasks = new LinkedList<Task>();

                int freePoolConfigsAmount = freeServers.Sum(s => s.PoolConfigs.Count);
                if (freePoolConfigsAmount < MinFreeConfigsCount)
                {
                    _logger.LogWarning("Starting free keys generation");
                    generateTasks.AddLast(
                        GenerateKeysAsync(freeServersConfigsCount, MinFreeConfigsCount, stoppingToken));
                }

                int premiumPoolConfigsAmount = premiumServers.Sum(s => s.PoolConfigs.Count);
                if (premiumPoolConfigsAmount < MinPremiumConfigsCount)
                {
                    _logger.LogWarning("Starting premium keys generation");
                    generateTasks.AddLast(GenerateKeysAsync(premiumServersConfigsCount, MinPremiumConfigsCount,
                        stoppingToken));
                }

                Task.WaitAll(generateTasks.ToArray(), stoppingToken);
            }
            catch (AggregateException e)
            {
                _logger.LogError(e, "Error while key generation");
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Unexpected error");
            }
            finally
            {
                await Task.Delay(TimeSpan.FromHours(1));
            }
        }
    }

    private async Task GenerateKeysAsync(ConcurrentDictionary<Server, int> serversKeysCount, int generateAmount,
        CancellationToken cancellationToken)
    {
        var scope = _serviceScopeFactory.CreateScope();
        IVpnServerConnector vpnServerConnector = scope.ServiceProvider.GetRequiredService<IVpnServerConnector>();

        SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1);
        try
        {
            await Parallel.ForEachAsync(Enumerable.Range(0, generateAmount), cancellationToken,
                async (index, token) =>
                {
                    var scope = _serviceScopeFactory.CreateScope();
                    var poolCertificateService =
                        scope.ServiceProvider.GetRequiredService<IPoolCertificateService>();

                    await semaphoreSlim.WaitAsync(token);

                    Server serverWithMinKeys = serversKeysCount.MinBy(entry => entry.Value).Key;
                    int currentCount = serversKeysCount[serverWithMinKeys];
                    serversKeysCount.TryUpdate(serverWithMinKeys, currentCount + 1, currentCount);

                    semaphoreSlim.Release();

                    OutlineUserModel? outlineKey = null;
                    try
                    {
                        outlineKey =
                            await vpnServerConnector.CreateCertificateAsync(serverWithMinKeys);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, $"Something went wrong with key creation on {serverWithMinKeys.Country}");
                        return;
                    }

                    PoolConfig poolConfig = new PoolConfig()
                    {
                        AccessUrl = outlineKey.AccessUrl,
                        CreateDate = DateTimeOffset.UtcNow,
                        OutlineUserId = outlineKey.Id,
                        ServerId = serverWithMinKeys.Id,
                        UserConfigId = null
                    };

                    await poolCertificateService.AddAsync(poolConfig);
                    await poolCertificateService.SaveAsync();
                });
        }
        catch (Exception e)
        {
            _logger.LogError(e, $"Unexpected error");
            return;
        }

        _logger.LogWarning("Keys were generated");
    }
}