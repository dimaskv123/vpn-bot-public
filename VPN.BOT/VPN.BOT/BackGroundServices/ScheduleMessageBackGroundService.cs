﻿using Repository.Interfaces;
using Telegram.Bot;
using VPN.BOT.Services.Services;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.BackGroundServices;

public class ScheduleMessageBackGroundService : BaseBackgroundService
{
    private readonly IServiceProvider _serviceScopeFactory;
    private readonly ILogger<RevokeCertificateBackGroundService> _logger;
    private readonly IConfiguration _configuration;

    public ScheduleMessageBackGroundService(IServiceProvider serviceScopeFactory,
        ILogger<RevokeCertificateBackGroundService> logger, IConfiguration configuration)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _logger = logger;
        _configuration = configuration;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await Task.Delay(5_000);

        try
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var scope = _serviceScopeFactory.CreateScope();

                var botClient = scope.ServiceProvider.GetRequiredService<ITelegramBotClient>();
                var userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();
                var messageRepository = scope.ServiceProvider.GetRequiredService<IScheduleMessageRepository>();
                var adminNotifyService = scope.ServiceProvider.GetRequiredService<IAdminNotifyService>();
                var handleUpdateService = scope.ServiceProvider.GetRequiredService<HandleUpdateService>();

                var messages =
                    (await messageRepository.Find(x => x.IsSent == false && x.PlannedTimeSend < DateTimeOffset.Now)).ToList();

                if (messages.Any())
                {
                    var users = (await userRepository.GetAllAsync()).ToList();
                    var validUsers = users.Where(u => u.EndDate == null);

                    foreach (var message in messages)
                    {
                        if (message.ToUserId is null)
                        {
                            foreach (var user in validUsers)
                            {
                                try
                                {
                                    if (message.UpdateKeyBoard)
                                    {
                                        await handleUpdateService.UpdateReplyKeyboardMarkup(user);
                                    }

                                    await botClient.SendTextMessageAsync(chatId: user.TelegramId,
                                        text: message.TextMessage, cancellationToken: stoppingToken);
                                }
                                catch (Exception ex)
                                {
                                    _logger.LogError(ex, "Error while send schedule message");
                                }

                                await Task.Delay(125);
                            }
                        }
                        else
                        {
                            try
                            {
                                var user = validUsers.FirstOrDefault(x => x.Id == message.ToUserId);

                                if (message.UpdateKeyBoard)
                                {
                                    await handleUpdateService.UpdateReplyKeyboardMarkup(user);
                                }

                                await botClient.SendTextMessageAsync(chatId: user.TelegramId,
                                    text: message.TextMessage, cancellationToken: stoppingToken);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message != "Bad Request: chat not found")
                                {
                                    await adminNotifyService.NotifyAllAdmins(ex.Message);
                                }
                            }
                        }

                        message.IsSent = true;
                    }

                    await messageRepository.SaveAsync();
                }

                await Task.Delay(TimeSpan.FromMinutes(10), stoppingToken);
            }
        }
        catch (Exception ex)
        {
            _logger.LogCritical(ex, nameof(ScheduleMessageBackGroundService));
        }
    }
}
