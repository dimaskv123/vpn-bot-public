﻿using Domain.Entity;
using Repository.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.BackGroundServices
{
    public class TransitExpiredServerKeysBackGroundService : BaseBackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<TransitExpiredServerKeysBackGroundService> _logger;

        public TransitExpiredServerKeysBackGroundService
            (
            IServiceProvider serviceProvider,
            ILogger<TransitExpiredServerKeysBackGroundService> logger
            )
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogCritical($"{nameof(TransitExpiredServerKeysBackGroundService)} has been started!");

            while (!stoppingToken.IsCancellationRequested)
            {
                var scope = _serviceProvider.CreateScope();
                var sysSettingService = scope.ServiceProvider.GetRequiredService<ISysSettingService>();
                var serverRepository = scope.ServiceProvider.GetRequiredService<IServerRepository>();
                var certificateManager = scope.ServiceProvider.GetRequiredService<ICertificateManger>();
                var messageService = scope.ServiceProvider.GetRequiredService<IMessageService>();
                var botClient = scope.ServiceProvider.GetRequiredService<ITelegramBotClient>();

                _logger.LogInformation($"{nameof(TransitExpiredServerKeysBackGroundService)} check transit setting!");
                try
                {
                    var setting = await sysSettingService.GetSettingAsync(SysSettingName.TransitExpiredServerKeys);

                    if (!setting.Enabled)
                    {
                        await Task.Delay(TimeSpan.FromMinutes(5), stoppingToken);
                        continue;
                    }

                    _logger.LogCritical($"{nameof(TransitExpiredServerKeysBackGroundService)} check transit is enabled!");

                    var servers = (await serverRepository.Find(s => s.EndDate - TimeSpan.FromDays(1) < DateTimeOffset.Now));
                    foreach (var server in servers)
                    {
                        var configs = await certificateManager.FindCertificatesAsync(cfg => cfg.ServerId == server.Id && cfg.IsValid);
                        var successTransitConfigs = 0;
                        var configsCount = configs.Count();
                        if (configsCount < 1)
                        {
                            continue;
                        }

                        _logger.LogCritical($"{nameof(TransitExpiredServerKeysBackGroundService)} transiting {configsCount} amount from {server.Ip}!");

                        foreach (var config in configs)
                        {
                            try
                            {
                                var newKey = await certificateManager.UpdateConfigAsync(config, findBestServer: true);
                                await messageService.SendMessageAsync(MessageName.TransitKeyFromEndServer, config.User);

                                var messageToPin = await botClient.SendTextMessageAsync(chatId: config.User.TelegramId, text: "`" + newKey + "`",
                                                    parseMode: ParseMode.Markdown);

                                await botClient.PinChatMessageAsync(chatId: config.User.TelegramId, messageToPin.MessageId);
                                successTransitConfigs++;
                            }
                            catch (Exception e)
                            {
                                _logger.LogCritical($"Key - {config.Id} `{config.VpnKey}` has not changed! {e.Message}");
                            }

                            _logger.LogCritical($"{nameof(TransitExpiredServerKeysBackGroundService)} has transited {successTransitConfigs} of {configsCount} amount from {server.Ip}!");
                        }
                    }

                }
                catch (Exception e)
                {
                    _logger.LogCritical(e.Message);
                }

                await Task.Delay(TimeSpan.FromMinutes(5), stoppingToken);
            }
        }
    }
}
