﻿using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.BackGroundServices;

public class RevokeCertificateBackGroundService : BaseBackgroundService
{
    private readonly IServiceProvider _serviceScopeFactory;
    private readonly ILogger<RevokeCertificateBackGroundService> _logger;

    public RevokeCertificateBackGroundService(IServiceProvider serviceScopeFactory, ILogger<RevokeCertificateBackGroundService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            _logger.LogWarning($"Start {nameof(RevokeCertificateBackGroundService)}");

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var scope = _serviceScopeFactory.CreateScope();
                    var service = scope.ServiceProvider.GetRequiredService<ICertificateManger>();
                    _logger.LogInformation("Start revoke certificates");
                    await service.RevokeAllExpiredCertificate();
                    _logger.LogInformation("Done with revoke certificates");
                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, $"{nameof(RevokeCertificateBackGroundService)} failed");
                }

                await Task.Delay(TimeSpan.FromMinutes(30), stoppingToken);
            }
        }
        catch (Exception ex)
        {
            _logger.LogCritical(ex, nameof(RevokeCertificateBackGroundService));
        }
    }
}