﻿using Repository.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using VPN.BOT.Common;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.ExternalService.Yookassa.Interfaces;
using VPN.BOT.Services.Services;
using VPN.BOT.Common.Constants;
using VPN.BOT.Services.Services.Interfaces;
using VPN.BOT.Services.Services.SubscribeEngine;

namespace VPN.BOT.BackGroundServices;

public class CapturePaymentBackGroundService : BaseBackgroundService
{

    private readonly IServiceProvider _serviceScopeFactory;
    private readonly ILogger<CapturePaymentBackGroundService> _logger;

    public CapturePaymentBackGroundService(IServiceProvider serviceScopeFactory, ILogger<CapturePaymentBackGroundService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            _logger.LogInformation($"Start {nameof(CapturePaymentBackGroundService)}");

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var scope = _serviceScopeFactory.CreateScope();
                    var yookassaClient = scope.ServiceProvider.GetRequiredService<IYookassaClientManager>();
                    var paymentRepository = scope.ServiceProvider.GetRequiredService<IPaymentRepository>();
                    var botClient = scope.ServiceProvider.GetRequiredService<ITelegramBotClient>();
                    var certificateManager = scope.ServiceProvider.GetRequiredService<ICertificateManger>();
                    var adminNotifyService = scope.ServiceProvider.GetRequiredService<IAdminNotifyService>();
                    var userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();
                    var subscribeEngineFactory = scope.ServiceProvider.GetRequiredService<SubscribeEngineFactory>();

                    var payments = await paymentRepository.FindAsync(x => x.Status == PaymentStatus.Pending);

                    foreach (var payment in payments)
                    {
                        var paymentUpdated = await yookassaClient.GetPayment(payment.YookassaPaymentId, payment.PaymentTo);

                        if (paymentUpdated.Status == Yandex.Checkout.V3.PaymentStatus.WaitingForCapture)
                        {
                            paymentUpdated = await yookassaClient.CapturePayment(payment.YookassaPaymentId, payment.PaymentTo);
                        }

                        if (paymentUpdated.Status == Yandex.Checkout.V3.PaymentStatus.Succeeded)
                        {
                            // Сделать revoke всех с сертификатов кроме того, что пришео в пэйменте
                            
                            // если на конфиге уже есть оплаты, то просто докинуть дней
                            // елси нет, то инициализировать конфиг 
                            
                            if (payment.UserConfig.ServerId == 4)
                            {
                                
                            }

                            if (payment.UserConfig.Payments.Count > 1)
                            {
                                await certificateManager.UpdateCertificateAfterSuccessfulPayment(payment.UserConfig, payment.DurationInDays);
                                await botClient.SendTextMessageAsync(chatId: payment.UserConfig.User.TelegramId, text: string.Format(MessagesText.PaymentCompleteRepeat, payment.UserConfig.EndDate.ToString("dd.MM.yyyy")), parseMode: ParseMode.Markdown, cancellationToken: stoppingToken);
                            }
                            else
                            {
                                foreach (var config in payment.UserConfig.User.Configs)
                                {
                                    if (config.IsValid)
                                    {
                                        await certificateManager.RevokeUserCertificateAsync(config);
                                    }
                                }
                                
                                await certificateManager.UpdateCertificateAfterSuccessfulPayment(payment.UserConfig);
                                var messageToPin = await botClient.SendTextMessageAsync(chatId: payment.UserConfig.User.TelegramId, text: "`" + payment.UserConfig.VpnKey + "`", parseMode: ParseMode.Markdown);
                                await botClient.PinChatMessageAsync(chatId: payment.UserConfig.User.TelegramId, messageToPin.MessageId);
                                
                                InlineKeyboardMarkup markUp;
                                
                                var fastConnect = new InlineKeyboardButton("Подключиться 🚀")
                                {
                                    Url = CertificateManger.GetEncodedInviteUrl(payment.UserConfig.VpnKey) 
                                };
                                
                                var button = new[] { fastConnect };

                                var buttons = new[] { button };

                                markUp = new InlineKeyboardMarkup(buttons);
                            
                                await botClient.SendTextMessageAsync(chatId: payment.UserConfig.User.TelegramId, text: string.Format(MessagesText.PaymentComplete, payment.UserConfig.EndDate.ToString("dd.MM.yyyy")), parseMode: ParseMode.Markdown, replyMarkup: markUp, cancellationToken: stoppingToken);
                            }

                            var subscribeEngine = subscribeEngineFactory.Create(payment.UserConfig.User.UserSettings.SubscribeTheoryMethod);

                            if (subscribeEngine != null)
                            {
                                await subscribeEngine.UnbanUser(payment.UserConfig.User, false);
                            }
                            
                            //validConfig.EndDate = validConfig.EndDate.AddDays(payment.DurationInDays);

                            payment.UserConfig.User.UserSettings.SubscribeTheoryMethod =
                                SubscribeTheoryMethod.HasActiveSubscription;
                            
                            await userRepository.UpdateAsync(payment.UserConfig.User);
                            try
                            {
                                var userName = string.IsNullOrEmpty(payment?.UserConfig?.User?.UserName)
                                    ? string.Empty
                                    : "@" + payment?.UserConfig?.User?.UserName;
                                await adminNotifyService.NotifyAllAdmins($"Поступила оплата на {paymentUpdated.Amount.Value} р. от {userName} {payment?.UserConfig?.User?.FirstName}  {payment?.UserConfig?.User?.LastName}");
                            }
                            catch (Exception e)
                            {
                            }
                        }

                        if (paymentUpdated.Status == Yandex.Checkout.V3.PaymentStatus.Canceled)
                        {
                            if (!payment.UserConfig.IsValid)
                            {
                                await certificateManager.UpdateCertificateAfterFailedPayment(payment.UserConfig);
                            }
                        }

                        var newStatus = MapStatus(paymentUpdated.Status);
                        
                        if (payment.Status != newStatus)
                        {
                            payment.Status = newStatus;
                            payment.UpdatedAt = DateTimeOffset.Now;
                        }

                        await paymentRepository.SaveAsync();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, $"{nameof(CapturePaymentBackGroundService)} failed");
                }

                await Task.Delay(TimeSpan.FromSeconds(5), stoppingToken);
            }
        }
        catch (Exception ex)
        {
            _logger.LogCritical(ex, nameof(CapturePaymentBackGroundService));
        }
    }

    private PaymentStatus MapStatus(Yandex.Checkout.V3.PaymentStatus status)
    {
        return status switch
        {
            Yandex.Checkout.V3.PaymentStatus.Canceled => PaymentStatus.Canceled,
            Yandex.Checkout.V3.PaymentStatus.Pending => PaymentStatus.Pending,
            Yandex.Checkout.V3.PaymentStatus.Succeeded => PaymentStatus.Succeeded,
            Yandex.Checkout.V3.PaymentStatus.WaitingForCapture => PaymentStatus.WaitingForCapture,
            _ => throw new ArgumentOutOfRangeException(nameof(status), status, null)
        };
    }
}