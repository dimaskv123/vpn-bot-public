﻿namespace VPN.BOT.BackGroundServices;

public class BaseBackgroundService : BackgroundService
{
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await Task.Delay(TimeSpan.FromSeconds(20));

        await StartAsync(stoppingToken);
    }
}