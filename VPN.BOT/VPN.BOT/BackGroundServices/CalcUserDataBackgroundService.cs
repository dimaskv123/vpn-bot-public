﻿using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.BackGroundServices
{
    public class CalcUserDataBackgroundService : BaseBackgroundService
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<CalcUserDataBackgroundService> _logger;
        private readonly IConfiguration _configuration;

        public CalcUserDataBackgroundService
            (
                IServiceProvider serviceProvider,
                ILogger<CalcUserDataBackgroundService> logger,
                IConfiguration configuration
            )
        {
            _services = serviceProvider;
            _logger = logger;
            _configuration = configuration;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _logger.LogCritical($"Время Московское: {DateTimeOffset.UtcNow.AddHours(3)}");
                _logger.LogInformation($"Start {nameof(CalcUserDataBackgroundService)}");


                //var connectionString = _configuration["ConnectionStrings:PostgreDb"];


                // Set the path to the backup file
                //var backupFilePath = $"/postgrebackups/mydatabase{DateTimeOffset.UtcNow.Date.ToString("dd.MM.yyyy")}.sql";

                //_logger.LogCritical($"Погнали нахуй бекап ебана роt");

                //try
                //{
                //    // Connect to the PostgreSQL database
                //    using var connection = new NpgsqlConnection(connectionString);
                //    connection.Open();

                //    // Execute the pg_dump command to create a backup of the database
                //    var command = new NpgsqlCommand($"pg_dump -U root dpn_db {backupFilePath}", connection);
                //    awawit command.ExecuteNonQueryAsync();

                //}
                //catch (Exception e)
                //{
                //    _logger.LogCritical(e.StackTrace);

                //    // throw;
                //}


                //_logger.LogCritical($"Птичка пролетела");

                //_logger.LogCritical($"Стартую калк процесс");

                while (!stoppingToken.IsCancellationRequested)
                {
                    var scope = _services.CreateScope();
                    var service = scope.ServiceProvider.GetRequiredService<ICertificateManger>();
                    try
                    {
                        _logger.LogInformation($"{nameof(CalcUserDataBackgroundService)} strted!!!!");

                        await service.UpdateUsersDataInfoAsync(stoppingToken);

                    }
                    catch (Exception e)
                    {
                        _logger.LogInformation(e, $"{nameof(CalcUserDataBackgroundService)} failed");
                    }
                    finally
                    {
                        scope.Dispose();
                        _logger.LogInformation($"{nameof(CalcUserDataBackgroundService)} disposed!");
                    }


                    await Task.Delay(TimeSpan.FromMinutes(20), stoppingToken);
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, nameof(CalcUserDataBackgroundService));
            }
        }
    }
}
