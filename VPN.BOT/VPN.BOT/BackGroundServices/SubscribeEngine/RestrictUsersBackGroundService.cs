﻿using VPN.BOT.Services.Services.Interfaces;
using VPN.BOT.Services.Services.SubscribeEngine.Interfaces;

namespace VPN.BOT.BackGroundServices.SubscribeEngine;

public class RestrictUsersBackGroundService : BackgroundService
{

    private readonly IServiceProvider _serviceScopeFactory;
    private readonly ILogger<RestrictUsersBackGroundService> _logger;

    public RestrictUsersBackGroundService(IServiceProvider serviceScopeFactory, ILogger<RestrictUsersBackGroundService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            await Task.Delay(TimeSpan.FromSeconds(30), stoppingToken);
            _logger.LogInformation($"Start {nameof(RestrictUsersBackGroundService)}");

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var scope = _serviceScopeFactory.CreateScope();
                    var subscribeEngines = scope.ServiceProvider.GetRequiredService<IEnumerable<ISubscribeEngine>>();

                    _logger.LogInformation("Start restrioctions");
                    foreach (var subscribeEngine in subscribeEngines)
                    {
                        await subscribeEngine.Init();
                        await subscribeEngine.RestrictUsers();
                    }
                    _logger.LogInformation("Done with restrioctions");

                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, $"{nameof(RestrictUsersBackGroundService)} failed");
                }

                await Task.Delay(TimeSpan.FromMinutes(60), stoppingToken);
            }
        }
        catch (Exception ex)
        {
            _logger.LogCritical(ex, nameof(RestrictUsersBackGroundService));
        }
    }
}