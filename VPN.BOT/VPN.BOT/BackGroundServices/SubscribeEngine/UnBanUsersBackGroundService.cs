﻿using VPN.BOT.Services.Services.SubscribeEngine.Interfaces;

namespace VPN.BOT.BackGroundServices.SubscribeEngine;

public class UnBanUsersBackGroundService : BackgroundService
{
    private readonly IServiceProvider _serviceScopeFactory;
    private readonly ILogger<UnBanUsersBackGroundService> _logger;

    public UnBanUsersBackGroundService(IServiceProvider serviceScopeFactory, ILogger<UnBanUsersBackGroundService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            _logger.LogInformation($"Start {nameof(UnBanUsersBackGroundService)}");

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var scope = _serviceScopeFactory.CreateScope();
                    var subscribeEngines = scope.ServiceProvider.GetRequiredService<IEnumerable<ISubscribeEngine>>();

                    _logger.LogInformation("Start unban");
                    foreach (var subscribeEngine in subscribeEngines)
                    {
                        await subscribeEngine.Init();
                        await subscribeEngine.UnbanUsers();
                    }
                    _logger.LogInformation("Done with unban");
                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, $"{nameof(UnBanUsersBackGroundService)} failed");
                }

                await Task.Delay(TimeSpan.FromMinutes(30), stoppingToken);
            }
        }
        catch (Exception ex)
        {
            _logger.LogCritical(ex, nameof(UnBanUsersBackGroundService));
        }
    }
}