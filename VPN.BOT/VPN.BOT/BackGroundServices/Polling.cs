using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using VPN.BOT.Services.ExternalService.Model;
using VPN.BOT.Services.Services;

namespace VPN.BOT.BackGroundServices;

public class PoolingService : BackgroundService 
{
    private readonly ILogger<PoolingService> _logger;
    private readonly IServiceProvider _services;
    
    public PoolingService(ILogger<PoolingService> logger,
        IServiceProvider serviceProvider)
    {
        _logger = logger;
        _services = serviceProvider;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await Task.Delay(TimeSpan.FromSeconds(5));

        while (!stoppingToken.IsCancellationRequested)
        {
            var botClient = _services.GetRequiredService<ITelegramBotClient>();

            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { } // receive all update types
            };
            var updateReceiver = new QueuedUpdateReceiver(botClient, receiverOptions);
            
            try
            {
                await Parallel.ForEachAsync(
                    updateReceiver,
                    new ParallelOptions()
                    {
                        MaxDegreeOfParallelism = 10
                    },
                    async (source, token) =>
                    {
                        await using var scope = _services.CreateAsyncScope();
                        
                        var handleService = scope.ServiceProvider.GetRequiredService<HandleUpdateService>();
                        await handleService.EchoAsync(source);
                    });
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception, "Polling is down");
            }

            await Task.Delay(TimeSpan.FromSeconds(5));
        }
    }
}