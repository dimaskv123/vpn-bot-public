﻿using VPN.BOT.Services.Services.OnTriggerSendMessageEngine;

namespace VPN.BOT.BackGroundServices;

public class TriggerBackgroundService : BaseBackgroundService
{
    private readonly IServiceProvider _serviceScopeFactory;
    private readonly ILogger<RevokeCertificateBackGroundService> _logger;

    public TriggerBackgroundService(IServiceProvider serviceScopeFactory, ILogger<RevokeCertificateBackGroundService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await Task.Delay(5_000);

        _logger.LogInformation($"Start {nameof(TriggerBackgroundService)}");

        try
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var scope = _serviceScopeFactory.CreateScope();

                    var triggerEngines = scope.ServiceProvider.GetRequiredService<IEnumerable<TriggerEngine>>();

                    foreach (var triggerEngine in triggerEngines)
                    {
                        await triggerEngine.CheckTriggerCondition();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                await Task.Delay(TimeSpan.FromMinutes(10));

            }
        }
        catch (Exception ex)
        {
            _logger.LogCritical(ex, nameof(TriggerBackgroundService));
        }
    }
}