﻿using DAL.Context;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StackExchange.Redis;
using Telegram.Bot;
using VPN.BOT.BackGroundServices;
using VPN.BOT.BackGroundServices.SubscribeEngine;
using VPN.BOT.Services;

namespace VPN.BOT;

public class Startup
{
    public IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        Configuration["Serilog:LogsFolder"] =
            AppDomain.CurrentDomain.BaseDirectory + Configuration["Serilog:LogsFolder"];

        var botConfig = Configuration.GetSection("TelegramToken").Value;

        services.AddHostedService<ConfigGeneratorBackgroundService>();
        services.AddHostedService<PoolingService>();
        services.AddHostedService<CapturePaymentBackGroundService>();
        services.AddHostedService<RevokeCertificateBackGroundService>();
        services.AddHostedService<ScheduleMessageBackGroundService>();
        services.AddHostedService<CalcUserDataBackgroundService>();
        services.AddHostedService<TransitExpiredServerKeysBackGroundService>();
        services.AddHostedService<RestrictUsersBackGroundService>();
        services.AddHostedService<UnBanUsersBackGroundService>();
        services.AddHostedService<TriggerBackgroundService>();

        services.AddHttpClient("telegram_bot_client")
            .AddTypedClient<ITelegramBotClient>((httpClient, sp) =>
            {
                TelegramBotClientOptions options = new(botConfig);
                return new TelegramBotClient(options, httpClient);
            });

        services.AddDbContext<DataContext>(options =>
        {
            options.UseNpgsql(Configuration["ConnectionStrings:PostgreDb"]);
            options.EnableSensitiveDataLogging();
        }, ServiceLifetime.Transient);

        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

        //services.AddSingleton<IConnectionMultiplexer>(
        // ConnectionMultiplexer.Connect(Configuration.GetConnectionString("RedisConnection")));
        // services.AddScoped<IDatabase>(provider => provider.GetRequiredService<IConnectionMultiplexer>().GetDatabase());


        services.AddServices(Configuration);

        services.AddLogging();

        services.AddControllers();

        services.AddHealthChecks();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
        IHostApplicationLifetime applicationLifetime)
    {
        app.UseHealthChecks("/healthCheck", new HealthCheckOptions
        {
            ResponseWriter = async (context, report) =>
            {
                await context.Response.WriteAsync(
                    JsonConvert.SerializeObject(
                        new
                        {
                            result = "vpn.bot is running"
                        }));
            }
        });

        app.UseRouting();
        app.UseCors();

        #region Migrate

        using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
        {
            using (var context = serviceScope.ServiceProvider.GetService<DataContext>())
            {
                context?.Database.Migrate();
            }
        }

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

        #endregion
    }
}