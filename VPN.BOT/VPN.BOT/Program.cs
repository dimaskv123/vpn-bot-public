using Serilog;
using VPN.BOT;
using VPN.BOT.LoggerConfiguration;

public class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); })
            .ConfigureLogging(logging => { logging.ClearProviders(); })
            .UseSerilog(VpbBotLoggerConfig.ConfigureLogging);
}