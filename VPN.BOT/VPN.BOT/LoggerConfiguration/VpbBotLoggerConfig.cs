﻿using Serilog;
using Serilog.Exceptions;
using Serilog.Filters;
using Serilog.Sinks.TelegramBot;

namespace VPN.BOT.LoggerConfiguration;

public class VpbBotLoggerConfig
{
    public static void ConfigureLogging(HostBuilderContext ctx, Serilog.LoggerConfiguration config)
    {
        var environment = ctx.HostingEnvironment;
        var configuration = ctx.Configuration;
        var mongoConnectionString = ctx.Configuration.GetConnectionString("MongoDb");

        config
            .Enrich.FromLogContext()
            .Enrich.WithExceptionDetails();

        config
            .WriteTo.Console()
            .Filter.ByExcluding(Matching.FromSource("System.Net.Http"))
            .WriteTo.Async(a => a.Logger(l => l.MinimumLevel.Information().WriteTo.MongoDB(mongoConnectionString, "Logs")))
            .WriteTo.Async(a => a.Logger(l =>
            {
                l.Filter.ByExcluding(logEvent =>
                {
                    if ((logEvent.Properties.TryGetValue("SourceContext", out var name)))
                    {
                        return name.ToString()
                            .Contains("telegram_bot_client");
                    }

                    return false;
                });

                l.MinimumLevel.Warning().WriteTo
                    .TelegramBot("5882475618:AAEBYnzgQ2ejwbQ10PWHBYSquhvuPN5SSqg", "6261544653");
                l.MinimumLevel.Warning().WriteTo
                    .TelegramBot("5882475618:AAEBYnzgQ2ejwbQ10PWHBYSquhvuPN5SSqg", "254724042");
            }));



        config
        .ReadFrom.Configuration(configuration);
    }

    private static string Format(string value)
    {
        if (string.IsNullOrWhiteSpace(value))
        {
            throw new ArgumentNullException(value);
        }

        return value.ToLower().Replace(".", "-");
    }
}