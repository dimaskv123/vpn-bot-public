﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class UserBlogEntity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserBlogs",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "integer", nullable: false),
                    InstagramLogin = table.Column<string>(type: "text", nullable: true),
                    IsLoginConfidence = table.Column<bool>(type: "boolean", nullable: false),
                    InstagramSubscribers = table.Column<int>(type: "integer", nullable: false),
                    TgChannel = table.Column<string>(type: "text", nullable: true),
                    TgChannelSubscribers = table.Column<int>(type: "integer", nullable: false),
                    LastAdDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBlogs", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_UserBlogs_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserBlogs");
        }
    }
}
