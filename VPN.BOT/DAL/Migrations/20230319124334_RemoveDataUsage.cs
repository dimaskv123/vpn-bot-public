﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class RemoveDataUsage : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataUsageHistory");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DataUsageHistory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserConfigId = table.Column<int>(type: "integer", nullable: false),
                    DataDifference = table.Column<double>(type: "double precision", nullable: false),
                    TodayGigabytes = table.Column<double>(type: "double precision", nullable: false),
                    TotalGigabytes = table.Column<double>(type: "double precision", nullable: false),
                    UpdateTime = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataUsageHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DataUsageHistory_UserConfigs_UserConfigId",
                        column: x => x.UserConfigId,
                        principalTable: "UserConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DataUsageHistory_UserConfigId",
                table: "DataUsageHistory",
                column: "UserConfigId");
        }
    }
}
