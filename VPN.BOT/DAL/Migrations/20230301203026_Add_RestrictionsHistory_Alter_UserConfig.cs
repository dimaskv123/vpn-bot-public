﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddRestrictionsHistoryAlterUserConfig : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Users_PayerId",
                table: "Payments");

            migrationBuilder.RenameColumn(
                name: "PayerId",
                table: "Payments",
                newName: "UserConfigId");

            migrationBuilder.RenameIndex(
                name: "IX_Payments_PayerId",
                table: "Payments",
                newName: "IX_Payments_UserConfigId");

            migrationBuilder.CreateTable(
                name: "RestrictionsHistory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserConfigId = table.Column<int>(type: "integer", nullable: false),
                    RestrictionType = table.Column<int>(type: "integer", nullable: false),
                    StartTime = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    EndTime = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RestrictionsHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RestrictionsHistory_UserConfigs_UserConfigId",
                        column: x => x.UserConfigId,
                        principalTable: "UserConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RestrictionsHistory_UserConfigId",
                table: "RestrictionsHistory",
                column: "UserConfigId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_UserConfigs_UserConfigId",
                table: "Payments",
                column: "UserConfigId",
                principalTable: "UserConfigs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_UserConfigs_UserConfigId",
                table: "Payments");

            migrationBuilder.DropTable(
                name: "RestrictionsHistory");

            migrationBuilder.RenameColumn(
                name: "UserConfigId",
                table: "Payments",
                newName: "PayerId");

            migrationBuilder.RenameIndex(
                name: "IX_Payments_UserConfigId",
                table: "Payments",
                newName: "IX_Payments_PayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Users_PayerId",
                table: "Payments",
                column: "PayerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
