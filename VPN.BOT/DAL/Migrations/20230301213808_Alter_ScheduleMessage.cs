﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class AlterScheduleMessage : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ToUserId",
                table: "ScheduleMessages",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleMessages_ToUserId",
                table: "ScheduleMessages",
                column: "ToUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleMessages_Users_ToUserId",
                table: "ScheduleMessages",
                column: "ToUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleMessages_Users_ToUserId",
                table: "ScheduleMessages");

            migrationBuilder.DropIndex(
                name: "IX_ScheduleMessages_ToUserId",
                table: "ScheduleMessages");

            migrationBuilder.DropColumn(
                name: "ToUserId",
                table: "ScheduleMessages");
        }
    }
}
