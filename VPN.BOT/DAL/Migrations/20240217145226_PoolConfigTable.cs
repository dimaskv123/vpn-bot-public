﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class PoolConfigTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PoolConfigs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AccessUrl = table.Column<string>(type: "text", nullable: false),
                    OutlineUserId = table.Column<string>(type: "text", nullable: false),
                    CreateDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    ServerId = table.Column<int>(type: "integer", nullable: false),
                    UserConfigId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PoolConfigs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PoolConfigs_Servers_ServerId",
                        column: x => x.ServerId,
                        principalTable: "Servers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PoolConfigs_UserConfigs_UserConfigId",
                        column: x => x.UserConfigId,
                        principalTable: "UserConfigs",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_PoolConfigs_AccessUrl_UserConfigId",
                table: "PoolConfigs",
                columns: new[] { "AccessUrl", "UserConfigId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PoolConfigs_ServerId",
                table: "PoolConfigs",
                column: "ServerId");

            migrationBuilder.CreateIndex(
                name: "IX_PoolConfigs_UserConfigId",
                table: "PoolConfigs",
                column: "UserConfigId",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PoolConfigs");
        }
    }
}
