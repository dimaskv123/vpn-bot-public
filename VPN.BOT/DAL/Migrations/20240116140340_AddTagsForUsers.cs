﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddTagsForUsers : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("""
                                 WITH "UsersWithExpensiveTag" as (
                                 select distinct "UserId"
                                 from "Tags" t
                                 where t."Name" = 'expensive.subscription'
                                 )
                                 
                                 insert into "Tags"
                                 select uc."UserId", 'expensive.subscription', now() from "UserConfigs" uc
                                 join "Servers" s on s."Id" = uc."ServerId"
                                 left join "UsersWithExpensiveTag" ut on ut."UserId" = uc."UserId"
                                 where s."IsPremiumServer" = true and uc."IsValid" = true and ut."UserId" is null
                                 order by uc."EndDate"
                                 limit 300
                                 """);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
