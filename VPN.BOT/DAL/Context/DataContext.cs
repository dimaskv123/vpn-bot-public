﻿using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using VPN.BOT.Common.Constants;
using VPN.BOT.Common.Enums;

namespace DAL.Context
{
    public class DataContext : DbContext
    {
        public DataContext() { }
        public DataContext(DbContextOptions<DataContext> options)
        : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne<Referal>(r => r.Invite)
                .WithOne(r => r.CreatedBy)
                .HasForeignKey<Referal>(r => r.CreatedById);

            modelBuilder.Entity<Payment>()
                .HasOne(p => p.UserConfig)
                .WithMany(r => r.Payments)
                .HasForeignKey(x => x.UserConfigId);

            modelBuilder.Entity<UserConfig>()
                .HasOne(us => us.User)
                .WithMany(u => u.Configs)
                .HasForeignKey(us => us.UserId);

            modelBuilder.Entity<UserConfig>()
                .HasOne(us => us.Server)
                .WithMany(s => s.Configs)
                .HasForeignKey(us => us.ServerId);
            
            modelBuilder.Entity<UserConfig>()
                .HasOne(us => us.PoolConfig)
                .WithOne(pc => pc.UserConfig)
                .HasForeignKey<PoolConfig>(us => us.UserConfigId);

            modelBuilder.Entity<RestrictionsHistory>()
                .HasOne(p => p.UserConfig)
                .WithMany(r => r.RestrictionsHistories)
                .HasForeignKey(x => x.UserConfigId);

            modelBuilder.Entity<ScheduleMessage>()
                .HasOne(p => p.User)
                .WithMany(r => r.ScheduleMessages)
                .HasForeignKey(x => x.ToUserId);

            modelBuilder.Entity<TriggerMessage>()
                .HasOne(p => p.User)
                .WithMany(r => r.TriggerMessages)
                .HasForeignKey(x => x.UserId);

            modelBuilder.Entity<User>()
                .HasOne(u => u.UserSettings)
                .WithOne(us => us.User)
                .HasForeignKey<UserSettings>(us => us.UserId);

            modelBuilder.Entity<UserSettings>()
                .HasKey(x => x.UserId);

            modelBuilder.Entity<UserSettings>()
                .Property(x => x.PersonalTodayLimit)
                .HasDefaultValue(Constants.LimitConstants.TODAY_GIGABYTES);

            modelBuilder.Entity<SysSetting>()
                .HasIndex(u => u.Name)
                .IsUnique();
            
            modelBuilder.Entity<UserBlog>()
                .HasKey(x => x.UserId);

            modelBuilder.Entity<Payment>()
                .Property(p => p.PaymentTo)
                .HasDefaultValue(PaymentTo.Dima);
            
            modelBuilder.Entity<Tag>().HasKey(x => new { x.UserId, x.Name });
            
            modelBuilder.Entity<User>()
                .HasMany<Tag>(t => t.Tags)
                .WithOne(u => u.User)
                .HasForeignKey(t => t.UserId);

            modelBuilder.Entity<PoolConfig>()
                .HasOne(pc => pc.UserConfig)
                .WithOne(a => a.PoolConfig)
                .HasForeignKey<PoolConfig>(uc => uc.UserConfigId)
                .IsRequired(false);

            modelBuilder.Entity<PoolConfig>().Property(pc => pc.AccessUrl).IsRequired();
            modelBuilder.Entity<PoolConfig>().Property(pc => pc.CreateDate).IsRequired();
            modelBuilder.Entity<PoolConfig>().Property(pc => pc.OutlineUserId).IsRequired();
            
            modelBuilder.Entity<PoolConfig>().HasIndex(pc => new {pc.AccessUrl, pc.UserConfigId}).IsUnique();

            modelBuilder.Entity<PoolConfig>()
                .HasOne(pc => pc.Server)
                .WithMany(s => s.PoolConfigs)
                .HasForeignKey(pc => pc.ServerId);

        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserConfig> UserConfigs { get; set; }
        public DbSet<Server> Servers { get; set; }
        public DbSet<ScheduleMessage> ScheduleMessages { get; set; }
        public DbSet<Referal> Referals { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<RestrictionsHistory> RestrictionsHistory { get; set; }
        public DbSet<UserSettings> UserSettings { get; set; }
        public DbSet<TriggerMessage> TriggerMessages { get; set; }
        public DbSet<SysSetting> SysSettings { get; set; }
        public DbSet<UserBlog> UserBlogs { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PoolConfig> PoolConfigs { get; set; }
    }
}