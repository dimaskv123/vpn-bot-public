﻿using UserScraper.Services;

namespace UserScraper.Tests
{
    public class TelegramCheckServiceTests
    {
        private readonly TelegramChannelCheckService _telegramChannelCheckService;

        public TelegramCheckServiceTests()
        {
            _telegramChannelCheckService = new TelegramChannelCheckService(null);
        }

        [Theory]
        [InlineData("Нутрициолог/диетолог/провизор https://t.me/oglavnomponyatno", "oglavnomponyatno")]
        [InlineData("dancer \ud83d\udca6 https://t.me/nickxfam call me bambleby", "nickxfam")]
        [InlineData("https://t.me/thewanderingsy", "thewanderingsy")]
        [InlineData("https://t.me/thewanderingsy - это мой тг", "thewanderingsy")]
        public void Bio_Returns_ChannelName(string bio, string expectedChannelName)
        {
            var result = _telegramChannelCheckService.GetTgChannelNameFromString(bio);
            Assert.Equal(expectedChannelName, result);
        }
    }
}
