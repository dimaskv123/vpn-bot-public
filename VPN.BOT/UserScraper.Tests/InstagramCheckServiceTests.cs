﻿using UserScraper.Services;

namespace UserScraper.Tests
{
    public class InstagramCheckServiceTests
    {
        private readonly InstagramCheckService _instagramCheckService;
        public InstagramCheckServiceTests()
        {
            _instagramCheckService = new InstagramCheckService();
        }
       
        [Theory]
        [InlineData("Визуализатор, контент-креатор Мой Инстаграмм @levkinaekaterina_", "levkinaekaterina_")]
        [InlineData("@kravchenko.alina_", "kravchenko.alina_")]
        [InlineData("Instagram: @ps.sophie", "ps.sophie")]
        [InlineData("нельзяграм @sirenfjord", "sirenfjord")]
        [InlineData("Архитектор, дизайнер интерьеров inst: @leuanastasia", "leuanastasia")]
        [InlineData("Блогер, продюсер. Мой инстаграм: https://instagram.com/nutumn?igshid=YmM0MjE2YWMzOA==", "nutumn")]
        [InlineData("@negrimez- Актер, филантроп, обманщик", "negrimez")]
        [InlineData("зацени мой инст - lazaretty", "lazaretty")]
        [InlineData("pro cs player, check my instagram: lehagr0b", "lehagr0b")]
        [InlineData("Консультант по Матрице Судьбы | Прага \ud83d\udda4 inst: irina.kerdman", "irina.kerdman")]
        [InlineData("Веду мероприятия и инвестирую\ud83c\udfa4\ud83d\udcb0Рассказываю про свои достижения и показываю путь к своим целям здесь \ud83d\udc49\ud83c\udffb https://t.me/eldimodima", null)]
        [InlineData(" iOS average enjoyer\ud83d\ude0e\ud83d\ude0e\ud83d\ude0e", null)]
        public void Bio_Returns_UserName(string bio, string? expectedUserName)
        {
            var result =_instagramCheckService.GetUserNameByBio(bio);

            Assert.Equal(expectedUserName, result);
        }
    }
}