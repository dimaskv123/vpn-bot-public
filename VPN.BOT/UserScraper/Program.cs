using Serilog;
using UserScraper.Configurations;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog(LogConfiguration.ConfigureLogging);

var services = builder.Services;
var configuration = builder.Configuration;
services.AddControllers();

services.AddRepositories();
services.AddInternalServices();
services.AddExternalServices(configuration);
services.AddBackgroundServices();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();