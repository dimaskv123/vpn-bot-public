﻿using System.Text.RegularExpressions;
using VPN.BOT.Services.ExternalServices;
using Yandex.Checkout.V3;

namespace UserScraper.Services;

public class InstagramCheckService
{
    private HttpClient _client;


    public InstagramCheckService()
    {
        _client = new HttpClient();
        _client.DefaultRequestHeaders.Add("User-Agent", "PostmanRuntime/7.28.4");
        _client.DefaultRequestHeaders.Add("Accept", "*/*");
        _client.DefaultRequestHeaders.Add("Connection", "keep-alive");
    }

    public async Task<int> GetInstagramFollowersByUserNameAsync(string userName)
    {
        var response = await _client.GetAsync($"https://www.instagram.com/{userName}");

        response.EnsureSuccessStatusCode();

        var content = await response.Content.ReadAsStringAsync();

        var regex = new Regex("\"([^\"]*) Followers");

        var matches = regex.Matches(content);

        if (!matches.Any()) return 0;
        
        var parsed = int.TryParse(matches[0].Groups[1].Value.Replace(",", string.Empty), out var result);

        return parsed ? result : 0;
    }

    public string? GetUserNameByBio(string bio)
    {
        string? userName;
        //1
        string input1 = "Hello there is my instagram - instagram.com/matchvalue/ dsf";
        if (TryGetUserNameFromLink(bio, out userName))
        {
            return userName;
        }


        // 2
        var input2 = "inst: @username! subscribe guys";
        if (TryGetUserNameStraightUp(bio, out userName))
        {
            return userName;
        }

        // 3
        var input3 = "SMM | на связи пн-сб 10:00-20:00 \u2763\ufe0fInst: kmll___".ToLower();
        if (TryGetUserNameByInstSubstring(bio, out userName))
        {
            return userName;
        }

        return null;
    }

    public bool TryGetUserNameFromLink(string bio, out string? userName)
    {
        string pattern = @"instagram\.com/([a-zA-Z0-9_]+)";

        Match match = Regex.Match(bio, pattern);
        
        userName = match.Success ? match.Groups[1].Value : null;

        return match.Success;
    }

    public bool TryGetUserNameStraightUp(string bio, out string? userName)
    {
        string pattern = @"@([a-zA-Z0-9._]+)";

        Match match = Regex.Match(bio, pattern);

        userName = match.Success ? match.Groups[1].Value : null;

        return match.Success;
    }

    public bool TryGetUserNameByInstSubstring(string bio, out string? userName)
    {
        string[] substrings = { "instagram", "insta", "inst", "инстаграм", "инста", "инст" };
        string cutBio = string.Empty;

        if (!TryCutStringIfContainsSubstrings(bio, substrings, out cutBio))
        {
            userName = null;
            return false;
        }

        string pattern = @"([a-zA-Z0-9._]+)";
        Match match = Regex.Match(cutBio, pattern);

        userName = match.Success ? match.Groups[1].Value : null;

        return match.Success;
    }


    private bool TryCutStringIfContainsSubstrings(string input, string[] substrings, out string result)
    {
        result = string.Empty;
        string pattern = string.Join("|", substrings.Select(Regex.Escape));
        Match match = Regex.Match(input, pattern);

        if (match.Success)
        {
            int startIndex = match.Index + match.Length;
            result = input.Substring(startIndex);
            return true;
        }

        return false;
    }
}