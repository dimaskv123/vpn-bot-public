﻿using HtmlAgilityPack;
using System.Text.RegularExpressions;


namespace UserScraper.Services
{
    public class TelegramChannelCheckService
    {
        private readonly ILogger<TelegramChannelCheckService> _logger;
        private HttpClient _httpClient;

        public TelegramChannelCheckService(ILogger<TelegramChannelCheckService> logger)
        {
            _logger = logger;

            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Add("User-Agent", "PostmanRuntime/7.28.4");
            _httpClient.DefaultRequestHeaders.Add("Accept", "*/*");
            _httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
        }

        public string? GetTgChannelNameFromString(string bio)
        {
            string pattern = @"t\.me/[^ ]+";

            Match match = Regex.Match(bio, pattern);


            return match.Success ? match.Value : null;
        }

        public async ValueTask<int> GetTelegramFollowersAmountByLink(string tgChannelLink)
        {
            var response = await _httpClient.GetAsync("https://" + tgChannelLink);

            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(content);

            HtmlNode subscribersNode = doc.DocumentNode.SelectSingleNode("//div[@class='tgme_page_extra']");

            if (subscribersNode == null)
            {
                return 0;
            }

            string subscribersNodeContent = subscribersNode.InnerText;

            string pattern = @"(\d+[\s\d+]*)";

            Match match = Regex.Match(subscribersNodeContent, pattern);

            if (!match.Success)
            {
                _logger.LogCritical($"Cant parse tg channel - {subscribersNodeContent}");
                return 0;
            }

            string subscribersAmountToParse = match.Value.Replace(" ", "");

           if (!int.TryParse(subscribersAmountToParse, out int subscribersAmount))
           {
                _logger.LogCritical($"Cant parse number tg channel - {subscribersAmountToParse}");
           };

           return subscribersAmount;
        }


    }
}
