﻿using Domain.Entity;
using Telegram.Bot;

namespace UserScraper.Services
{
    public class UserBioService
    {
        private readonly InstagramCheckService _instagramCheckService;
        private readonly TelegramChannelCheckService _telegramChannelCheckService;
        private readonly ITelegramBotClient _botClient;
        private readonly ILogger<UserBioService> _logger;

        public UserBioService(
            ITelegramBotClient telegramBotClient,
            InstagramCheckService instagramCheckService,
            TelegramChannelCheckService telegramChannelCheckService,
            ILogger<UserBioService> logger)
        {
            _botClient = telegramBotClient;
            _instagramCheckService = instagramCheckService;
            _telegramChannelCheckService = telegramChannelCheckService;
            _logger = logger;
        }

        public async Task EnrichAndParseBioAsync(User user, CancellationToken stoppingToken)
        {
            try
            {
                int userId = user.Id;
                _logger.LogInformation($"Starting parse info about user - `{userId}`");

                var chatInfo = await _botClient.GetChatAsync(user.TelegramId, stoppingToken);

                if (user.LastUpdateBioDate != null) //  && (DateTimeOffset.UtcNow - user.LastUpdateBioDate).Value.Days < 14
                {
                    return;
                }

                if (string.IsNullOrEmpty(chatInfo.Bio))
                {
                    _logger.LogInformation($"Bio is empty for user - `{userId}`");
                    return;
                }

                var bio = chatInfo.Bio;
                var bioInLower = chatInfo.Bio.ToLower();

                _logger.LogInformation($"Got the bio - {bio} | user - `{userId}`");

                _logger.LogInformation($"Trying to get inst from bio - {bio}  | user - `{userId}`");
                string? instUserName = _instagramCheckService.GetUserNameByBio(bioInLower);
                _logger.LogInformation($"inst {instUserName ?? String.Empty}  | user - `{userId}`");

                int instSubscribersAmount = 0;
                bool isConfidence = false;

                _logger.LogInformation($"Trying to get TG from bio - {bio}  | user - `{userId}`");

                var tgChannelLink =
                    _telegramChannelCheckService.GetTgChannelNameFromString(bioInLower);
                int tgSubscribersAmount = 0;

                _logger.LogInformation($"TG {tgChannelLink ?? String.Empty}  | user - `{userId}`");



                _logger.LogInformation($"Trying to get inst subs amount | user - `{userId}`");

                if (instUserName is not null)
                {
                    instSubscribersAmount = await _instagramCheckService.GetInstagramFollowersByUserNameAsync(instUserName);
                    isConfidence = true;
                }
                else if (instUserName == null && !string.IsNullOrEmpty(chatInfo.Username))
                {
                    instUserName = chatInfo.Username;
                    instSubscribersAmount = await _instagramCheckService.GetInstagramFollowersByUserNameAsync(instUserName);
                }

                _logger.LogInformation($"Got inst subs amount - {instSubscribersAmount} | user - `{userId}`");
                _logger.LogInformation($"Trying to get tg subs amount | user - `{userId}`");

                if (tgChannelLink is not null)
                {
                    tgSubscribersAmount = await _telegramChannelCheckService.GetTelegramFollowersAmountByLink(tgChannelLink);
                }

                _logger.LogInformation($"Got inst subs amount - {tgSubscribersAmount} | user - `{userId}`");

                user.Bio = bio;
                user.LastUpdateBioDate = DateTimeOffset.UtcNow;

                if (instSubscribersAmount == 0 && tgSubscribersAmount == 0)
                {
                    _logger.LogInformation($"Did not find any subs in the both services | user - `{userId}`");
                    return;
                }

                user.UserBlogs ??= new UserBlog();
                user.UserBlogs.InstagramSubscribers = instSubscribersAmount;
                user.UserBlogs.IsLoginConfidence = isConfidence;
                user.UserBlogs.InstagramLogin = instUserName;
                user.UserBlogs.TgChannelSubscribers = tgSubscribersAmount;
                user.UserBlogs.TgChannel = tgChannelLink;

                _logger.LogInformation($"Done | user - `{userId}`");
            }
            catch (Exception e)
            {
                _logger.LogCritical($"{nameof(UserBioService)}: " + e.Message);
                await Task.Delay(1500, stoppingToken);
            }
        }
    }
}
