﻿using UserScraper.Services;
using VPN.BOT.Services.Services.Interfaces;


namespace UserScraper.BackgroundServices
{
    public class UserInfoScraperBackgroundService : BackgroundService
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<UserInfoScraperBackgroundService> _logger;
        private readonly IConfiguration _configuration;


        public UserInfoScraperBackgroundService(
            ILogger<UserInfoScraperBackgroundService> logger,
            IConfiguration configuration,
            IServiceProvider services
        )
        {
            _logger = logger;
            _configuration = configuration;
            _services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogWarning("Starting update users bio !!!");

                var scope = _services.CreateScope();
                var userBioService = scope.ServiceProvider.GetRequiredService<UserBioService>();
                var userService = scope.ServiceProvider.GetRequiredService<IUserService>();

                var users = await userService.GetValidUsersAsync();

                foreach (var user in users.OrderByDescending(x => x.StartDate))
                {
                    try
                    {
                        await userBioService.EnrichAndParseBioAsync(user, stoppingToken);
                        await userService.SaveAsync();
                        await Task.Delay(300, stoppingToken);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError($"{nameof(UserInfoScraperBackgroundService)}: " + e.Message);
                        await Task.Delay(1500, stoppingToken);
                    }
                }

                _logger.LogWarning("Done update users bio !!!");

                await Task.Delay(TimeSpan.FromDays(15), stoppingToken);
            }


        }
    }
}