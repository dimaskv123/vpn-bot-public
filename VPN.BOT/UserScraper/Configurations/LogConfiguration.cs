﻿using Serilog;
using Serilog.Exceptions;
using Serilog.Filters;
using Serilog.Sinks.TelegramBot;

namespace UserScraper.Configurations
{
    public static class LogConfiguration
    {
        public static void ConfigureLogging(HostBuilderContext ctx, Serilog.LoggerConfiguration lc)
        {
            var mongoConnectionString = ctx.Configuration.GetConnectionString("MongoDb");

            ArgumentException.ThrowIfNullOrEmpty(mongoConnectionString, "Cannot configure logging without mongo connection string");

            lc
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails();

            lc
                .WriteTo.Console()
                .Filter.ByExcluding(Matching.FromSource("System.Net.Http"))
                .WriteTo.Async(a => a.Logger(l => l.MinimumLevel.Information().WriteTo.MongoDB(mongoConnectionString, "Logs")))
                .WriteTo.Async(a => a.Logger(l =>
                {
                    l.Filter.ByExcluding(logEvent =>
                    {
                        if ((logEvent.Properties.TryGetValue("SourceContext", out var name)))
                        {
                            return name.ToString()
                                .Contains("telegram_bot_client");
                        }

                        return false;
                    });

                    l.MinimumLevel.Warning().WriteTo
                        .TelegramBot("5882475618:AAEBYnzgQ2ejwbQ10PWHBYSquhvuPN5SSqg", "6261544653");
                    l.MinimumLevel.Warning().WriteTo
                        .TelegramBot("5882475618:AAEBYnzgQ2ejwbQ10PWHBYSquhvuPN5SSqg", "254724042");
                }));
        }

    }
}
