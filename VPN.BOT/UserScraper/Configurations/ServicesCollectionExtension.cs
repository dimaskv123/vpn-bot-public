﻿using DAL.Context;
using Microsoft.EntityFrameworkCore;
using Repository.Implementation;
using Repository.Interfaces;
using StackExchange.Redis;
using Telegram.Bot;
using UserScraper.BackgroundServices;
using UserScraper.Services;
using VPN.BOT.Services.Services;
using VPN.BOT.Services.Services.Interfaces;

namespace UserScraper.Configurations;

public static class ServicesCollectionExtension
{
    public static void AddExternalServices(this IServiceCollection services, IConfiguration configuration)
    {
        var botConfig = configuration.GetRequiredSection("TelegramToken").Value ?? string.Empty;
        services.AddHttpClient("telegram_bot_client")
            .AddTypedClient<ITelegramBotClient>((httpClient, sp) =>
            {
                TelegramBotClientOptions options = new(botConfig);
                return new TelegramBotClient(options, httpClient);
            });

        services.AddHttpClient<IVpnServerConnector, VpnServerConnector>(client =>
        {
            // todo: client.BaseAddress = new Uri("");
        });
        services.AddDbContext<DataContext>(options =>
        {
            options.UseNpgsql(configuration["ConnectionStrings:PostgreDb"]);
            options.EnableSensitiveDataLogging();
        });
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

        // services.AddSingleton<IConnectionMultiplexer>(
        //     ConnectionMultiplexer.Connect(configuration.GetConnectionString("RedisConnection") ?? string.Empty));
        //services.AddScoped(provider => provider.GetRequiredService<IConnectionMultiplexer>().GetDatabase());
    }

    public static void AddRepositories(this IServiceCollection services)
    {
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<ICertificateRepository, CertificateRepository>();
        services.AddScoped<ICertificatePoolRepository, CertificatePoolRepository>();
        services.AddScoped<IServerRepository, ServerRepository>();
        services.AddScoped<IReferalRepository, ReferalRepository>();
        services.AddScoped<IScheduleMessageRepository, ScheduleMessageRepository>();
        services.AddScoped<IPaymentRepository, PaymentRepository>();
        services.AddScoped<IRestrictionsHistoryRepository, RestrictionsHistoryRepository>();
        services.AddScoped<ITriggerMessageRepository, TriggerMessageRepository>();
        services.AddScoped<ISysSettingRepository, SysSettingRepository>();
    }

    public static void AddInternalServices(this IServiceCollection services)
    {
        services.AddScoped<TelegramChannelCheckService>();
        services.AddScoped<InstagramCheckService>();
        services.AddScoped<UserBioService>();

        services.AddTransient<IUserService, UserService>();
        services.AddTransient<IReferalService, ReferalService>();
        services.AddTransient<IUserReferalService, UserReferalService>();
        services.AddScoped<ICertificateManger, CertificateManger>();
        services.AddScoped<IPoolCertificateService, PoolCertificateService>();
       // services.AddScoped<IRedisService, RedisService>();
        services.AddScoped<IAdminNotifyService, AdminNotifyService>();
        services.AddScoped<IMessageService, MessageService>();
        services.AddScoped<IDataUsageHistoryService, DataUsageHistoryService>();
        services.AddScoped<IRestrictionService, RestrictionService>();
        services.AddScoped<ISysSettingService, SysSettingService>();
    }
    public static void AddBackgroundServices(this IServiceCollection services)
    {
        services.AddHostedService<UserInfoScraperBackgroundService>();
    }
}