using VPN.BOT.Common.Enums;

namespace Repository.Models;

public class PaymentsByReceiver
{
    public PaymentTo Receiver { get; set; }

    public int TotalAmount { get; set; }
}