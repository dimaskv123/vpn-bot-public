﻿using Domain.Entity;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IUserRepository : IGenericRepository<User, long>
    {
        Task<bool> DoesUserExistAsync(long userId);
        Task<User> FindByTelegramIdAsync(long userId);
        Task<User> FindByVpnKey(string vpnKey);
        Task<IEnumerable<User>> GetAllValidAsync();

        Task<IEnumerable<User>> GetIncludeDeepConfigsAsync(Expression<Func<User, bool>>? expression = null,
            bool needsToTrack = true, params Expression<Func<User, object>>[] includes);

        Task<User> FindByTelegramIdIncludeDeepConfigsAsync(long userId, bool needsToTrack = true,
            params Expression<Func<User, object>>[] includes);
    }
}
