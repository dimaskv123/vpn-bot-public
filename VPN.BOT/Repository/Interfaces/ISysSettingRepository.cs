﻿using Domain.Entity;
using Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface ISysSettingRepository : IGenericRepository<SysSetting, int>
    {
        Task<SysSetting> GetSysSettingAsync(string name);
    }
}
