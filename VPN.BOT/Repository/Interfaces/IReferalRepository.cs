﻿using Domain.Entity;

namespace Repository.Interfaces
{
    public interface IReferalRepository
    {
        Task<Referal> GetByIdAsync(int id);
        Task AddAsync(Referal entity);
        Task SaveAsync();
        Task<Referal> FindByCodeAsync(string code);
        Task<Referal> FindByUserAsync(int id);
    }
}
