﻿using Domain.Entity;

namespace Repository.Interfaces;

public interface ITriggerMessageRepository : IGenericRepository<TriggerMessage, int>
{
    
}