﻿using Domain.Entity;

namespace Repository.Interfaces;

public interface IRestrictionsHistoryRepository : IGenericRepository<RestrictionsHistory, int>
{
    Task<RestrictionsHistory> GetLastRestrictionAsync(int userId);
}