﻿using System.Data;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Storage;

namespace Repository.Interfaces
{
    public interface IGenericRepository<T, TFind> 
        where T : class
    {
        Task<T> GetByIdAsync(TFind id);
        Task<IEnumerable<T>> GetAllAsync(bool needsToTrack = true);
                
        Task UpdateAsync(T entity);
        Task<List<T>> Find(Expression<Func<T, bool>> expression);
        Task AddAsync(T entity);
        Task AddRangeAsync(IEnumerable<T> entities);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
        Task SaveAsync();
        Task<IDbContextTransaction> BeginTransactionAsync(IsolationLevel level = IsolationLevel.ReadCommitted);
    }
}
