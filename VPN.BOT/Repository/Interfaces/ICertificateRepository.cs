﻿using System.Linq.Expressions;
using Domain.Entity;

namespace Repository.Interfaces
{
    public interface ICertificateRepository : IGenericRepository<UserConfig, int>
    {
        Task<IEnumerable<UserConfig>> GetAllIncludeAsync();
        Task<List<UserConfig>> FindInclude(Expression<Func<UserConfig, bool>> expression);
    }
}
