﻿using Domain.Entity;

namespace Repository.Interfaces;

public interface IScheduleMessageRepository : IGenericRepository<ScheduleMessage, int>
{
    public Task<IEnumerable<ScheduleMessage>> GetMessagesByUserId(int userId, bool includeCommonMessages = false);

    public Task MarkMessagesAsSentAsync(int userId);
}