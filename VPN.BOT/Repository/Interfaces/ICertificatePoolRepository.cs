﻿using System.Linq.Expressions;
using Domain.Entity;

namespace Repository.Interfaces;

public interface ICertificatePoolRepository : IGenericRepository<PoolConfig, int>
{
    Task<PoolConfig?> FindFirstAsync(Expression<Func<PoolConfig, bool>> expression);
}