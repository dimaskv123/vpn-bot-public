﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Domain.Entity;

namespace Repository.Interfaces
{
    public interface IServerRepository : IGenericRepository<Server, int>
    {
        Task<IEnumerable<Server>> GetAllIncludeUserConfigsAsync(bool needsToTrack = true);

        Task<IEnumerable<Server>> FindIncludeAllValidConfigsAsync(Expression<Func<Server, bool>> expression,
            bool needsToTrack = true);
    }
}
