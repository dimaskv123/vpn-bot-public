﻿using System.Linq.Expressions;
using Domain.Entity;
using Repository.Models;

namespace Repository.Interfaces;

public interface IPaymentRepository : IGenericRepository<Payment, int>
{
    Task<List<Payment>> FindAsync(Expression<Func<Payment, bool>> expression);

    Task<List<PaymentsByReceiver>> GetPaymentsByReceiver(int month, int year);
}