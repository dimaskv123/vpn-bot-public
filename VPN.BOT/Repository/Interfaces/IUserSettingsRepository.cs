using Domain.Entity;

namespace Repository.Interfaces;

public interface IUserSettingsRepository : IGenericRepository<UserSettings, int>
{
    
}