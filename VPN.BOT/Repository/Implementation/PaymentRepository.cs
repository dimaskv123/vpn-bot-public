﻿using DAL.Context;
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System.Linq.Expressions;
using Repository.Models;
using VPN.BOT.Common.Enums;

namespace Repository.Implementation;

public class PaymentRepository : GenericRepository<Payment, int>, IPaymentRepository
{
    public PaymentRepository(DataContext context) : base(context)
    {
    }

    public async Task<List<Payment>> FindAsync(Expression<Func<Payment, bool>> expression)
    {
        return await _context.Payments
            .Where(expression)
            .Include(x => x.UserConfig).ThenInclude(x => x.User).ThenInclude(x => x.UserSettings)
            .Include(x => x.UserConfig).ThenInclude(x => x.User).ThenInclude(x => x.ScheduleMessages)
            .Include(x => x.UserConfig).ThenInclude(x => x.User).ThenInclude(x => x.Configs)
            .Include(x => x.UserConfig).ThenInclude(x => x.User).ThenInclude(x => x.Configs).ThenInclude(x => x.Server)
            .Include(x => x.UserConfig).ThenInclude(x => x.RestrictionsHistories)
            .Include(x => x.UserConfig).ThenInclude(x => x.Payments)
            .Include(x => x.UserConfig).ThenInclude(x => x.Server)
            .ToListAsync();
    }

    public async Task<List<PaymentsByReceiver>> GetPaymentsByReceiver(int month, int year)
    {
        return await _context.Payments.Where(p =>
                p.UpdatedAt.HasValue 
                && p.UpdatedAt.Value.Month == month
                && p.UpdatedAt.Value.Year == year
                && p.Status == PaymentStatus.Succeeded)
            .GroupBy(x => x.PaymentTo)
            .Select(g => new PaymentsByReceiver()
            {
                Receiver = g.Key,
                TotalAmount = g.Sum(gp => gp.Amount)
            }).ToListAsync();
    }
}