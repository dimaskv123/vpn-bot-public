﻿using System.Data;
using System.Linq.Expressions;
using DAL.Context;
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Repository.Interfaces;

namespace Repository.Implementation;

public class CertificatePoolRepository : GenericRepository<PoolConfig, int>, ICertificatePoolRepository
{
    public CertificatePoolRepository(DataContext context) : base(context)
    {
    }


    public Task<PoolConfig?> FindFirstAsync(Expression<Func<PoolConfig, bool>> expression)
    {
        return _context.PoolConfigs.FirstOrDefaultAsync(expression);
    }
}