﻿using DAL.Context;
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;

namespace Repository.Implementation
{
    public class ReferalRepository : IReferalRepository
    {
        protected readonly DataContext _context;

        public ReferalRepository(DataContext dataContext)
        {
            _context = dataContext;
        }

        public async Task AddAsync(Referal entity)
        {
            await _context.AddAsync(entity);
        }

        public async Task<Referal> GetByIdAsync(int id)
        {
            return await _context.Set<Referal>().FindAsync(id);
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
        public async Task<Referal> FindByCodeAsync(string code)
        {
            return await _context.Set<Referal>().FirstOrDefaultAsync(r => r.InviteCode.Equals(code));
        }

        public async Task<Referal> FindByUserAsync(int id)
        {
            return await _context.Set<Referal>().FirstOrDefaultAsync(r => r.CreatedById == id);
        }
    }
}
