﻿using DAL.Context;
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System.Linq.Expressions;

namespace Repository.Implementation
{
    public class UserRepository : GenericRepository<User, long>, IUserRepository
    {
        private readonly DataContext _context;
        public UserRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetAllValidAsync()
        {
            var result = await _context.Set<User>().Include(x => x.UserBlogs).Where(x => x.EndDate == null).AsQueryable().ToListAsync();
            return result;
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _context.Set<User>()
                .Include(x => x.Configs).ThenInclude(x => x.RestrictionsHistories)
                .Include(x => x.Configs).ThenInclude(x => x.Server)
                .Include(x => x.Configs).ThenInclude(x => x.Payments)
                .Include(x => x.UserSettings)
                .Include(x => x.UserBlogs)
                .Include(x => x.ScheduleMessages)
                .Include(x => x.TriggerMessages)
                .AsQueryable()
                
                .ToListAsync();
        }

        public async Task<IEnumerable<User>> GetIncludeDeepConfigsAsync(Expression<Func<User, bool>>? expression = null, bool needsToTrack = true, params Expression<Func<User, object>>[] includes)
        {
            var query = _context.Set<User>().AsQueryable();

            query = query.Include(x => x.Configs).ThenInclude(x => x.RestrictionsHistories)
                .Include(x => x.Configs).ThenInclude(x => x.Server)
                .Include(x => x.Configs).ThenInclude(x => x.Payments)
                .Include(x => x.Tags);
                 //.Include(x => x.UserSettings)
                //.Include(x => x.UserBlogs)
                //.Include(x => x.ScheduleMessages)
                //.Include(x => x.TriggerMessages)

            foreach (var include in includes)
            {
                query = query.Include(include);
            }

            if (!needsToTrack)
            {
                query = query.AsNoTracking();
            }

            if (expression is not null)
            {
                query = query.Where(expression);
            }

            return await query.ToListAsync();
        }

        public async Task<bool> DoesUserExistAsync(long userId)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.TelegramId == userId) != null;
        }

        public async Task<User> FindByTelegramIdAsync(long userId)
        {
            return await _context.Users
                .Include(x => x.Configs).ThenInclude(cfg => cfg.Server)
                .Include(x => x.Configs).ThenInclude(cfg => cfg.RestrictionsHistories)
                .Include(x => x.Configs).ThenInclude(cfg => cfg.Payments)
                .Include(x => x.UserSettings)
                .Include(x => x.ScheduleMessages)
                .Include(x => x.InvitedBy)
                .Include(x => x.Invite)
                .FirstOrDefaultAsync(u => u.TelegramId == userId);
        }

        public async Task<User> FindByTelegramIdIncludeDeepConfigsAsync(long userId, bool needsToTrack = true, params Expression<Func<User, object>>[] includes)
        {
            var result  = await GetIncludeDeepConfigsAsync(user => user.TelegramId == userId, needsToTrack, includes);
            return result.FirstOrDefault();
        }


        public async Task<User> FindByVpnKey(string vpnKey)
        {
            return await _context.Users
                .Include(x => x.Configs).ThenInclude(cfg => cfg.Server)
                .Include(x => x.Configs).ThenInclude(cfg => cfg.RestrictionsHistories)
                .Include(x => x.Configs).ThenInclude(cfg => cfg.Payments)
                .Include(x => x.UserSettings)
                .Include(x => x.InvitedBy)
                .Include(x => x.Invite)
                .FirstOrDefaultAsync(u => u.Configs.Any(c => c.VpnKey == vpnKey));
        }
    }
}
