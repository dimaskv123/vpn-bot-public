using DAL.Context;
using Domain.Entity;
using Repository.Interfaces;

namespace Repository.Implementation;

public class UserSettingsRepository : GenericRepository<UserSettings, int>, IUserSettingsRepository
{
    public UserSettingsRepository(DataContext context) : base(context)
    {
    }
}