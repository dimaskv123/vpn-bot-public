﻿using System.Security.Cryptography.X509Certificates;
using DAL.Context;
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;

namespace Repository.Implementation;

public class ScheduleMessageRepository :  GenericRepository<ScheduleMessage, int>, IScheduleMessageRepository
{
    public ScheduleMessageRepository(DataContext context) : base(context)
    {
    }

    public async Task<IEnumerable<ScheduleMessage>> GetMessagesByUserId(int userId, bool includeCommonMessages = false)
    {
        if (includeCommonMessages)
        {
            return await _context.ScheduleMessages.Where(x => x.ToUserId == userId || x.ToUserId == -1).ToListAsync();
        }
        
        return await _context.ScheduleMessages.Where(x => x.ToUserId == userId).ToListAsync();
    }

    public async Task MarkMessagesAsSentAsync(int userId)
    {
        var messages = await _context.ScheduleMessages.Where(message => message.ToUserId == userId).ToListAsync();

        foreach (var message in messages)
        {
            message.IsSent = true;
        }

        _context.UpdateRange(messages);
    }


}