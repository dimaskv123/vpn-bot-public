﻿using DAL.Context;
using Domain.Entity;
using Repository.Interfaces;

namespace Repository.Implementation;

public class TriggerMessageRepository : GenericRepository<TriggerMessage, int>, ITriggerMessageRepository
{
    public TriggerMessageRepository(DataContext context) : base(context)
    {
    }
    
}