﻿using DAL.Context;
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System.Linq.Expressions;

namespace Repository.Implementation
{
    public class CertificateRepository : GenericRepository<UserConfig, int>, ICertificateRepository
    {
        public CertificateRepository(DataContext context) : base(context)
        {
        }

        public async Task<IEnumerable<UserConfig>> GetAllIncludeAsync()
        {
            return await _context.UserConfigs.Include(uc => uc.Server).Include(uc => uc.User).ToListAsync();
        }

        public async Task<List<UserConfig>> FindInclude(Expression<Func<UserConfig, bool>> expression)
        {
            return await _context.UserConfigs.Include(x => x.Server)
                .Include(x => x.RestrictionsHistories)
                .Include(x => x.User)
                .ThenInclude(x => x.UserSettings)
                .Include(x => x.Payments).Where(expression).ToListAsync();
        }
    }
}
