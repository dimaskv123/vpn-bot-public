﻿using System.Linq.Expressions;
using DAL.Context;
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;

namespace Repository.Implementation
{
    public class ServerRepository : GenericRepository<Server, int>, IServerRepository
    {
        public ServerRepository(DataContext context) : base(context)
        {
        }
        
        public async Task<IEnumerable<Server>> GetAllIncludeUserConfigsAsync(bool needsToTrack = true)
        {
            var result = await _context.Servers.Include(s => s.Configs).ToListAsync();
            return result;
        }
        
        public async Task<IEnumerable<Server>> FindIncludeAllValidConfigsAsync(Expression<Func<Server,bool>> expression, bool needsToTrack = true)
        {
            var result = await _context.Servers.Include(s => s.Configs.Where(config => config.IsValid)).Include(s => s.PoolConfigs.Where(pc => pc.UserConfigId == null)).Where(expression).ToListAsync();
            return result;
        }
    }
}
