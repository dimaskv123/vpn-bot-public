﻿using DAL.Context;
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;

namespace Repository.Implementation
{
    public class SysSettingRepository : GenericRepository<SysSetting, int>, ISysSettingRepository
    {

        public SysSettingRepository(DataContext context) : base(context)
        {
        }

        public async Task<SysSetting> GetSysSettingAsync(string name)
        {
            var result = await _context.Set<SysSetting>()
                               .FirstOrDefaultAsync(x => x.Name.ToLower().Contains(name.ToLower()));

            return result;
        }
    }
}
