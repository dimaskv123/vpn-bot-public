﻿using DAL.Context;
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;

namespace Repository.Implementation;

public class RestrictionsHistoryRepository : GenericRepository<RestrictionsHistory, int>, IRestrictionsHistoryRepository
{
    public RestrictionsHistoryRepository(DataContext context) : base(context)
    {
    }

    public Task<RestrictionsHistory> GetLastRestrictionAsync(int configId)
    {
        return _context.RestrictionsHistory.Where(x => x.UserConfigId == configId).OrderBy(x => x.StartTime).LastAsync();
    }
}