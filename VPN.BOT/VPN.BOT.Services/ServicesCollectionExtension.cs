﻿using Domain.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository.Implementation;
using Repository.Interfaces;
using VPN.BOT.Common.Constants;
using VPN.BOT.Services.ExternalService.Yookassa;
using VPN.BOT.Services.ExternalService.Yookassa.Interfaces;
using VPN.BOT.Services.ExternalService.Yookassa.Settings;
using VPN.BOT.Services.ExternalServices;
using VPN.BOT.Services.Services;
using VPN.BOT.Services.Services.Interfaces;
using VPN.BOT.Services.Services.OnTriggerSendMessageEngine;
using VPN.BOT.Services.Services.SubscribeEngine;
using VPN.BOT.Services.Services.SubscribeEngine.Interfaces;

namespace VPN.BOT.Services;

public static class ServicesCollectionExtension
{
    public static void AddServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<HandleUpdateService>();

        Constants.Host = configuration.GetSection("ServerHost").Value;

        services.AddHttpClient<IVpnServerConnector, VpnServerConnector>(client =>
        {
            // todo: client.BaseAddress = new Uri("");
        });
        services.AddTransient<IUserService, UserService>();
        services.AddTransient<IReferalService, ReferalService>();
        services.AddTransient<IUserReferalService, UserReferalService>();
        services.AddScoped<ICertificateManger, CertificateManger>();
        services.AddScoped<IPoolCertificateService, PoolCertificateService>();
        //services.AddScoped<IRedisService, RedisService>();
        services.AddScoped<IAdminNotifyService, AdminNotifyService>();
        services.AddScoped<IYookassaClientManager, YookassaClientManager>();
        services.AddScoped<IMessageService, MessageService>();
        services.AddScoped<IDataUsageHistoryService, DataUsageHistoryService>();
        services.AddScoped<IRestrictionService, RestrictionService>();
        services.AddScoped<ISysSettingService, SysSettingService>();
        services.AddScoped<InstagramCheckService>();
        services.AddScoped<InstagramCheckLegacyService>();

        // todo: в отдельное место какое-то лучше

        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IUserSettingsRepository, UserSettingsRepository>();
        services.AddScoped<ICertificateRepository, CertificateRepository>();
        services.AddScoped<IServerRepository, ServerRepository>();
        services.AddScoped<IReferalRepository, ReferalRepository>();
        services.AddScoped<IScheduleMessageRepository, ScheduleMessageRepository>();
        services.AddScoped<IPaymentRepository, PaymentRepository>();
        services.AddScoped<IRestrictionsHistoryRepository, RestrictionsHistoryRepository>();
        services.AddScoped<ITriggerMessageRepository, TriggerMessageRepository>();
        services.AddScoped<ISysSettingRepository, SysSettingRepository>();
        services.AddScoped<ICertificatePoolRepository, CertificatePoolRepository>();


        services.AddScoped<ISubscribeEngine, LimitByTotalGigabits>();
        services.AddScoped<ISubscribeEngine, LimitByTodayGigabits>();
        services.AddScoped<SubscribeEngineFactory>();

        services.AddScoped<TriggerEngine, NoUserConfigTrigger>();
        services.AddScoped<TriggerEngine, OnUnCompletePayment>();
        services.AddScoped<TriggerEngine, PaymentExpireIn3DaysTrigger>();
        services.AddScoped<TriggerEngine, PaymentExpireIn1DayTrigger>();
        services.AddScoped<TriggerEngine, NoDataUsageTrigger>();
        services.AddScoped<TriggerEngine, ConfigRetakeTrigger>();
    }
}