﻿using System.Text.RegularExpressions;
using Repository.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Types;
using VPN.BOT.Services.ExternalServices;
using Exception = System.Exception;

namespace VPN.BOT.Services.Services;

public class InstagramCheckLegacyService
{
    private IUserRepository _userRepository;
    private ITelegramBotClient _botClient;
    private InstagramCheckService _instagramCheckService;

    private int MinFollowersAmount = 5000;

    public InstagramCheckLegacyService(IUserRepository userRepository, ITelegramBotClient botClient,
        InstagramCheckService instagramCheckService)
    {
        _userRepository = userRepository;
        _botClient = botClient;
        _instagramCheckService = instagramCheckService;
    }

    public async Task CheckAllUser()
    {
        var users = await _userRepository.Find(x => x.EndDate == null);

        foreach (var user in users.OrderBy(x => x.Id))
        {
            try
            {
                Chat chat = null;
                try
                {
                    chat = await _botClient.GetChatAsync(user.TelegramId);
                }
                catch
                {
                }

                List<string> links = new List<string>();

                if (chat != null)
                {
                    links = await TryGetInstagramLink(chat.Username, chat.Bio);

                }
                else
                {
                    links = await TryGetInstagramLink(user.UserName, string.Empty);
                }
                
                if (!links.Any()) continue;
                
                var text = $"Юзер {user.Id} \nИмя: {user.FirstName} \nФамилия: {user.LastName} \nUsernName: @{user.UserName} \nBIO: {chat?.Bio}" +
                           Environment.NewLine +
                           Environment.NewLine +
                           "Найденны ссылки: " + Environment.NewLine + string.Join(Environment.NewLine, links);

                await _botClient.SendTextMessageAsync(669363145, text);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }

    public async Task<List<string>> TryGetInstagramLink(string userName, string bio)
    {
        var links = new List<string>();

        var (isBloger, followers) = await CheckFollowersOnInstagram(userName);
        
        if (!string.IsNullOrEmpty(userName) && isBloger)
        {
            links.Add($"https://www.instagram.com/{userName} ({followers} подписчиков)");
        }

        if (!string.IsNullOrEmpty(bio))
        {
            var regex = new Regex(":+\\s(\\w+)");

            var matches = regex.Matches(bio);

            foreach (Match match in matches)
            {
                var potentialUserName = match.Groups[1].Value;

                (isBloger, followers) = await CheckFollowersOnInstagram(potentialUserName);
                
                if (isBloger)
                {
                    links.Add($"https://www.instagram.com/{potentialUserName} ({followers} подписчиков)");
                }
            }
            
            var userNameRegex = new Regex("@+\\s(\\w+)");

            var userNameMatches = userNameRegex.Matches(bio);

            foreach (Match match in userNameMatches)
            {
                var potentialUserName = match.Groups[1].Value;

                (isBloger, followers) = await CheckFollowersOnInstagram(potentialUserName);
                
                if (isBloger)
                {
                    links.Add($"https://www.instagram.com/{potentialUserName} ({followers} подписчиков)");
                }
            }

            var urlRegex =
                new Regex(
                    "^https?:\\/\\/(?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b(?:[-a-zA-Z0-9()@:%_\\+.~#?&\\/=]*)$");

            matches = urlRegex.Matches(bio);

            foreach (Match match in matches)
            {
                links.Add(match.Value);
            }
        }

        return links;
    }

    private async Task<(bool, int)> CheckFollowersOnInstagram(string userName)
    {
        var followersAmount = await _instagramCheckService.GetInstagramFollowersByUserName(userName);

        return (followersAmount > MinFollowersAmount, followersAmount);
    }
}