﻿using Domain.Entity;
using VPN.BOT.Common.Enums;

namespace VPN.BOT.Services.Services.Interfaces
{
    public interface ISysSettingService
    {
        Task<SysSetting> GetSettingAsync(SysSettingName sysSettingName);
        Task UpdateSettingValue(SysSetting sysSetting, string value);
        Task<DateTimeOffset?> GetSettingDateValueAsync(SysSettingName sysSettingName);
        Task UpdateSettingValueAsync(SysSettingName sysSettingName, string value);
    }
}
