﻿using Domain.Entity;
using VPN.BOT.Services.ExternalService.Model;

namespace VPN.BOT.Services.Services.Interfaces;

public interface IVpnServerConnector
{
    public Task<OutlineUserModel> CreateCertificateAsync(User userModel, Server bestServer);
    Task<OutlineUserModel> CreateCertificateAsync(Server serverModel);
    Task DeleteCertificateAsync(UserConfig userConfig);

    Task LimitKey(UserConfig userConfig);
    Task UnLimitKey(UserConfig userConfig);
}