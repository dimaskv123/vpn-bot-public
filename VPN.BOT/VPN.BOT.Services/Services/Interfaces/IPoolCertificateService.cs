﻿using Domain.Entity;

namespace VPN.BOT.Services.Services.Interfaces;

public interface IPoolCertificateService
{
    Task AddAsync(PoolConfig poolConfig);
    Task<PoolConfig?> GetAsync(bool isPremium = false);
    Task SaveAsync();
}