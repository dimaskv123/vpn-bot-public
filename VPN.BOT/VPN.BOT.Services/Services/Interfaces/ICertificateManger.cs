﻿using Domain.Entity;
using System.Linq.Expressions;

namespace VPN.BOT.Services.Services.Interfaces;

public interface ICertificateManger
{
    Task RevokeAllExpiredCertificate();
    Task RevokeUserCertificateAsync(User user);
    Task RevokeUserCertificateAsync(UserConfig userConfig);
    Task<string> GetNewCertificate(long userId, int days, bool premium = false);
    Task<int> CreateCertificateBeforePayment(long userId, int days, bool premium = false);
    Task UpdateCertificateAfterSuccessfulPayment(UserConfig userConfig, int addDays = 0);
    Task UpdateCertificateAfterFailedPayment(UserConfig userConfig);
    Task<bool> DoesUserHaveValidCertificateAsync(long userId);
    Task UpdateUsersDataInfoAsync(CancellationToken cancellationToken);
    Task<List<Server>> GetAllAvailableServers(int days = 0, bool premium = false);
    Task<string> UpdateUserConfigAsync(long userId, int serverId = 0, bool findBestServer = false);
    Task<List<UserConfig>> FindCertificatesAsync(Expression<Func<UserConfig, bool>> expression);
    Task<string> UpdateConfigAsync(UserConfig config, int serverId = 0, bool findBestServer = false);
}