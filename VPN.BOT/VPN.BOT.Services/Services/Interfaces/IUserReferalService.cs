﻿using Domain.Entity;

namespace VPN.BOT.Services.Services.Interfaces
{
    public interface IUserReferalService
    {
        Task RegisterReferalUserAsync(long invitedUserId, string referalId);
        Task<string> GetInviteCodeAsync(User user);
        Task<int> GetInvitedAmountByUserAsync(int id);
    }
}
