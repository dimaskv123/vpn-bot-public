﻿using Domain.Entity;

namespace VPN.BOT.Services.Services.Interfaces
{
    public interface IReferalService
    {
        Task<Referal> FindByInviteCodeAsync(string code);
        string GenerateInviteCode(User user);
        Task<Referal> FindByUserCreatedAsync(int id);
    }
}
