﻿namespace VPN.BOT.Services.Services.Interfaces;

public interface IAdminNotifyService
{
    Task NotifyAllAdmins(string message);
}