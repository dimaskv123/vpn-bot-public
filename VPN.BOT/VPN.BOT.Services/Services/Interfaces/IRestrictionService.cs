﻿namespace VPN.BOT.Services.Services.Interfaces
{
    public interface IRestrictionService
    {
        Task MarkUnbanLastUserRestrictionAsync(long userId);
    }
}
