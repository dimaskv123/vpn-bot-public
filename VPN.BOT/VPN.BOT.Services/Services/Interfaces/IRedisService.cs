﻿namespace VPN.BOT.Services.Services.Interfaces
{
    public interface IRedisService
    {
        Task<T> GetAsync<T>(string key) where T : class;
        Task AddToCollectionAsync<T>(string collectionName, T element);
        Task<IEnumerable<T>> GetCollectionAsync<T>(string collectionName);
        Task DeleteKeyAsync(string keyName);
    }
}
