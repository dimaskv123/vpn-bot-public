﻿using Domain.Entity;
using VPN.BOT.Common.Enums;

namespace VPN.BOT.Services.Services.Interfaces
{
    public interface IUserService
    {
        Task<User> FindByTelegramIdAsync(long userId);
        Task<User> FindByVpnKeyAsync(string vpnKey);
        Task<bool> DoesUserExist(long userId);

        public Task<int> GetNewUsersAmountByDate(DateTimeOffset date);
        Task RevokeUser(User user);

        IEnumerable<User> WhereNoTriggerOrLastTriggerWasIn(IEnumerable<User> source, TriggerType triggerType,
            TimeSpan timeSpan);

        Task<List<User>> GetValidUsersAsync(); 
        Task UpdateEmailAsync(User user, string email);
        Task UpdateChatStateAsync(User user, ChatState chatState);
        Task SaveAsync();
    }
}