﻿using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using VPN.BOT.Common.Enums;
using User = Domain.Entity.User;

namespace VPN.BOT.Services.Services.Interfaces
{
    public interface IMessageService
    {
        Task<Message> SendMessageAsync(MessageName messageName, User user);
        Task<ReplyKeyboardMarkup> GenerateReplyKeyboardMarkup(User user);
    }
}
