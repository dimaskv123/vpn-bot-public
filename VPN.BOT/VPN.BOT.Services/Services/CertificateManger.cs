﻿using Domain.Entity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Web;
using Telegram.Bot.Exceptions;
using VPN.BOT.Common.Constants;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.ExternalService.Model;
using VPN.BOT.Services.ExternalServices;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services;

public class CertificateManger : ICertificateManger
{
    private const string UnresolvedKeysCollectionName = "UnresolvedRevokeKeysIds";
    private readonly IVpnServerConnector _vpnServerConnector;
    private readonly IUserRepository _userRepository;
    private readonly ICertificateRepository _certificateRepository;
    private readonly IPoolCertificateService _poolCertificateService;
    private readonly IServerRepository _serverRepository;
    private readonly IMessageService _messageService;
    private readonly ILogger<CertificateManger> _logger;
    private readonly ISysSettingService _sysSettingService;
    //private readonly IRedisService _redisService;
    private readonly IServiceProvider _services;

    private const int DataRoundMantissaAccuracy = 3;

    public CertificateManger(
        IVpnServerConnector vpnServerConnector,
        IUserRepository userRepository,
        ICertificateRepository certificateRepository,
        IPoolCertificateService poolCertificateService,
        IServerRepository serverRepository,
        ILogger<CertificateManger> logger,
        IMessageService messageService,
        ISysSettingService sysSettingService,
        //IRedisService redis,
        IServiceProvider serviceProvider)
    {
        _vpnServerConnector = vpnServerConnector;
        _userRepository = userRepository;
        _certificateRepository = certificateRepository;
        _poolCertificateService = poolCertificateService;
        _serverRepository = serverRepository;
        _logger = logger;
        _messageService = messageService;
        _sysSettingService = sysSettingService;
        //_redisService = redis;
        _services = serviceProvider;
    }

    public async Task RevokeAllExpiredCertificate()
    {
        // to do: сделать через куаребле
        var allConfigs =
            (await _certificateRepository.FindInclude(us => us.IsValid && us.EndDate <= DateTimeOffset.Now)).ToList();
        //var unresolvedKeys = await _redisService.GetCollectionAsync<UserConfig>(UnresolvedKeysCollectionName);


        //if (unresolvedKeys is not null && unresolvedKeys.Any())
        //{
        //    allConfigs.AddRange(unresolvedKeys);

        //    if (unresolvedKeys.Any(cfg => cfg.Id == 90214))
        //    {
        //        _logger.LogCritical($"{90214} достал из кеша на анбан");
        //    }

        //    await _redisService.DeleteKeyAsync(UnresolvedKeysCollectionName);
        //}

        foreach (var config in allConfigs)
        {
            if (config.EndDate >= DateTimeOffset.Now)
            {
                continue;
            }

            if (config.Id == 90214)
            {
                _logger.LogCritical($"{90214} запрос на удаление в отулайн");
            }

            RevokeUserCertificateAsync(config);
            try
            {
                await UpdateUserTodayLimitAsync(config.User.TelegramId);
            }
            catch (Exception e)
            {
                _logger.LogCritical(
                    $"{nameof(UpdateUserTodayLimitAsync)} {config.Id} {e.Message}{e.StackTrace}");
            }


            if (config.User is not null)
            {
                await NotificateUserAboutRevokeKeyAsync(config);
            }

            try
            {
                await _certificateRepository.SaveAsync();
                await _userRepository.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Revoke pizdez");
                continue;
            }
        }
    }

    private async Task NotificateUserAboutRevokeKeyAsync(UserConfig config)
    {
        var user = config.User;

        try
        {
            user.UserSettings.SubscribeTheoryMethod = SubscribeTheoryMethod.NoSubscribeTheoryMethod;
            if (config.Payments.Any(x => x.Status == PaymentStatus.Succeeded))
            {
                await _messageService.SendMessageAsync(MessageName.PayedCertificateExpire, user);
            }
            else
            {
                await _messageService.SendMessageAsync(MessageName.CertificateExpire, user);
            }
        }
        catch (ApiRequestException ex)
        {
            user.EndDate = DateTimeOffset.Now;
        }
    }

    public async Task RevokeUserCertificateAsync(UserConfig userConfig)
    {
        userConfig.IsValid = false;
        userConfig.DataDifference = 0;

        try
        {
            await _vpnServerConnector.DeleteCertificateAsync(userConfig);
        }
        catch (Exception e)
        {
            _logger.LogError(
                $"{nameof(RevokeAllExpiredCertificate)} cannot revoke config {userConfig.Id} because of {e.Message} Will write into redis.");
            //await _redisService.AddToCollectionAsync(UnresolvedKeysCollectionName,
            //    new UserConfig()
            //    {
            //        Id = userConfig.Id,
            //        OutlineUserId = userConfig.OutlineUserId,
            //        Server = userConfig.Server,
            //        ServerId = userConfig.ServerId
            //    });
        }
    }

    public async Task UpdateUserTodayLimitAsync(long telegramId)
    {
        // Первый ключ - безлимитный, второй - с 1 гб по умолчанию, с 3 ключа начинаем резать
        const int KeysAmountWithOutReduce = 2;
        var user = await _userRepository.FindByTelegramIdAsync(telegramId);

        var freeConfigs = user.Configs.Where(config =>
            config.Payments == null || config.Payments.All(x => x.Status != PaymentStatus.Succeeded)).ToList();
        int limitedKeysAmount = freeConfigs.Count - KeysAmountWithOutReduce;

        bool needsToReduce = freeConfigs.Count >= KeysAmountWithOutReduce;

        if (!needsToReduce)
        {
            return;
        }

        double reduceAmount = Math.Round((limitedKeysAmount * Constants.LimitConstants.REDUCE_STEP_GIGABYTES), 1);

        if (user.UserSettings.PersonalTodayLimit - reduceAmount < Constants.LimitConstants.MIN_PERSONAL_LIMIT_GIGABYTES)
        {
            user.UserSettings.PersonalTodayLimit = Constants.LimitConstants.REDUCE_STEP_GIGABYTES;
        }
        else
        {
            user.UserSettings.PersonalTodayLimit -= reduceAmount;
        }
    }

    public async Task RevokeUserCertificateAsync(Domain.Entity.User user)
    {
        var expiredConfig = user.Configs.OrderByDescending(uc => uc.EndDate).FirstOrDefault();
        if (expiredConfig == null) return;

        await RevokeUserCertificateAsync(expiredConfig);
        await _userRepository.SaveAsync();
    }

    public async Task<string> GetNewCertificate(long userId, int days, bool premium = false)
    {
        UserConfig? newUserConfiguration = null;
        User userModel = await _userRepository.FindByTelegramIdIncludeDeepConfigsAsync(userId, false);

        PoolConfig? poolConfig = await _poolCertificateService.GetAsync(premium);
        if (poolConfig is null)
        {
            newUserConfiguration = await CreateCertificateAsync(userModel, days, premium);
        }
        else
        {
            newUserConfiguration = new UserConfig()
            {
                VpnKey = poolConfig.AccessUrl,
                OutlineUserId = poolConfig.OutlineUserId,
                ServerId = poolConfig.ServerId,
                IsValid = true
            };
        }

        if (newUserConfiguration is null)
        {
            string errorMessage = "Cant get key from pool and generate it!";

            _logger.LogCritical(errorMessage);
            throw new Exception(errorMessage);
        }

        newUserConfiguration.StartDate = DateTimeOffset.Now;
        newUserConfiguration.EndDate = DateTimeOffset.Now.AddDays(days);
        newUserConfiguration.UserId = userModel.Id;

        await _certificateRepository.AddAsync(newUserConfiguration);
        await _certificateRepository.SaveAsync();

        if (poolConfig is not null)
        {
            poolConfig.UserConfigId = newUserConfiguration.Id;
            await _poolCertificateService.SaveAsync();
        }

        return newUserConfiguration.VpnKey;
    }

    private async Task<UserConfig?> CreateCertificateAsync(User user, int days, bool premium = false)
    {
        var orderedServers = await GetBestServersAsync(days, premium);
        OutlineUserModel? newOutlineUser = null;
        Server bestServer = null;

        foreach (var server in orderedServers.Take(5))
        {
            try
            {
                bestServer = server;
                newOutlineUser = await _vpnServerConnector.CreateCertificateAsync(user, bestServer);
                break;
            }
            catch
            {
            }
        }

        if (newOutlineUser is null)
        {
            string errorMessage = "Impossible to generate key";

            _logger.LogCritical(errorMessage);
            throw new Exception(errorMessage);
        }

        var newUserConfiguration = new UserConfig()
        {
            VpnKey = newOutlineUser.AccessUrl,
            OutlineUserId = newOutlineUser.Id,
            UserId = user.Id,
            ServerId = bestServer.Id,
            IsValid = true
        };

        return newUserConfiguration;
    }

    public async Task<int> CreateCertificateBeforePayment(long userId, int days, bool premium = false)
    {
        var bestServer = await GetBestServerAsync(days, premium);
        var userModel = await _userRepository.FindByTelegramIdIncludeDeepConfigsAsync(userId, false);

        var newUserConfiguration = new UserConfig()
        {
            StartDate = DateTimeOffset.Now,
            EndDate = DateTimeOffset.Now.AddDays(days),
            UserId = userModel.Id,
            ServerId = bestServer.Id,
            IsValid = false
        };

        await _certificateRepository.AddAsync(newUserConfiguration);
        await _certificateRepository.SaveAsync();

        return newUserConfiguration.Id;
    }

    public async Task UpdateCertificateAfterSuccessfulPayment(UserConfig userConfig, int addDays = 0)
    {
        if (string.IsNullOrEmpty(userConfig.VpnKey))
        {
            var newOutlineUser = await _vpnServerConnector.CreateCertificateAsync(userConfig.User, userConfig.Server);
            userConfig.VpnKey = newOutlineUser.AccessUrl;
            userConfig.OutlineUserId = newOutlineUser.Id;
        }

        userConfig.EndDate = userConfig.EndDate.AddDays(addDays);
        userConfig.IsValid = true;

        await _certificateRepository.UpdateAsync(userConfig);

        await _serverRepository.SaveAsync();
    }

    public async Task UpdateCertificateAfterFailedPayment(UserConfig userConfig)
    {
        _certificateRepository.Remove(userConfig);
        await _serverRepository.SaveAsync();
    }

    public async Task<bool> DoesUserHaveValidCertificateAsync(long userId)
    {
        var user = await _userRepository.FindByTelegramIdIncludeDeepConfigsAsync(userId, false);
        return user != null && user.Configs != null && user.Configs.Any(c => c.IsValid);
    }

    public async Task UpdateUsersDataInfoAsync(CancellationToken cancellationToken)
    {
        var lastUpdateDate = await _sysSettingService.GetSettingDateValueAsync(SysSettingName.LastCalcDataUpdateDate);
        var timeNow = DateTimeOffset.UtcNow.AddHours(3);
        var resetTraffic = timeNow.Day != lastUpdateDate.Value.Day;
        var limitPersonalTraffic = await _sysSettingService.GetSettingAsync(SysSettingName.LimitPersonalTraffic);

        if (limitPersonalTraffic.Enabled)
        {
            _logger.LogCritical("Включена настройка по персональному лимиту!");
        }

        if (resetTraffic)
        {
            _logger.LogCritical("Начинаю обнуление");

            await ResetDataAllValidKeysAsync();
            await _sysSettingService.UpdateSettingValueAsync(SysSettingName.LastCalcDataUpdateDate, timeNow.ToString());

            _logger.LogCritical("Занулил");
            return;
        }

        var servers = await _serverRepository.GetAllAsync(false);
        _logger.LogInformation("got server list");
        _logger.LogWarning("Parallel data update has been started");
        await Parallel.ForEachAsync(servers, cancellationToken, async (server, token) =>
        {
            try
            {
                var scope = _services.CreateScope();
                var certificateRepository = scope.ServiceProvider.GetRequiredService<ICertificateRepository>();
                var userConfigs = (await certificateRepository.FindInclude(x => x.ServerId == server.Id && x.IsValid))
                    .ToList();

                if (!userConfigs.Any()) return;

                var outlineService = new OutlineService(server.BaseUrl);
                var usersBytes = await outlineService.GetOutlineUsersBytesAsync();
                _logger.LogInformation($"got configs for {server.Country}");

                foreach (var userConfig in userConfigs)
                {
                    if (!usersBytes.ContainsKey(userConfig.OutlineUserId))
                    {
                        continue;
                    }

                    var gigabytesUsed = BytesToGigabytes(usersBytes[userConfig.OutlineUserId]) +
                                        userConfig.LastKeyTotalGigabytes;
                    var delta = Math.Abs(Math.Round(gigabytesUsed - userConfig.TotalGigabytes,
                        DataRoundMantissaAccuracy));

                    userConfig.DataDifference = delta;

                    userConfig.TodayGigabytes =
                        Math.Round(userConfig.TodayGigabytes + delta, DataRoundMantissaAccuracy);

                    userConfig.TotalGigabytes = gigabytesUsed;

                    if (limitPersonalTraffic.Enabled)
                    {
                        await UpdateUserTodayLimitAsync(userConfig.User.TelegramId);
                    }
                }

                _logger.LogInformation($"done with  {server.Country}");

                await certificateRepository.SaveAsync();

                _logger.LogInformation($"saved changes  {server.Country}");
            }
            catch (Exception e)
            {
                _logger.LogError($"Сервер {server.Id} - {server.Country} не отвечает - {e.Message}");
            }
        });
        _logger.LogWarning("Parallel data update has been finished");


        if (limitPersonalTraffic.Enabled)
        {
            _logger.LogCritical("Настройка по персональному лимиту закончена! отключите настройку в базе");
            limitPersonalTraffic.Enabled = false;
            await _sysSettingService.UpdateSettingValue(limitPersonalTraffic, "2");
        }

        await _sysSettingService.UpdateSettingValueAsync(SysSettingName.LastCalcDataUpdateDate, timeNow.ToString());
    }

    private async Task ResetDataAllValidKeysAsync()
    {
        var userConfigs = (await _certificateRepository.FindInclude(x => x.IsValid))
            .ToList();

        foreach (var config in userConfigs)
        {
            config.TodayGigabytes = 0;
        }

        await _certificateRepository.SaveAsync();
    }

    public async Task<List<Server>> GetAllAvailableServers(int days = 0, bool premium = false)
    {
        return await _serverRepository
            .Find(s => s.IsPremiumServer.Equals(premium) &&
                       (s.EndDate == null || (s.EndDate - DateTimeOffset.UtcNow).Value.Days > days));
    }

    private double BytesToGigabytes(long bytes)
    {
        return Math.Round(double.Parse(bytes.ToString()) / 100_000_000_0, 3);
    }

    private async Task<Server> GetBestServerAsync(int days, bool premium = false)
    {
        // var userServers = await _certificateRepository.GetAllAsync(false);
        var servers = (await _serverRepository.GetAllIncludeUserConfigsAsync(false))
            .Where(s => s.IsPremiumServer.Equals(premium) &&
                        (s.EndDate is null || (s.EndDate - DateTimeOffset.UtcNow).Value.Days > days));

        var serverModel = new Server();

        // if (userServers.Any())
        // {
        //     var bestServer = servers.Except(userServers.Select(x => x.Server)).FirstOrDefault();


        serverModel = servers
            .OrderBy(s => s.Configs.Count(x => x.IsValid))
            .First();
        //     _logger.LogCritical($"Лучший  сервер {premium}: {bestServer.Country} {bestServer.Configs.Count}");


        // serverModel = bestServer;
        // }
        // else
        // {
        //     serverModel = servers.FirstOrDefault();
        // }

        if (serverModel == null)
        {
            string exceptionText = $"No server found with reqiared conditions! {days} {premium}";
            _logger.LogCritical(exceptionText);
            throw new ArgumentNullException(exceptionText);
        }

        return serverModel;
    }

    private async Task<List<Server>> GetBestServersAsync(int days, bool premium = false)
    {
        var servers = (await _serverRepository.GetAllIncludeUserConfigsAsync(false))
            .Where(s => s.IsPremiumServer.Equals(premium) &&
                        (s.EndDate is null || (s.EndDate - DateTimeOffset.UtcNow).Value.Days > days));

        var orderedServers = servers
            .OrderBy(s => s.Configs.Count(x => x.IsValid)).ToList();

        if (orderedServers == null || !orderedServers.Any())
        {
            string exceptionText = $"No server found with reqiared conditions! {days} {premium}";
            _logger.LogCritical(exceptionText);
            throw new ArgumentNullException(exceptionText);
        }

        return orderedServers;
    }


    public async Task<string> UpdateUserConfigAsync(long userId, int serverId = 0, bool findBestServer = false)
    {
        var user = await _userRepository.FindByTelegramIdIncludeDeepConfigsAsync(userId);

        var lastValidConfig = user.Configs.LastOrDefault(c => c.IsValid);

        if (lastValidConfig == null)
        {
            throw new ValidationException("User does not have any key.");
        }

        var newVpnKey = await UpdateConfigAsync(lastValidConfig, serverId, findBestServer);

        return newVpnKey;
    }

    public async Task<string> UpdateConfigAsync(UserConfig config, int serverId = 0, bool findBestServer = false)
    {
        Server server;

        if (findBestServer)
        {
            var keyDays = (config.EndDate - config.StartDate).Days;

            var isPaid =
                config?.Payments?.Any(x => x.Status == PaymentStatus.Succeeded && x.UserConfigId == config.Id) ?? false;

            server = await GetBestServerAsync(keyDays, isPaid);
        }
        else
        {
            server = await _serverRepository.GetByIdAsync(serverId);
        }

        if (server == null)
        {
            throw new ValidationException($"No server with id {serverId}");
        }

        try
        {
            await _vpnServerConnector.DeleteCertificateAsync(config);
        }
        catch (Exception e)
        {
        }

        var newOutlineUser = await _vpnServerConnector.CreateCertificateAsync(config.User, server);

        config.OutlineUserId = newOutlineUser.Id;
        config.VpnKey = newOutlineUser.AccessUrl;
        config.ServerId = server.Id;
        config.LastKeyTotalGigabytes = config.TotalGigabytes;

        await _certificateRepository.SaveAsync();

        return config.VpnKey;
    }

    public Task<List<UserConfig>> FindCertificatesAsync(Expression<Func<UserConfig, bool>> expression)
    {
        return _certificateRepository.FindInclude(expression);
    }

    public static string GetEncodedInviteUrl(string vpnKey)
    {
        var encodeInvite = Constants.InviteUrl + HttpUtility.UrlEncode(vpnKey);

        return encodeInvite;
    }
}