﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services;

public class PoolCertificateService : IPoolCertificateService
{

    private readonly ILogger<PoolCertificateService> _logger;
    private readonly ICertificatePoolRepository _configsPoolRepository;
    private readonly IServerRepository _serverRepository;
    
    
    public PoolCertificateService(ILogger<PoolCertificateService> logger, ICertificatePoolRepository configsPoolRepository, IServerRepository serverRepository)
    {
        _logger = logger;
        _configsPoolRepository = configsPoolRepository;
        _serverRepository = serverRepository;
    }

    public async Task AddAsync(PoolConfig poolConfig)
    {
        await _configsPoolRepository.AddAsync(poolConfig);
    }
    
    public async Task<PoolConfig?> GetAsync(bool isPremium = false)
    {
        var servers = await _serverRepository.Find(server => server.EndDate == null && server.IsPremiumServer == isPremium);
        var serversIds = servers.Select(s => s.Id);
        
        PoolConfig? poolConfig = await _configsPoolRepository.FindFirstAsync(config => config.UserConfigId == null && serversIds.Contains(config.ServerId));
        
        return poolConfig;
    }
    
    public async Task SaveAsync()
    {
        await _configsPoolRepository.SaveAsync();
    }
}