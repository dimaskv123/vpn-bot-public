﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Common.Utils;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services
{
    public class SysSettingService : ISysSettingService
    {
        private readonly ISysSettingRepository _sysSettingRepository;
        private readonly ILogger<SysSettingService> _logger;

        public SysSettingService
            (
            ISysSettingRepository sysSettingRepository,
            ILogger<SysSettingService> logger
            )
        {
            _sysSettingRepository = sysSettingRepository;
            _logger = logger;
        }

        public async Task<SysSetting> GetSettingAsync(SysSettingName sysSettingName)
        {
            var settingName = sysSettingName.GetTitle();
            var result = await _sysSettingRepository.GetSysSettingAsync(settingName);

            if (result is null)
            {
                _logger.LogCritical($"{settingName} does not exist in the data base!");
            }

            return result;
        }

        public async Task UpdateSettingValueAsync(SysSettingName sysSettingName, string value)
        {
            var setting = await GetSettingAsync(sysSettingName);
            setting.Value = value;

            await _sysSettingRepository.SaveAsync();
        }

        public async Task UpdateSettingValue(SysSetting sysSetting, string value)
        {
            sysSetting.Value = value;
            await _sysSettingRepository.SaveAsync();
        }

        public async Task<DateTimeOffset?> GetLastCalcDataDateAsync()
        {
            var setting = await GetSettingAsync(SysSettingName.LastCalcDataUpdateDate);

            if (setting.Value is null)
            {
                return DateTimeOffset.Now;
            }

            DateTimeOffset.TryParse(setting.Value, out DateTimeOffset resultDate);

            if (resultDate == DateTimeOffset.MinValue)
            {
                _logger.LogCritical($"{setting.Name} has invalid date value!");
            }

            return resultDate;
        }

        public async Task<DateTimeOffset?> GetSettingDateValueAsync(SysSettingName sysSettingName)
        {
            var setting = await GetSettingAsync(sysSettingName);

            if (setting?.Value is null)
            {
                _logger.LogCritical($"{setting.Name} has no value. Service will return date.now!");
                return DateTimeOffset.Now;
            }

            DateTimeOffset.TryParse(setting.Value, out DateTimeOffset resultDate);

            if (resultDate == DateTimeOffset.MinValue)
            {
                _logger.LogCritical($"{setting.Name} has invalid date value!");
            }

            return resultDate;
        }
    }
}
