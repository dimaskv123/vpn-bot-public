﻿using Microsoft.Extensions.DependencyInjection;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.SubscribeEngine.Interfaces;

namespace VPN.BOT.Services.Services.SubscribeEngine;

public class SubscribeEngineFactory
{
    private readonly IEnumerable<ISubscribeEngine> _engines;

    public SubscribeEngineFactory(IEnumerable<ISubscribeEngine> engines)
    {
        _engines = engines;
    }

    public ISubscribeEngine Create(SubscribeTheoryMethod subscribeTheoryMethod)
    {
        return (subscribeTheoryMethod switch
        {
            SubscribeTheoryMethod.NoSubscribeTheoryMethod => null,
            SubscribeTheoryMethod.LimitByTotalGigabytes => _engines.First(x => x is ILimitByTotalGigabits),
            SubscribeTheoryMethod.LimitByTodayGigabytes => _engines.First(x => x is ILimitByTodayGigabits),
            _ => null
        })!; 
    }
}