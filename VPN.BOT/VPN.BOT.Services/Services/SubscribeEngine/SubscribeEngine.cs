﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using System.Security.Cryptography.X509Certificates;
using Telegram.Bot;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;
using VPN.BOT.Services.Services.SubscribeEngine.Interfaces;

namespace VPN.BOT.Services.Services.SubscribeEngine;

public abstract class SubscribeEngine
{
    protected IUserRepository _userRepository;
    protected IUserSettingsRepository _userSettingsRepository;
    protected IRestrictionsHistoryRepository _restrictionsHistoryRepository;
    protected ITelegramBotClient _botClient;
    protected IVpnServerConnector _vpnServerConnector;
    protected ILogger<SubscribeEngine> _logger;

    protected SubscribeEngine(
        IUserRepository repository,
        ITelegramBotClient botClient,
        IRestrictionsHistoryRepository restrictionsHistoryRepository,
        IVpnServerConnector vpnServerConnector,
        IUserSettingsRepository userSettingsRepository,
        ILogger<SubscribeEngine> logger)
    {
        _userRepository = repository;
        _botClient = botClient;
        _restrictionsHistoryRepository = restrictionsHistoryRepository;
        _vpnServerConnector = vpnServerConnector;
        _usersToMark = Enumerable.Empty<User>();
        _usersMarked = Enumerable.Empty<User>();
        _users = Enumerable.Empty<User>();
        _logger = logger;
        _userSettingsRepository = userSettingsRepository;
    }

    protected int MaxUsers { get; set; }
    protected SubscribeTheoryMethod SubscribeTheoryMethod { get; set; }
    protected DateTimeOffset BanPeriod { get; set; }

    protected IEnumerable<User> _usersToMark;
    protected IEnumerable<User> _usersToUnBan;
    protected IEnumerable<User> _usersMarked;
    protected IEnumerable<User> _users;
    private IUserRepository repository;
    private ITelegramBotClient botClient;
    private IRestrictionsHistoryRepository restrictionsHistoryRepository;
    private IVpnServerConnector vpnServerConnector;

    public virtual async Task Init()
    {
        _users = (await _userRepository.GetIncludeDeepConfigsAsync(
            x => !x.UserSettings.VIP && x.EndDate == null && !x.Configs.Any(x => x.IsValid && x.Payments.Any(p => p.Status == PaymentStatus.Succeeded)) && x.Configs.Any(x => x.IsValid), 
            false, 
            u=> u.UserSettings));
        await SetUpUsersToBan();
        await SetUpUsersToUnBan();
    }

    public async Task RestrictUsers()
    {
        if (_usersMarked.Count() < MaxUsers)
        {
            var markedUsersCount = 0;

            foreach (var notBannedUser in _usersMarked)
            {
                if (notBannedUser.Configs.Any(x => x.IsValid) || (notBannedUser.UserSettings.SubscribeTheoryMethod == SubscribeTheoryMethod && !notBannedUser.Configs.Any(x => x.IsValid && x.RestrictionsHistories.Any())))
                {
                    if (_usersMarked.All(x => x.Id != notBannedUser.Id))
                    {
                        _usersToMark = _usersToMark.Append(notBannedUser);
                    }
                }
            }

            foreach (var user in _usersToMark)
            {
                if (markedUsersCount + _usersMarked.Count() > MaxUsers)
                {
                    break;
                }

                try
                {
                    if (user.Configs.Any(x => x.IsValid))
                    {
                        var validConfig = user.Configs.First(x => x.IsValid);

                        var lastUnbannedConfig = validConfig.RestrictionsHistories.Where(x => x.EndTime != null).OrderByDescending(x => x.EndTime).FirstOrDefault();

                        if (lastUnbannedConfig != null && lastUnbannedConfig.EndTime.Value.Day == DateTimeOffset.Now.Day && (DateTimeOffset.Now - lastUnbannedConfig.EndTime.Value).TotalHours < 6 )
                        {
                            continue;
                        }

                        await Restrict(user);
                        await _restrictionsHistoryRepository.AddAsync(new RestrictionsHistory()
                        {
                            UserConfigId = validConfig.Id,
                            RestrictionType = SubscribeTheoryMethod,
                            UnbanTime = BanPeriod,
                            StartTime = DateTimeOffset.Now
                        });


                        user.UserSettings.SubscribeTheoryMethod = SubscribeTheoryMethod;

                        markedUsersCount++;
                        _usersMarked = _usersMarked.Append(user);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogCritical($"TOTAL ban impossible: {ex.Message} {ex.StackTrace}");
                    // logger
                }

                await _userRepository.UpdateAsync(user);
                await _userRepository.SaveAsync();
                await _restrictionsHistoryRepository.SaveAsync();
            }
        }
    }

    public async Task UnbanUsers()
    {
        //_logger.LogCritical($"Количество которое нужно разбанитт - {_usersToUnBan.Count()}");
        foreach (var user in _usersToUnBan)
        {
            var lastRestriction =
                await _restrictionsHistoryRepository.GetLastRestrictionAsync(user.Configs.First(x => x.IsValid).Id);
            try
            {
                var usrCfg = user.Configs.First(x => x.IsValid);
                _logger.LogCritical(
                    $"Сейчас буду разбанивать ключ - cfgId: {usrCfg.Id} {usrCfg.VpnKey} {usrCfg.Server.BaseUrl}");
                await Unban(user);

                lastRestriction.EndTime = DateTimeOffset.Now;
                await _restrictionsHistoryRepository.UpdateAsync(lastRestriction);
                await _userRepository.UpdateAsync(user);
                await _userSettingsRepository.UpdateAsync(user.UserSettings);
                
                _logger.LogCritical($"Разбанил ключ - cfgId: {usrCfg.Id} {usrCfg.VpnKey} {usrCfg.Server.BaseUrl}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while unban userId {user.Id}");
            }
            finally
            {
                await _userRepository.SaveAsync();
                await _restrictionsHistoryRepository.SaveAsync();
            }
        }
    }

    public async Task RestrictUser(User user, bool notifyUser = true)
    {
        try
        {
            if (user.Configs.Any(x => x.IsValid))
            {
                await Restrict(user);

                await _restrictionsHistoryRepository.AddAsync(new RestrictionsHistory()
                {
                    UserConfigId = user.Configs.First(x => x.IsValid).Id,
                    RestrictionType = SubscribeTheoryMethod,
                    UnbanTime = BanPeriod,
                    StartTime = DateTimeOffset.Now
                });

                user.UserSettings.SubscribeTheoryMethod = SubscribeTheoryMethod;
            }

            _usersMarked = _usersMarked.Append(user);
        }
        catch (Exception ex)
        {
            user.UserSettings.SubscribeTheoryMethod = SubscribeTheoryMethod.Error;
            user.UserSettings.SubscribeTheoryMethodError = $"{this.GetType().Name} error: {ex.Message}";
            // logger
        }

        await _userRepository.UpdateAsync(user);
        await _userRepository.SaveAsync();
    }
    
    public async Task UnbanUser(User user, bool notifyUser = true)
    {
        try
        {
            await Unban(user, notifyUser);

            var lastRestriction = await _restrictionsHistoryRepository.GetLastRestrictionAsync(user.Configs.First(x => x.IsValid).Id);
            if (lastRestriction != null)
            {
                lastRestriction.EndTime = DateTimeOffset.Now;
                await _restrictionsHistoryRepository.UpdateAsync(lastRestriction);
                await _userRepository.SaveAsync();
            }
        }
        catch (Exception ex)
        {
            user.UserSettings.SubscribeTheoryMethod = SubscribeTheoryMethod.UnbanError;
            user.UserSettings.SubscribeTheoryMethodError = $"{this.GetType().Name} error: {ex.Message}";
        }
        
        await _userRepository.UpdateAsync(user);
        await _userRepository.SaveAsync();
    }
    
    protected abstract Task Restrict(User user, bool notifyUser = true);

    protected abstract Task Unban(User user, bool notifyUser = true);
    
    protected abstract Task SetUpUsersToBan();
    protected abstract Task SetUpUsersToUnBan();
}