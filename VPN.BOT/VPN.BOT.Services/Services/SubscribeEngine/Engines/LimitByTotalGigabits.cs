﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using Telegram.Bot;
using VPN.BOT.Common;
using VPN.BOT.Common.Constants;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;
using VPN.BOT.Services.Services.SubscribeEngine.Interfaces;

namespace VPN.BOT.Services.Services.SubscribeEngine;

public class LimitByTotalGigabits : SubscribeEngine, ILimitByTotalGigabits
{

    private readonly IScheduleMessageRepository _scheduleMessageRepository;
    private readonly IMessageService _messageService;

    private readonly int LimitByTotalGigabitsValue = Constants.LimitConstants.TOTAL_GIGABYTES;
    
    public override async Task Init()
    {
        MaxUsers = 10000;

        SubscribeTheoryMethod = SubscribeTheoryMethod.LimitByTotalGigabytes;

        await base.Init();
    }

    protected override async Task Unban(User user, bool notifyUser = true)
    {
        await _scheduleMessageRepository.MarkMessagesAsSentAsync(user.Id);
        
        user.UserSettings.SubscribeTheoryMethod = SubscribeTheoryMethod.NoSubscribeTheoryMethod;
        
        await _scheduleMessageRepository.SaveAsync();
    }

    protected override async Task Restrict(User user, bool notifyUser = true)
    {
        BanPeriod = user.Configs.First(x => x.IsValid).EndDate;

        await _vpnServerConnector.DeleteCertificateAsync(user.Configs.First(x => x.IsValid));

        if (notifyUser)
        {
            await _messageService.SendMessageAsync(MessageName.LimitByTotal, user);

            for (int i = 1; i < (BanPeriod - DateTimeOffset.Now).Days; i++)
            {
                await _scheduleMessageRepository.AddAsync(new ScheduleMessage()
                {
                    ToUserId = user.Id,
                    UpdateKeyBoard = false,
                    PlannedTimeSend = DateTimeOffset.Now.AddDays(i),
                    IsSent = false,
                    TextMessage = string.Format(MessagesText.LimitByTotalGigabitsRepeatNotifyMessage, user.Configs.First(x => x.IsValid).EndDate.ToString("dd.MM.yyyy"))
                });
            }
        }
    }

    protected override Task SetUpUsersToUnBan()
    {
        _usersToUnBan = _users
            .Where(x =>
            {
                var validKey = x.Configs.FirstOrDefault(x => x.IsValid);

                if (validKey is null)
                {
                    return false;
                }

                var lastRestriction = validKey.RestrictionsHistories.MaxBy(x => x.StartTime);

                return lastRestriction is not null
                       && lastRestriction.RestrictionType == SubscribeTheoryMethod.LimitByTotalGigabytes
                       && lastRestriction.UnbanTime <= DateTimeOffset.Now
                       && lastRestriction.EndTime is null;
            });
        return Task.CompletedTask;
    }

    protected override Task SetUpUsersToBan()
    {
        _usersToMark = _users
            .OrderByDescending(x => x.StartDate)
            .Where(x => x.Configs.Count > 1 && x.Configs.Any(x => x.IsValid && x.TotalGigabytes > LimitByTotalGigabitsValue))
            .Where(x =>
            {
                var validConfig = x.Configs.FirstOrDefault(x => x.IsValid);
                
                if (!validConfig.RestrictionsHistories.Any())
                {
                    return true;
                }

                var lastRestriction = validConfig.RestrictionsHistories.MaxBy(x => x.StartTime);
                
                return lastRestriction.EndTime != null;
            })
            .Take(MaxUsers - _usersMarked.Count());

        return Task.CompletedTask;
    }

    public LimitByTotalGigabits(IUserRepository repository, ITelegramBotClient botClient, IRestrictionsHistoryRepository restrictionsHistoryRepository, IVpnServerConnector vpnServerConnector, IUserSettingsRepository userSettingsRepository, ILogger<SubscribeEngine> logger, IMessageService messageService, IScheduleMessageRepository scheduleMessageRepository) : base(repository, botClient, restrictionsHistoryRepository, vpnServerConnector, userSettingsRepository, logger)
    {
        _messageService = messageService;
        _scheduleMessageRepository = scheduleMessageRepository;
    }
}