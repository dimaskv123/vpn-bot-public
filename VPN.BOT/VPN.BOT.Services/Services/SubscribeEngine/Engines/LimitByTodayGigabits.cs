﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using VPN.BOT.Common;
using VPN.BOT.Common.Constants;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;
using VPN.BOT.Services.Services.SubscribeEngine.Interfaces;

namespace VPN.BOT.Services.Services.SubscribeEngine;

public class LimitByTodayGigabits : SubscribeEngine, ILimitByTodayGigabits
{
    private readonly IMessageService _messageService;

    public LimitByTodayGigabits(IUserRepository repository, ITelegramBotClient botClient, IRestrictionsHistoryRepository restrictionsHistoryRepository, IVpnServerConnector vpnServerConnector, IUserSettingsRepository userSettingsRepository, ILogger<SubscribeEngine> logger, IMessageService messageService) : base(repository, botClient, restrictionsHistoryRepository, vpnServerConnector, userSettingsRepository, logger)
    {
        _messageService = messageService;
    }
    
    public override async Task Init()
    {
        MaxUsers = 10_000;

        SubscribeTheoryMethod = SubscribeTheoryMethod.LimitByTodayGigabytes;

        BanPeriod = DateTimeOffset.UtcNow.AddDays(1).Date;

        await base.Init();
    }
    
    protected override async Task Restrict(User user, bool notifyUser = true)
    {
        await _vpnServerConnector.LimitKey(user.Configs.First(x => x.IsValid));

        if (notifyUser)
        {
            await _messageService.SendMessageAsync(MessageName.LimitByToday, user);
        }
    }

    protected override async Task Unban(User user, bool notifyUser = true)
    {
        await _vpnServerConnector.UnLimitKey(user.Configs.First(x => x.IsValid));
        if (notifyUser)
        {
            await _botClient.SendTextMessageAsync(chatId: user.TelegramId,
                text: string.Format(MessagesText.UnBlockByTodayGigabitsMessage, user.UserSettings.PersonalTodayLimit),
                parseMode: ParseMode.Markdown);
        }

        user.UserSettings.SubscribeTheoryMethod = SubscribeTheoryMethod.NoSubscribeTheoryMethod;
    }

    protected override Task SetUpUsersToUnBan()
    {
        _usersToUnBan = _users
            .Where(x =>
            {
                var validKey = x.Configs.FirstOrDefault(x => x.IsValid);

                if (validKey is null)
                {
                    return false;
                }

                var lastRestriction = validKey.RestrictionsHistories.MaxBy(x => x.StartTime);
                return lastRestriction is not null &&
                       lastRestriction.RestrictionType == SubscribeTheoryMethod.LimitByTodayGigabytes
                       && lastRestriction.UnbanTime.Value <= DateTimeOffset.Now 
                       && lastRestriction.EndTime is null;
            });

        return Task.CompletedTask;
    }

    protected override Task SetUpUsersToBan()
    {
        _usersToMark = _users
            .OrderByDescending(x => x.StartDate)
            .Where(u => u.Configs.Count > 1 && u.Configs.Any(uc => uc.IsValid && uc.TodayGigabytes > u.UserSettings.PersonalTodayLimit))
            .Where(x =>
            {
                var validConfig = x.Configs.FirstOrDefault(x => x.IsValid);
                
                if (!validConfig.RestrictionsHistories.Any())
                {
                    return true;
                }

                var lastRestriction = validConfig.RestrictionsHistories.MaxBy(x => x.StartTime);

                return lastRestriction.EndTime != null;
            })
            .Take(MaxUsers - _usersMarked.Count());

        return Task.CompletedTask;
    }
}