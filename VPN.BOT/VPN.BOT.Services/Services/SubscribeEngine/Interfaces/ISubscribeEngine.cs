﻿using Domain.Entity;

namespace VPN.BOT.Services.Services.SubscribeEngine.Interfaces;

public interface ISubscribeEngine
{
    Task Init();

    Task RestrictUsers();

    Task UnbanUsers();
    Task UnbanUser(User user, bool notifyUser = true);
    Task RestrictUser(User user, bool notifyUser = true);
}