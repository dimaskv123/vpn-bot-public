﻿using Domain.Entity;
using Repository.Interfaces;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services
{
    public class ReferalService : IReferalService
    {
        private readonly IReferalRepository _referalRepository;
        public ReferalService
            (
            IReferalRepository referalRepository
            )
        {
            _referalRepository = referalRepository;
        }

        public async Task<Referal> FindByInviteCodeAsync(string code)
        {
            return await _referalRepository.FindByCodeAsync(code);
        }

        public async Task<Referal> FindByUserCreatedAsync(int id)
        {
            return await _referalRepository.FindByUserAsync(id);
        }

        public string GenerateInviteCode(User user)
        {
            var inviteCode = user.UserName == null ? user.TelegramId.ToString() : user.UserName;
            return inviteCode;
        }
    }
}
