﻿using Repository.Interfaces;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services
{
    public class RestrictionService : IRestrictionService
    {
        private readonly IRestrictionsHistoryRepository _restrictionsHistoryRepository;
        private readonly IUserService _userService;
        public RestrictionService(
            IRestrictionsHistoryRepository restrictionsHistoryRepository,
            IUserService userService)
        {
            _restrictionsHistoryRepository = restrictionsHistoryRepository;
            _userService = userService;
        }

        public async Task MarkUnbanLastUserRestrictionAsync(long userId)
        {
            var user = await _userService.FindByTelegramIdAsync(userId);

            if (user == null)
            {
                throw new ArgumentException($"No user with Id {userId}");
            }

            var lastValidConfig = user.Configs.OrderByDescending(c => c.EndDate).FirstOrDefault(c => c.IsValid);

            if (lastValidConfig == null)
            {
                throw new ArgumentException($"No configs from user Id {userId}");
            }
            var lastRestriction = await _restrictionsHistoryRepository.GetLastRestrictionAsync(lastValidConfig.Id);

            if (lastRestriction == null || lastRestriction.EndTime != null)
            {
                return;
            }

            lastRestriction.UnbanTime = DateTimeOffset.UtcNow;

            await _restrictionsHistoryRepository.SaveAsync();
        }
    }
}
