﻿using Domain.Entity;
using Microsoft.Extensions.Configuration;
using Repository.Interfaces;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services
{
    public class UserReferalService : IUserReferalService
    {
        private readonly IUserRepository _userRepository;
        private readonly IReferalRepository _referalRepository;
        private readonly IReferalService _referalService;
        private readonly IConfiguration _configuration;

        public UserReferalService
        (
            IUserRepository userRepository,
            IReferalRepository referalRepository,
            IReferalService referalService,
            IConfiguration configuration
        )
        {
            _userRepository = userRepository;
            _referalRepository = referalRepository;
            _referalService = referalService;
            _configuration = configuration;
        }

        public async Task<string> GetInviteCodeAsync(User user)
        {
            if (user == null)
                new ArgumentNullException("User is empty!");

            var botUrl = _configuration.GetSection("BotUrl").Value;
            var inviteLink = "?start={0}";

            if (botUrl == null)
            {
                throw new ArgumentNullException("BotUrl is empty!");
            }

            Referal referal = await _referalService.FindByUserCreatedAsync(user.Id);
            if (referal != null)
            {
                return botUrl + string.Format(inviteLink, referal.InviteCode);
            }

            var newCode = _referalService.GenerateInviteCode(user);
            referal = new Referal()
            {
                InviteCode = newCode,
                CreatedById = user.Id
            };

            await _referalRepository.AddAsync(referal);
            await _referalRepository.SaveAsync();

            var result = botUrl + string.Format(inviteLink, referal.InviteCode);
            return result;
        }

        public async Task RegisterReferalUserAsync(long invitedUserId, string referalId)
        {
            var invitedUser = await _userRepository.FindByTelegramIdIncludeDeepConfigsAsync(invitedUserId);
            if (invitedUser.InvitedById != null)
                return;

            var referal = await _referalService.FindByInviteCodeAsync(referalId);
            if (referal == null)
                return;

            if (referal?.CreatedById == invitedUser.Id)
            {
                return;
            }

            invitedUser.InvitedById = referal.Id;

            await _userRepository.SaveAsync();
        }

        public async Task<int> GetInvitedAmountByUserAsync(int id)
        {
            var referal = await _referalRepository.FindByUserAsync(id);

            if (referal == null) return 0;

            var result = (await _userRepository.Find(user => user.InvitedById == referal.Id)).Count();

            return result;
        }
    }
}
