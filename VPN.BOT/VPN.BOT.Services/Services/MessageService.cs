﻿using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using VPN.BOT.Common;
using VPN.BOT.Common.Constants;
using VPN.BOT.Common.Enums;
using VPN.BOT.Common.Utils;
using VPN.BOT.Services.Services.Interfaces;
using static VPN.BOT.Common.Constants.Constants;
using User = Domain.Entity.User;

namespace VPN.BOT.Services.Services
{
    public class MessageService : IMessageService
    {
        private readonly ITelegramBotClient _botClient;
        private readonly ILogger<MessageService> _logger;
        public MessageService(
                ITelegramBotClient botClient,
                ILogger<MessageService> logger)
        {
            _botClient = botClient;
            _logger = logger;
        }

        public async Task<Message> SendMessageAsync(MessageName messageName, User user)
        {
            Message message = null;

            try
            {
                message = messageName switch
                {
                    MessageName.LimitByTotal => await SendLimitTotalMessageAsync(user),
                    MessageName.LimitByToday => await SendLimitTodayMessageAsync(user),
                    MessageName.Help => await SendHelpMessageAsync(user),
                    MessageName.Start => await SendStartMessageAsync(user),
                    MessageName.NoUserConfigTrigger => await NoUserConfigTriggerMessageAsync(user),
                    MessageName.UnCompletePayment => await UnCompletePaymentTriggerMessageAsync(user),
                    MessageName.NoDataUsage => await NoDataUsageTriggerMessageAsync(user),
                    MessageName.ConfigRetake => await ConfigRetakeTriggerMessageAsync(user),
                    MessageName.PaymentExpireIn3Days => await PaymentExpireIn3DaysTriggerMessageAsync(user),
                    MessageName.PaymentExpireIn1Day => await PaymentExpireIn1DayTriggerMessageAsync(user),
                    MessageName.CertificateExpire => await CertificateExpireMessageAsync(user),
                    MessageName.PayedCertificateExpire => await PayedCertificateExpireMessageAsync(user),
                    MessageName.TransitKeyFromEndServer => await SendTransitKeyMessageAsync(user),
                    _ => throw new ArgumentOutOfRangeException()
                };
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, $"Error while send message {messageName.ToString()} to user {user.Id} {user.UserName} {user.FirstName} {user.LastName}");
            }

            return message;
        }

        private async Task<Message> SendLimitTotalMessageAsync(User user)
        {
            var result = await _botClient.SendTextMessageAsync(chatId: user.TelegramId,
                     text: string.Format(
                         MessagesText.LimitByTotalGigabytesMessage,
                         LimitConstants.TOTAL_GIGABYTES,
                         user.Configs.First(x => x.IsValid).EndDate.AddHours(3).ToString("dd.MM.yyyy HH:mm:ss")), parseMode: ParseMode.Markdown);

            var offer = new InlineKeyboardButton("Перейти на PRO версию")
            {
                CallbackData = "/buyVpn"
            };

            var button = new[] { offer };

            var buttons = new[] { button };

            var markUp = new InlineKeyboardMarkup(buttons);

            //присылать картинку с тарифами

            await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.OfferMessage,
                parseMode: ParseMode.Markdown, replyMarkup: markUp);

            return result;
        }

        private async Task<Message> SendLimitTodayMessageAsync(User user)
        {
            var result = await _botClient.SendTextMessageAsync(chatId: user.TelegramId,
                text: string.Format(MessagesText.LimitByTodayGigabytesMessage, user.UserSettings.PersonalTodayLimit.ToMegabytes()),
                parseMode: ParseMode.Markdown);

            var offer = new InlineKeyboardButton("Перейти на PRO версию")
            {
                CallbackData = "/buyVpn"
            };

            var button = new[] { offer };

            var buttons = new[] { button };

            var markUp = new InlineKeyboardMarkup(buttons);

            //присылать картинку с тарифакми
            await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.OfferMessage,
                parseMode: ParseMode.Markdown, replyMarkup: markUp);

            return result;
        }

        private async Task<Message> SendHelpMessageAsync(User user)
        {
            var result = await _botClient.SendTextMessageAsync(
                chatId: user.TelegramId,
                text: user.Configs.Any(x => x.IsValid && x.Payments.Any(x => x.Status == PaymentStatus.Succeeded)) ?
                    MessagesText.HelpVipMessage :
                    MessagesText.HelpMessage,
                parseMode: ParseMode.Markdown,
                replyMarkup: await GenerateReplyKeyboardMarkup(user));

            return result;
        }

        private async Task<Message> SendStartMessageAsync(User user)
        {
            var getVpn = new InlineKeyboardButton("Получить VPN 🔑")
            {
                CallbackData = "/getvpn"
            };
            var firstRow = new[] { getVpn };

            var buttons = new[] { firstRow };

            var markUp = new InlineKeyboardMarkup(buttons);

            return await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.WelcomeMessage,
                parseMode: ParseMode.Markdown, replyMarkup: markUp);
        }

        private async Task<Message> NoUserConfigTriggerMessageAsync(User user)
        {
            var getVpn = new InlineKeyboardButton("Получить VPN 🔑")
            {
                CallbackData = "/getvpn"
            };

            var firstRow = new[] { getVpn };

            var buttons = new[] { firstRow };

            var markUp = new InlineKeyboardMarkup(buttons);

            return await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.OnNoConfigMessage,
                parseMode: ParseMode.Markdown, replyMarkup: markUp);
        }

        private async Task<Message> UnCompletePaymentTriggerMessageAsync(User user)
        {
            var buyVpn = new InlineKeyboardButton("Получить новую ссылку")
            {
                CallbackData = "/buyVpn"
            };

            var firstRow = new[] { buyVpn };

            var buttons = new[] { firstRow };

            // Keyboard markup
            var markUp = new InlineKeyboardMarkup(buttons);

            return await _botClient.SendTextMessageAsync(chatId: user.TelegramId,
                text: MessagesText.OnUncompletePayment,
                parseMode: ParseMode.Markdown);  //, replyMarkup: markUp);
        }

        private async Task<Message> NoDataUsageTriggerMessageAsync(User user)
        {
            if (user.Configs.FirstOrDefault(x => x.IsValid) == null)
            {
                throw new Exception("No valid user config");
            }

            InlineKeyboardMarkup markUp;

            var iosDownload = new InlineKeyboardButton("IOS")
            {
                Url = "https://itunes.apple.com/us/app/outline-app/id1356177741"
            };

            var androidDownload = new InlineKeyboardButton("Android")
            {
                Url = "https://play.google.com/store/apps/details?id=org.outline.android.client"
            };

            var macOsDownload = new InlineKeyboardButton("Mac OS")
            {
                Url = "https://itunes.apple.com/us/app/outline-app/id1356178125"
            };

            var winOsDownload = new InlineKeyboardButton("Windows")
            {
                Url = "https://s3.amazonaws.com/outline-releases/client/windows/stable/Outline-Client.exe"
            };

            var mobile = new[] { iosDownload, androidDownload };
            var desktop = new[] { winOsDownload, macOsDownload };

            var buttons = new[] { mobile, desktop };

            // Keyboard markup
            markUp = new InlineKeyboardMarkup(buttons);

            await _botClient.SendTextMessageAsync(chatId: user.TelegramId,
                text: "`" + user.Configs.FirstOrDefault(x => x.IsValid).VpnKey + "`", parseMode: ParseMode.Markdown);

            return await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.NoDataUsage,
                parseMode: ParseMode.Markdown, replyMarkup: markUp);
        }

        private async Task<Message> ConfigRetakeTriggerMessageAsync(User user)
        {
            var buyVpn = new InlineKeyboardButton("Получить VPN 🔑")
            {
                CallbackData = "/getVpn"
            };

            var firstRow = new[] { buyVpn };

            var buttons = new[] { firstRow };

            // Keyboard markup
            var markUp = new InlineKeyboardMarkup(buttons);

            return await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.ConfigRetake,
                parseMode: ParseMode.Markdown, replyMarkup: markUp);
        }

        private async Task<Message> CertificateExpireMessageAsync(User user)
        {
            return await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.RevokeSertificate,
                ParseMode.Markdown, replyMarkup: new InlineKeyboardMarkup(new InlineKeyboardButton("Получить VPN 🔑")
                {
                    CallbackData = "Получить VPN 🔑"
                }));
        }

        private async Task<Message> PayedCertificateExpireMessageAsync(User user)
        {
            return await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.RevokePayedCertificate,
                ParseMode.Markdown, replyMarkup: new InlineKeyboardMarkup(new InlineKeyboardButton("Получить безлимитный VPN")
                {
                    CallbackData = "/buyVpn"
                }));
        }

        private async Task<Message> PaymentExpireIn3DaysTriggerMessageAsync(User user)
        {
            var buyVpn = new InlineKeyboardButton("Продлить PRO версию")
            {
                CallbackData = "/buyVpn"
            };

            var firstRow = new[] { buyVpn };

            var buttons = new[] { firstRow };

            // Keyboard markup
            var markUp = new InlineKeyboardMarkup(buttons);

            return await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.PaymentExpireIn3Days,
                parseMode: ParseMode.Markdown, replyMarkup: markUp);
        }

        private async Task<Message> PaymentExpireIn1DayTriggerMessageAsync(User user)
        {
            var buyVpn = new InlineKeyboardButton("Продлить PRO версию")
            {
                CallbackData = "/buyVpn"
            };

            var firstRow = new[] { buyVpn };

            var buttons = new[] { firstRow };

            // Keyboard markup
            var markUp = new InlineKeyboardMarkup(buttons);

            return await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: string.Format(MessagesText.PaymentExpireIn1Day, user.Configs.First(x => x.IsValid).EndDate.ToString("dd.MM.yyyy HH:mm")),
                parseMode: ParseMode.Markdown, replyMarkup: markUp);
        }

        public async Task<ReplyKeyboardMarkup> GenerateReplyKeyboardMarkup(User user)
        {
            var firstKeyboardLine = new List<KeyboardButton>();

            firstKeyboardLine.Add(user.Configs.Any(x => x.IsValid && x.Payments.Any(x => x.Status == PaymentStatus.Succeeded))
                ? new KeyboardButton("Заменить ключ VPN 🔑")
                : new KeyboardButton("Получить VPN 🔑"));


            // вернуть когда вернем обновление ключа
            //if (await _certificateManger.DoesUserHaveValidCertificateAsync(_currentUser.TelegramId) != null)
            //{
            firstKeyboardLine.Add(new KeyboardButton("Политика использования 📱"));
            //}

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(new[]
            {
                firstKeyboardLine.ToArray(),
                new KeyboardButton[] { "Профиль 😎", "Помощь 🚑" },
                new KeyboardButton[] { "Реферальная программа 🤝" },
            });

            replyKeyboardMarkup.ResizeKeyboard = true;

            return replyKeyboardMarkup;
        }

        private async Task<Message> SendTransitKeyMessageAsync(User user)
        {
            var result = await _botClient.SendTextMessageAsync(
                chatId: user.TelegramId,
                text:
                    MessagesText.TransitMessage,
                parseMode: ParseMode.Markdown);


            return result;
        }
    }
}
