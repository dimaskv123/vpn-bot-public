﻿using Domain.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using VPN.BOT.Common;
using VPN.BOT.Common.Constants;
using VPN.BOT.Common.Enums;
using VPN.BOT.Common.Utils;
using VPN.BOT.Services.ExternalService.Yookassa.Interfaces;
using VPN.BOT.Services.Services.Interfaces;
using Action = VPN.BOT.Common.Enums.Action;
using User = Domain.Entity.User;

namespace VPN.BOT.Services.Services;

public class HandleUpdateService
{
    private readonly ITelegramBotClient _botClient;
    private readonly ICertificateManger _certificateManger;
    private readonly IUserRepository _userRepository;
    private readonly IUserService _userService;
    private readonly ILogger<HandleUpdateService> _logger;
    private readonly IConfiguration _configuration;
    private readonly IUserReferalService _userReferalService;
    private readonly IYookassaClientManager _yookassaClientManager;
    private readonly IPaymentRepository _paymentRepository;
    private readonly IMessageService _messageService;
    private readonly IRestrictionService _restrictionService;
    private readonly ISysSettingService _sysSettingService;

    private User _currentUser;
    private DateTimeOffset _markUpUpdateDate;

    private const string NEGRIME_ID = "254724042";

    public HandleUpdateService
        (
        ITelegramBotClient botClient,
        ILogger<HandleUpdateService> logger,
        ICertificateManger certificateManger,
        IUserRepository userRepository,
        IConfiguration configuration,
        IUserService userService,
        IUserReferalService userReferalService,
        IYookassaClientManager yookassaClientManager,
        IPaymentRepository paymentRepository,
        IMessageService messageService,
        IRestrictionService restrictionService,
        ISysSettingService sysSettingService)
    {
        _botClient = botClient;
        _logger = logger;
        _certificateManger = certificateManger;
        _userRepository = userRepository;
        _currentUser = null;
        _configuration = configuration;
        _userService = userService;
        _userReferalService = userReferalService;
        _yookassaClientManager = yookassaClientManager;
        _paymentRepository = paymentRepository;
        _messageService = messageService;
        var markUpUpdateDateString = _configuration.GetSection("MarkUpUpdateDate").Value;
        _markUpUpdateDate = DateTimeOffset.Parse(markUpUpdateDateString);
        _restrictionService = restrictionService;
        _sysSettingService = sysSettingService;
    }

    public async Task EchoAsync(Update update)
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        var handler = update.Type switch
        {
            // UpdateType.Unknown:
            // UpdateType.ChannelPost:
            // UpdateType.EditedChannelPost:
            // UpdateType.ShippingQuery:
            // UpdateType.PreCheckoutQuery:
            // UpdateType.Poll:
            UpdateType.Message => BotOnMessageReceived(update.Message!),
            UpdateType.CallbackQuery => BotOnCallBackReceived(update.CallbackQuery!),
            UpdateType.MyChatMember => BotChatMemberReceived(update.MyChatMember),
            _ => UnknownUpdateHandlerAsync(update)
        };

        try
        {
            await handler;
        }
        catch (Exception exception)
        {
            await HandleErrorAsync(exception);
        }
        stopwatch.Stop();
        var seconds = TimeSpan.FromMilliseconds(stopwatch.ElapsedMilliseconds).TotalSeconds;
        var message = (update.Message?.Text ?? update.CallbackQuery?.Data) ?? string.Empty;
        if (seconds > 10)
        {
            _logger.LogWarning($"Message {message} with type {update.Type} took {seconds}");
        }
    }

    private async Task BotOnMessageReceived(Message message)
    {
        if (message.Type == MessageType.MessagePinned)
        {
            return;
        }

        //var timeSpan = DateTime.Now.AddHours(-3) - message.Date;
        //if (timeSpan.TotalSeconds > 5)
        //{
        //    return;
        //}

        _currentUser = await GetOrAddUser(message);

        var stateCheckResult = await CheckUserChatStateAsync(_currentUser, message);
        if (stateCheckResult != ChatState.Default)
        {
            return;
        }

        var commandParams = message?.Text?.TrimStart().TrimEnd().Split(" ")?.ToList();
        var command = message.Text;

        if (message.Entities != null && message.Entities.Length > 0 && message.Entities[0].Type == MessageEntityType.BotCommand)
        {
            command = commandParams[0];
            commandParams.RemoveAt(0);
        }

        var executeAction = ParseMessageToAction(command);

        if ((message.ForwardFrom != null || message.Text.Contains("ss://")) && Constants.AdminIds.Contains(message.From.Id))
        {
            executeAction = Action.Admin;
        }

        if (!Constants.AdminIds.Contains(message.From.Id) && executeAction == Action.ReshuffleUsers)
        {
            executeAction = Action.Unknown;
        }

        var action = executeAction switch
        {
            Action.Start => OnStart(message, commandParams),
            Action.GetVpn => OnGetVpn(message),
            Action.UpdateVpn => OnUpdateVpn(message),
            Action.Rules => OnRulesVpn(message.From.Id),
            Action.Profile => OnProfile(message),
            Action.Help => OnHelp(message),
            Action.Referral => OnReferral(message),
            Action.Test => OnTest(message),
            Action.Unknown => OnHelp(message),
            Action.BuyVpn => OnBuyVpn(message),
            Action.Admin => OnAdmin(message),
            Action.Menu => OnMenuKeyboard(message),
            Action.SetEmail => OnSetEmail(message),
            Action.ReshuffleUsers => ReshuffleUsersMessage(message),
            _ => throw new ArgumentOutOfRangeException()
        };

        await action;

        _currentUser.LastSentMessageDate = DateTimeOffset.Now;

        await _userRepository.SaveAsync();

        // todo: сохранить в БД id послднего сообщения отправленного юзеру
    }

    private async Task BotOnCallBackReceived(CallbackQuery callbackQuery)
    {
        // todo: полчить юезра из БД, удалить последнее сообщение,если оно есть 
        // пример удаления

        _currentUser = await GetOrAddUser(new Message()
        {
            From = callbackQuery.From
        });

        try
        {
            await _botClient.DeleteMessageAsync(chatId: callbackQuery.From.Id, callbackQuery.Message.MessageId);
        }
        catch (Exception e)
        {
            // ignore
        }

        var executeAction = ParseMessageToAction(callbackQuery.Data);

        var action = executeAction switch
        {

            Action.GetVpn => OnGetVpn(callbackQuery),
            Action.UpdateVpn => OnUpdateVpn(callbackQuery),
            Action.Rules => OnRulesVpn(callbackQuery.Message.From.Id),
            Action.BuyVpn => OnBuyVpn(callbackQuery),
            Action.ProcessPayment => ProcessPayment(callbackQuery),
            Action.Unban => Unban(callbackQuery),
            Action.ChangeServer => ChangeServer(callbackQuery),
            Action.SetEmail => OnSetEmail(callbackQuery),
            Action.ReshuffleUsers => ReshuffleUsersFromServer(callbackQuery),
            _ => throw new ArgumentOutOfRangeException($"Unknown comman - {callbackQuery.Data}")
        };

        var sentMessage = await action;

        _currentUser.LastSentMessageDate = DateTimeOffset.Now;

        await _userRepository.SaveAsync();

        // todo: сохранить в БД id послднего сообщения отправленного юзеру
    }

    private async Task BotChatMemberReceived(ChatMemberUpdated chatMemberUpdated)
    {
        _currentUser = await _userRepository.FindByTelegramIdAsync(chatMemberUpdated.From.Id);

        var action = chatMemberUpdated.NewChatMember.Status switch
        {
            ChatMemberStatus.Kicked => OnChatStopped(chatMemberUpdated),
            ChatMemberStatus.Left => OnChatStopped(chatMemberUpdated),
            _ => Task.CompletedTask
        };

        await action;
    }

    private async Task OnChatStopped(ChatMemberUpdated chatMemberUpdated)
    {
        _currentUser.EndDate = DateTimeOffset.Now;

        if (_currentUser.Configs.Any(x => x.IsValid && !x.Payments.Any() && !x.RestrictionsHistories.Any(r => r.EndTime == null)))
        {
            await _certificateManger.RevokeUserCertificateAsync(_currentUser);
        }

        await _userRepository.UpdateAsync(_currentUser);
        await _userRepository.SaveAsync();
    }

    private async ValueTask<ChatState> CheckUserChatStateAsync(User user, Message message)
    {
        var userSate = (ChatState)user.ChatState;

        switch (userSate)
        {
            case ChatState.SetEmail:
                var validationResult = await ValidateEmailAsync(user, message);
                return ChatState.SetEmail;

            case ChatState.Default:
                break;
        }

        return ChatState.Default;
    }

    private async Task<bool> ValidateEmailAsync(User user, Message message)
    {
        var email = message.Text.Trim();
        EmailAddressAttribute emailCheck = new EmailAddressAttribute();
        var isEmailValid = emailCheck.IsValid(email);

        string answerMessage = string.Empty;

        if (!isEmailValid)
        {
            var repeat = new InlineKeyboardButton("✅")
            {
                CallbackData = "/setEmail -repeat"
            };

            var cancel = new InlineKeyboardButton("❌")
            {
                CallbackData = "/setEmail -cancel"
            };

            var buttons = new[] { repeat, cancel };
            var markUp = new InlineKeyboardMarkup(buttons);


            answerMessage = "Некорректная электронная почта. Попробуем еще раз?";
            await _botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                text: answerMessage,
                replyMarkup: markUp,
                parseMode: ParseMode.Markdown);

            return false;
        }

        await _userService.UpdateEmailAsync(user, email);
        await _userService.UpdateChatStateAsync(user, ChatState.Default);
        await _userService.SaveAsync();
        answerMessage = "Электронная почта успешно установлена.";

        await _botClient.SendTextMessageAsync(chatId: message.Chat.Id,
            text: answerMessage,
            parseMode: ParseMode.Markdown);
        return true;
    }

    private async Task<Message> OnHelp(Message message)
    {
        return await _messageService.SendMessageAsync(MessageName.Help, _currentUser);
    }

    private async Task<Message> OnTest(Message message)
    {
        ReplyKeyboardMarkup replyKeyboardMarkup = new(new[]
        {
            new KeyboardButton[] { "Help me" },
            new KeyboardButton[] { "Call me ☎️" },
        })
        {
            ResizeKeyboard = true
        };

        return await _botClient.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: "123",
            replyMarkup: replyKeyboardMarkup);
    }

    private Task<Message> OnProfile(Message message)
    {
        // получить данные из БД о юзере в том числе тип подписка

        string textToSend = string.Empty;
        var currentKey = _currentUser.Configs.LastOrDefault(c => c.IsValid);

        var restriction = currentKey?.RestrictionsHistories.LastOrDefault(x => x.EndTime == null);

        string restrictionInfo = string.Empty;

        if (restriction != null)
        {
            var restrinctMessage = restriction.RestrictionType == SubscribeTheoryMethod.LimitByTotalGigabytes
                                                                ? string.Format(MessagesText.LimitByTotalGigabytesMessage, Constants.LimitConstants.TOTAL_GIGABYTES, restriction.UnbanTime?.ToString("dd.MM.yyyy HH:mm:ss"))
                                                                : string.Format(MessagesText.LimitByTodayGigabytesMessage, _currentUser.UserSettings.PersonalTodayLimit.ToMegabytes());
            restrictionInfo = Environment.NewLine + restrinctMessage + Environment.NewLine + "*Для покупки безлимитного VPN на выделенном сервере отправьте боту команду* /buyVpn";
        }

        var extra = currentKey != null
            ? string.Format(@$"
Срок работы ключа:
*{currentKey.EndDate.ToString("dd.MM.yyyy HH:mm:ss")}*")
            : string.Empty;

        var email = "\n\n*Почта:* " + (string.IsNullOrEmpty(_currentUser.Email) ? "почта не задана" : _currentUser.Email);
        var key = currentKey != null ? string.Format("`{0}`", currentKey.VpnKey) : "отсутствует";

        textToSend = string.Format(MessagesText.ProfileMessage, message.Chat.FirstName, key) + extra + restrictionInfo + email;
        textToSend += Environment.NewLine + MessagesText.EmailInstruction;


        return _botClient.SendTextMessageAsync(chatId: message.Chat.Id,
            text: textToSend, ParseMode.Markdown);

    }

    private Task<Message> Help(Message message)
    {
        var commands = "Список доступных команд:" + Environment.NewLine
                                                  + "/start - перезапустить" + Environment.NewLine
                                                  + "/getVpn - получить ВПН" + Environment.NewLine;
        //+ "/UpdateVpn - обновить ключ ВПН" + Environment.NewLine;

        return _botClient.SendTextMessageAsync(chatId: message.Chat.Id, text: commands);
    }

    private async Task<Message> OnStart(Message message, List<string> parameters)
    {
        var referal = parameters.FirstOrDefault();

        if (referal != null)
        {
            await _userReferalService.RegisterReferalUserAsync(message.Chat.Id, referal);
        }

        return await _messageService.SendMessageAsync(MessageName.Start, _currentUser);
    }

    private async Task<Message> OnMenuKeyboard(Message message)
    {
        return await _botClient.SendTextMessageAsync(chatId: _currentUser.TelegramId, text: "Меню с кнопками представлены ниже.", parseMode: ParseMode.Markdown, replyMarkup: await _messageService.GenerateReplyKeyboardMarkup(_currentUser));
    }

    private Task<Message> OnBuyVpn(Message message)
    {
        return OnBuyVpn(_currentUser);
    }

    private async Task<Message> OnSetEmail(Message message)
    {
        var user = _currentUser;

        if ((ChatState)user.ChatState != ChatState.SetEmail)
        {
            await _userService.UpdateChatStateAsync(user, ChatState.SetEmail);
        }

        var sentMessage = await _botClient.SendTextMessageAsync(chatId: message.Chat.Id,
            text: $"Отправьте в чат Вашу почту.\nПример сообщения:\nivanivanov@mail.ru.",
            parseMode: ParseMode.Markdown);

        return sentMessage;
    }

    private async Task<Message> OnSetEmail(CallbackQuery callbackQuery)
    {
        var user = _currentUser;

        var chunks = callbackQuery.Data.Split(" ");

        var setEmail = false;

        foreach (var chunk in chunks)
        {
            setEmail = chunk.StartsWith("-repeat");
        }

        if (!setEmail)
        {
            await _userService.UpdateChatStateAsync(user, ChatState.Default);
            await _userService.SaveAsync();

            return await _botClient.SendTextMessageAsync(chatId: callbackQuery.From.Id,
            text: $"Вы можете в любое время задать электронную почту с помощью команды /setEmail.",
            parseMode: ParseMode.Markdown);
        }

        if ((ChatState)user.ChatState != ChatState.SetEmail)
        {
            await _userService.UpdateChatStateAsync(user, ChatState.SetEmail);
            await _userService.SaveAsync();
        }

        var sentMessage = await _botClient.SendTextMessageAsync(chatId: callbackQuery.From.Id,
            text: $"Отправьте в чат Вашу почту.\nПример сообщения:\nivanivanov@mail.ru.",
            parseMode: ParseMode.Markdown);

        return sentMessage;
    }

    private async Task<Message> OnAdmin(Message message)
    {
        User existUser = null;

        if (message.ForwardFrom == null)
        {
            if (message.Text.Contains("ss://"))
            {
                existUser = await _userService.FindByVpnKeyAsync(message.Text);
            }
        }
        else
        {
            existUser = await _userRepository.FindByTelegramIdIncludeDeepConfigsAsync(message.ForwardFrom.Id, false, u => u.UserSettings);
        }

        var responseMessage = "Пользователя нет в БД";

        if (existUser == null)
        {
            return await _botClient.SendTextMessageAsync(chatId: message.Chat.Id, text: responseMessage, parseMode: ParseMode.Markdown);
        }

        responseMessage = string.Empty;

        var validConfig = existUser.Configs.LastOrDefault(c => c.IsValid);
        var isPaid = validConfig?.Payments?.Any(x => x.Status == PaymentStatus.Succeeded && x.UserConfigId == validConfig.Id) ?? false;
        var hasRestriction = validConfig?.RestrictionsHistories.LastOrDefault();
        var keysAmount = existUser.Configs.Count;
        var tags = existUser.Tags;


        responseMessage += $@"
Зашел: {existUser.StartDate}
Вышел: {existUser.EndDate}
Количество ключей: {keysAmount}
Персональный лимт МБ: {existUser.UserSettings.PersonalTodayLimit.ToMegabytes()}

Тип бана: {existUser.UserSettings.SubscribeTheoryMethod}
";
        if (validConfig == null)
        {
            return await _botClient.SendTextMessageAsync(chatId: message.Chat.Id, text: responseMessage, parseMode: ParseMode.Markdown);
        }

        if (tags is not null && tags.Any())
        {
            responseMessage += Environment.NewLine + $@"
Теги: {string.Join(Environment.NewLine, tags)}";
        }

        if (isPaid)
        {
            responseMessage += @$"
Пользователь платный $$$$$$$
";
        }

        responseMessage += Environment.NewLine;

        responseMessage += $@"
Информация о ключе:
Начало: {validConfig.StartDate}
Окончание: {validConfig.EndDate}
Ключ - `{validConfig.VpnKey}`
Сервер - {validConfig?.Server?.Country}

Трафик сегодня: {validConfig.TodayGigabytes}
Трафик всего: {validConfig.TotalGigabytes}
";
        responseMessage += Environment.NewLine;

        if (hasRestriction != null)
        {
            responseMessage += $@"
Информация о блокировке ключа:
Начало: {hasRestriction?.StartTime}
Окончание: {hasRestriction?.UnbanTime}
Разбанено по факту: {hasRestriction?.EndTime}
";
        }

        List<List<InlineKeyboardButton>> buttons = new List<List<InlineKeyboardButton>>();

        if (validConfig.RestrictionsHistories.Any(x => x.EndTime is null))
        {
            var unbanButton = new InlineKeyboardButton("Снять бан")
            {
                CallbackData = $"/unBan -userId:{existUser.TelegramId}"
            };

            buttons.Add(new List<InlineKeyboardButton>() { unbanButton });
        }

        var keyDays = (validConfig.EndDate - validConfig.StartDate).Days;
        var servers = await _certificateManger.GetAllAvailableServers(keyDays, isPaid);

        foreach (var server in servers)
        {
            var changeServerButton = new InlineKeyboardButton("Изменить сервер на " + server.Country)
            { CallbackData = $"/changeServer -userId:{existUser.TelegramId} -serverId:{server.Id}" };
            buttons.Add(new List<InlineKeyboardButton>() { changeServerButton });
        }

        var markUp = new InlineKeyboardMarkup(buttons);

        return await _botClient.SendTextMessageAsync(chatId: message.Chat.Id, text: responseMessage, parseMode: ParseMode.Markdown, replyMarkup: markUp);
    }

    private Task<Message> OnBuyVpn(CallbackQuery callbackQuery)
    {
        return OnBuyVpn(_currentUser);
    }

    private async Task<Message> OnBuyVpn(User user)
    {
        InlineKeyboardMarkup markUp;
        
        var month1 = new InlineKeyboardButton($"Доступ на 30 дней {Constants.Price.Price30Days}р")
        {
            CallbackData = $"/getVpn -duration:30"
        };

        var month3 = new InlineKeyboardButton($"Доступ на 90 дней {Constants.Price.Price90Days}р")
        {
            CallbackData = $"/getVpn -duration:90"
        };

        var firstRow = new[] { month1 };
        var secondRow = new[] { month3 };

        var buttons = new[] { firstRow, secondRow };

        // Keyboard markup
        markUp = new InlineKeyboardMarkup(buttons);

        var sentMessage = await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: MessagesText.SellMessage, parseMode: ParseMode.Markdown, replyMarkup: markUp);

        return sentMessage;
    }

    private async Task<Message> ProcessPayment(CallbackQuery callbackQuery)
    {
        var chunks = callbackQuery.Data.Split(" ");

        var duration = 0;

        foreach (var chunk in chunks)
        {
            if (chunk.StartsWith("-duration"))
            {
                duration = int.Parse(chunk.Split(":")[1]);
            }
        }

        var price = duration == 30 ? Constants.Price.Price30Days : Constants.Price.Price90Days;
        
        var linkInfo = await _yookassaClientManager.GenerateLinkForPayment(_currentUser.TelegramId, price, callbackQuery.Data, _currentUser.Email);

        var dateTimeNow = DateTimeOffset.Now;

        int premiumCertificateId;

        if (_currentUser.Configs.Any(x => x.IsValid && x.Payments.Any(x => x.Status == PaymentStatus.Succeeded)))
        {
            premiumCertificateId = _currentUser.Configs.First(x => x.IsValid && x.Payments.Any(x => x.Status == PaymentStatus.Succeeded)).Id;
        }
        else
        {
            premiumCertificateId = await _certificateManger.CreateCertificateBeforePayment(callbackQuery.From.Id, duration, true);
        }

        await _paymentRepository.AddAsync(new Payment()
        {
            UserConfigId = premiumCertificateId,
            YookassaPaymentId = linkInfo.Id,
            Amount = price,
            DurationInDays = duration,
            Status = PaymentStatus.Pending,
            CreatedAt = dateTimeNow,
            UpdatedAt = dateTimeNow,
            PaymentTo = linkInfo.PaymentTo
        });

        await _paymentRepository.SaveAsync();

        var makePayment = new InlineKeyboardButton("Оплатить в Юкасса")
        {
            Url = linkInfo.ConfirmationLink
        };

        var markUp = new InlineKeyboardMarkup(makePayment);

        var helpMessage = $"{duration} дней";

        var sentMessage = await _botClient.SendTextMessageAsync(chatId: callbackQuery.From.Id, text: string.Format(MessagesText.PaymentMessage, helpMessage, price), parseMode: ParseMode.Markdown, replyMarkup: markUp);

        return sentMessage;
    }

    private async Task<Message> Unban(CallbackQuery callbackQuery)
    {
        var chunks = callbackQuery.Data.Split(" ");

        long userId = 0;

        foreach (var chunk in chunks)
        {
            if (chunk.StartsWith("-userId"))
            {
                userId = long.Parse(chunk.Split(":")[1]);
            }
        }

        await _restrictionService.MarkUnbanLastUserRestrictionAsync(userId);
        return await _botClient.SendTextMessageAsync(chatId: callbackQuery.From.Id, text: string.Format("Будет разбанен сервисом разбана в ближайшее время"), parseMode: ParseMode.Markdown);
    }

    private async Task<Message> ChangeServer(CallbackQuery callbackQuery)
    {
        var chunks = callbackQuery.Data.Split(" ");

        long userId = 0;
        var serverId = 0;

        foreach (var chunk in chunks)
        {
            if (chunk.StartsWith("-userId"))
            {
                userId = long.Parse(chunk.Split(":")[1]);
            }

            if (chunk.StartsWith("-serverId"))
            {
                serverId = int.Parse(chunk.Split(":")[1]);
            }
        }

        try
        {
            var vpnKey = await _certificateManger.UpdateUserConfigAsync(userId, serverId);

            await _botClient.SendTextMessageAsync(chatId: callbackQuery.From.Id,
                text: $"Новый ключ - `{vpnKey}`", parseMode: ParseMode.Markdown);

            await _botClient.SendTextMessageAsync(chatId: userId,
                text: MessagesText.InstructionAfterKeyUpdate, parseMode: ParseMode.Markdown);

            var messageWithNewKey = await _botClient.SendTextMessageAsync(chatId: userId,
                text: $"`{vpnKey}`", parseMode: ParseMode.Markdown);

            await _botClient.PinChatMessageAsync(userId, messageWithNewKey.MessageId);

            return messageWithNewKey;
        }
        catch (Exception e)
        {
            var user = await _userRepository.FindByTelegramIdAsync(userId);
            var lastValidConfig = user.Configs.LastOrDefault(c => c.IsValid);

            _logger.LogCritical(e.Message + Environment.NewLine +  e.StackTrace);
            return await _botClient.SendTextMessageAsync(chatId: callbackQuery.From.Id,
                text: $"Кажись не вышло. Сервер {lastValidConfig.Server.Country} ({lastValidConfig.Server.Ip}) лежит {e.Message}",
                parseMode: ParseMode.Markdown);
        }
    }

    private Task<Message> OnGetVpn(Message message)
    {
        return GetVpn(message.From.Id);
    }

    private async Task<Message> OnUpdateVpn(Message message)
    {
        return await UpdateVpn(message.From.Id);
    }

    private async Task<Message> OnUpdateVpn(CallbackQuery callbackQuery)
    {
        return await UpdateVpn(callbackQuery.From.Id);
    }


    private async Task<Message> OnRulesVpn(long userId)
    {
        return await GetRules(userId);
    }

    private async Task<Message> OnReferral(Message message)
    {
        await _botClient.SendTextMessageAsync(chatId: message.Chat.Id,
            "`" + await _userReferalService.GetInviteCodeAsync(_currentUser) + "`", ParseMode.Markdown);
        return await _botClient.SendTextMessageAsync(chatId: message.Chat.Id, text: string.Format(MessagesText.ReferralMessage, await _userReferalService.GetInvitedAmountByUserAsync(_currentUser.Id)), parseMode: ParseMode.Markdown);
    }

    private async Task<Message> GetRules(long userId)
    {
        return await _botClient.SendTextMessageAsync(chatId: userId, text: MessagesText.RulesMessage,
                                             parseMode: ParseMode.Markdown);
    }

    private async Task<Message> UpdateVpn(long userId)
    {
        if (!_currentUser.Configs.Any(x => x.IsValid && x.Payments.Any(x => x.Status == PaymentStatus.Succeeded)))
        {
            return await OnHelp(null);
        }

        var isCertificateGranded = await _certificateManger.DoesUserHaveValidCertificateAsync(userId);

        string certificate = string.Empty;
        Message sentMessage = null;

        if (isCertificateGranded)
        {
            if (!_currentUser.LastUserConfigChangeDate.HasValue || DateTimeOffset.Now - _currentUser.LastUserConfigChangeDate > TimeSpan.FromDays(1))
            {
                certificate = await _certificateManger.UpdateUserConfigAsync(userId, findBestServer: true);

                var messageToPin = await _botClient.SendTextMessageAsync(chatId: userId, text: "`" + certificate + "`",
                    parseMode: ParseMode.Markdown);

                var connectButton = new InlineKeyboardButton("Подключиться 🚀")
                {
                    Url = CertificateManger.GetEncodedInviteUrl(certificate)
                };

                var firstRow = new[] { connectButton };

                var markUp = new InlineKeyboardMarkup(firstRow);

                sentMessage = await _botClient.SendTextMessageAsync(chatId: userId,
                    text: MessagesText.CertifiacateUpdatedMessage,
                    parseMode: ParseMode.Markdown, replyMarkup: markUp);

                await _botClient.PinChatMessageAsync(chatId: userId, messageToPin.MessageId);

                _currentUser.LastUserConfigChangeDate = DateTimeOffset.Now;
            }
            else
            {
                sentMessage = await _botClient.SendTextMessageAsync(chatId: userId,
                    text: $"Замена ключа будет доступна через {(_currentUser.LastUserConfigChangeDate.Value.AddDays(1) - DateTimeOffset.Now).ToString(@"hh\:mm")}",
                    parseMode: ParseMode.Markdown);
            }
        }
        else
        {
            var vpnCertificate = await _certificateManger.GetNewCertificate(userId, Constants.LimitConstants.DEFAULT_FREE_VPN_DAYS);

            var messageToPin = await _botClient.SendTextMessageAsync(chatId: userId, text: "`" + vpnCertificate + "`",
                parseMode: ParseMode.Markdown);

            var connectButton = new InlineKeyboardButton("Подключиться 🚀")
            {
                Url = CertificateManger.GetEncodedInviteUrl(vpnCertificate)
            };

            var firstRow = new[] { connectButton };

            var markUp = new InlineKeyboardMarkup(firstRow);

            sentMessage = await _botClient.SendTextMessageAsync(chatId: userId,
                text: MessagesText.CertifiacateReadyMessage, parseMode: ParseMode.Markdown, replyMarkup: markUp);

            await _botClient.PinChatMessageAsync(chatId: userId, messageToPin.MessageId);

            return sentMessage;
        }

        return sentMessage;
    }

    private Task<Message> OnGetVpn(CallbackQuery callbackQuery)
    {
        return GetVpn(callbackQuery.From.Id);
    }

    private Task UnknownUpdateHandlerAsync(Update update)
    {
        //_logger.LogInformation("Unknown update type: {UpdateType}", update.Type);
        return Task.CompletedTask;
    }

    public async Task HandleErrorAsync(Exception exception)
    {
        var ErrorMessage = exception switch
        {
            ApiRequestException apiRequestException => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
            _ => $"{exception}\n {exception.StackTrace}"
        };

        // todo: отправить ошибки админам
        //await _botClient.SendTextMessageAsync("669363145", ErrorMessage);

        _logger.LogError(exception, ErrorMessage);
    }

    public async Task<Message> ReshuffleUsersMessage(Message message)
    {
        var reshuffleCountSetting = await _sysSettingService.GetSettingAsync(SysSettingName.ReshuffleKeysCount);

        if (!reshuffleCountSetting.Enabled)
        {
            return await _botClient.SendTextMessageAsync(chatId: message.Chat.Id, $"Настройка {SysSettingName.ReshuffleKeysCount} отключена.", parseMode: ParseMode.Markdown);
        }

        var publicServers = await _certificateManger.GetAllAvailableServers(0, false);
        var proServers = await _certificateManger.GetAllAvailableServers(0, true);

        var buttons = new List<List<InlineKeyboardButton>>();

        foreach (var server in publicServers)
        {
            var changeServerButton = new InlineKeyboardButton("Переместить пользователей с сервера " + server.Country + $" ({server.Id})")
            { CallbackData = $"/reshuffleUsers -serverId:{server.Id}" };
            buttons.Add(new List<InlineKeyboardButton>() { changeServerButton });
        }

        foreach (var server in proServers)
        {
            var changeServerButton = new InlineKeyboardButton("Переместить пользователей с сервера " + server.Country + $" ({server.Id}) $$$")
            { CallbackData = $"/reshuffleUsers -serverId:{server.Id}" };
            buttons.Add(new List<InlineKeyboardButton>() { changeServerButton });
        }

        var markUp = new InlineKeyboardMarkup(buttons);

        return await _botClient.SendTextMessageAsync(chatId: message.Chat.Id, "Выберите сервер ", parseMode: ParseMode.Markdown, replyMarkup: markUp);
    }

    public async Task<Message> ReshuffleUsersFromServer(CallbackQuery callbackQuery)
    {
        var chunks = callbackQuery.Data.Split(" ");
        var serverId = 0;
        int rehuffleKeysCount = 0;

        var reshuffleCountSetting = await _sysSettingService.GetSettingAsync(SysSettingName.ReshuffleKeysCount);

        if (!int.TryParse(reshuffleCountSetting.Value, out rehuffleKeysCount))
        {
            return await _botClient.SendTextMessageAsync(chatId: callbackQuery.From.Id, $"Настройка {SysSettingName.ReshuffleKeysCount} должна содежрать число!.", parseMode: ParseMode.Markdown);
        }


        await _botClient.SendTextMessageAsync(chatId: callbackQuery.From.Id, $"Будет заменено {rehuffleKeysCount} ключей!.", parseMode: ParseMode.Markdown);

        foreach (var chunk in chunks)
        {
            if (chunk.StartsWith("-serverId"))
            {
                serverId = int.Parse(chunk.Split(":")[1]);
            }
        }

        var userConfigs = await _certificateManger.FindCertificatesAsync(x => x.IsValid && x.ServerId == serverId);
        userConfigs = userConfigs.OrderByDescending(x => x.StartDate).ToList();

        var status = 0;
        var configsToChange = rehuffleKeysCount == 0 ? userConfigs.Count : rehuffleKeysCount;
        var changedConfigs = 0;

        var messageText = $"Замена ключей на сервере {serverId} начата" + Environment.NewLine + $"Замена {configsToChange} ключей завершена на {status}%";

        var sentMessage = await _botClient.SendTextMessageAsync(callbackQuery.From.Id, messageText);

        var chatId = new ChatId(callbackQuery.From.Id);

        var exceptions = new List<string>();

        for (int i = 0; i < rehuffleKeysCount; i++)
        {
            var userConfig = userConfigs[i];
            try
            {
                var newUserConfig =
                    await _certificateManger.UpdateUserConfigAsync(userConfig.User.TelegramId, findBestServer: true);
                await _botClient.SendTextMessageAsync(userConfig.User.TelegramId,
                    "По техническим причинам ваш ключ доступа был изменен. Новый ключ доступа: ");
                await _botClient.SendTextMessageAsync(userConfig.User.TelegramId, newUserConfig);

                await Task.Delay(125);
                changedConfigs++;

                if (Convert.ToInt32(Math.Round((changedConfigs * 1.0 / configsToChange * 1.0) * 100)) ==
                    status) continue;

                status++;
                messageText = $"Замена ключей на сервере {serverId} начата" + Environment.NewLine +
                              $"Замена {configsToChange} ключей завершена на {status}%";
                await _botClient.EditMessageTextAsync(chatId, sentMessage.MessageId, messageText);
            }
            catch (Exception ex)
            {
                exceptions.Add(ex.Message);
            }
        }

        messageText = $"Замена ключей на сервере {serverId} начата" + Environment.NewLine +
                      $"Замена {configsToChange} ключей завершена на {status}%" + Environment.NewLine + "Завершено ✅";

        if (exceptions.Any())
        {
            messageText += Environment.NewLine + $"Ошибок : {exceptions.Count}"
                                              + Environment.NewLine + exceptions.First();
        }

        return await _botClient.SendTextMessageAsync(callbackQuery.From.Id, messageText);
    }
    
    private async Task<Message> GetVpn(long userId)
    {
        var isCertificateGranded = await _certificateManger.DoesUserHaveValidCertificateAsync(userId);

        if (isCertificateGranded)
        {
            return await _botClient.SendTextMessageAsync(chatId: userId, text: MessagesText.CertifiacateAlreadyGrandedMessage, parseMode: ParseMode.Markdown);
        }

        // проверка есть ли на последнем конфиге оплата 
        //var doesUserHavePayments = _currentUser.Configs.Any() && _currentUser.Configs.MaxBy(x => x.EndDate).Payments.Any(p => p.Status == PaymentStatus.Succeeded);

        //if (doesUserHavePayments)
        //{
        //    return await _botClient.SendTextMessageAsync(chatId: userId, text: MessagesText.UserAlreadyPayedOnGetVpnMessage, parseMode: ParseMode.Markdown);
        //}

        var vpnCertificate = string.Empty;
        try
        {
            await UserLock.AsyncLock(userId.ToString()).WaitAsync();
            
            isCertificateGranded = await _certificateManger.DoesUserHaveValidCertificateAsync(userId);

            if (isCertificateGranded)
            {
                Console.WriteLine("вышел");
                return await _botClient.SendTextMessageAsync(chatId: userId,
                    text: MessagesText.CertifiacateAlreadyGrandedMessage, parseMode: ParseMode.Markdown);
            }
            
            vpnCertificate =
                await _certificateManger.GetNewCertificate(userId, Constants.LimitConstants.DEFAULT_FREE_VPN_DAYS);
        }
        catch (Exception e)
        {
            _logger.LogCritical(e, "Не удалось получить ключ!");
            return await _botClient.SendTextMessageAsync(chatId: userId,
                text: "Операция временно недоступна. Попробуйте позже.", parseMode: ParseMode.Markdown);
        }
        finally
        {
            UserLock.AsyncLock(userId.ToString()).Release();
        }

        //await _botClient.SendTextMessageAsync(NEGRIME_ID, $"Выдал ключ пользователю - {_currentUser?.TelegramId} {_currentUser?.FirstName} {_currentUser?.LastName}");

        var messageToPin = await _botClient.SendTextMessageAsync(chatId: userId, text: "`" + vpnCertificate + "`", parseMode: ParseMode.Markdown);

        var connectButton = new InlineKeyboardButton("Подключиться 🚀")
        {
            Url = CertificateManger.GetEncodedInviteUrl(vpnCertificate)
        };

        var firstRow = new[] { connectButton };

        var markUp = new InlineKeyboardMarkup(firstRow);

        await _botClient.PinChatMessageAsync(chatId: userId, messageToPin.MessageId);

        var sentMessage = await _botClient.SendTextMessageAsync(chatId: userId, text: MessagesText.CertifiacateReadyMessage, parseMode: ParseMode.Markdown, replyMarkup: markUp);

        return sentMessage;
    }

    private async Task<User> GetOrAddUser(Message message)
    {
        var userId = message.From.Id;
        var existUser =
            await _userRepository.FindByTelegramIdIncludeDeepConfigsAsync(userId, includes: u => u.UserSettings);

        if (existUser != null)
        {
            if (existUser.EndDate != null)
            {
                existUser.EndDate = null;
                await _userRepository.SaveAsync();
            }

            return existUser;
        }
        
        var user = new User()
        {
            TelegramId = userId,
            UserName = message.From.Username,
            FirstName = message.From.FirstName,
            LastName = message.From.LastName,
            StartDate = DateTime.Now,
            EndDate = null,
            ChatState = 0,
            UserSettings = new UserSettings()
            {
                VIP = false,
                SubscribeTheoryMethod = SubscribeTheoryMethod.NoSubscribeTheoryMethod
            }
        };

        await _userRepository.AddAsync(user);
        await _userRepository.SaveAsync();

        await _botClient.SendTextMessageAsync(NEGRIME_ID,
            $"Добавлен новый пользователь - {(string.IsNullOrEmpty(user?.UserName) ? string.Empty : "@" + user?.UserName)} {user?.TelegramId} {user?.FirstName} {user?.LastName}");

        return user;
    }

    private VPN.BOT.Common.Enums.Action ParseMessageToAction(string message)
    {
        try
        {
            if (message.StartsWith("/getVpn"))
            {
                var result = message.Contains("duration") ? Action.ProcessPayment : Action.GetVpn;
                return result;
            }

            if (message.StartsWith("/unBan"))
            {
                return Action.Unban;
            }

            if (message.StartsWith("/changeServer"))
            {
                return Action.ChangeServer;
            }

            if (message.StartsWith("/setEmail"))
            {
                return Action.SetEmail;
            }

            if (message.StartsWith("/reshuffleUsers"))
            {
                return Action.ReshuffleUsers;
            }

            return ActionAliasDictionary.Instance.TryGetValue(message, out var action) ? action : Action.Unknown;
        }
        catch (Exception e)
        {
            return VPN.BOT.Common.Enums.Action.Unknown;
        }
    }

    public async Task UpdateReplyKeyboardMarkup(User user)
    {
        _currentUser = user;
        await _botClient.SendTextMessageAsync(chatId: user.TelegramId, text: "/start", parseMode: ParseMode.Markdown, replyMarkup: await _messageService.GenerateReplyKeyboardMarkup(user));
    }
}