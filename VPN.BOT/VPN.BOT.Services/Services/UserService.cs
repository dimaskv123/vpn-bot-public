﻿using Domain.Entity;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ICertificateManger _certificateManger;

        public UserService(
            IUserRepository userRepository,
            IReferalService referalService,
            ICertificateManger certificateManger)
        {
            _userRepository = userRepository;
            _certificateManger = certificateManger;
        }

        public async Task<bool> DoesUserExist(long userId)
        {
            return await _userRepository.DoesUserExistAsync(userId);
        }

        public async Task<User> FindByTelegramIdAsync(long userId)
        {
            return await _userRepository.FindByTelegramIdAsync(userId);
        }

        public async Task<User> FindByVpnKeyAsync(string vpnKey)
        {
            return await _userRepository.FindByVpnKey(vpnKey);
        }

        public async Task<int> GetNewUsersAmountByDate(DateTimeOffset date)
        {
            return (await _userRepository.Find(u => u.StartDate.Date == date.Date)).Count();
        }

        public async Task RevokeUser(User user)
        {
            user.EndDate = DateTimeOffset.Now;

            if (!user.Configs.Any(x => x.IsValid && x.Payments.Any()))
            {
                await _certificateManger.RevokeUserCertificateAsync(user);
            }

            await _userRepository.UpdateAsync(user);
            await _userRepository.SaveAsync();
        }

        public async Task<List<User>> GetValidUsersAsync()
        {
           var result =  (await _userRepository.GetAllValidAsync()).ToList();

           return result;
        }

        public async Task UpdateEmailAsync(User user, string email)
        {
            user.Email = email;
            await _userRepository.UpdateAsync(user);
        }

        public async Task UpdateChatStateAsync(User user, ChatState chatState)
        {
            int parsedChatState = (int)chatState;
            user.ChatState = parsedChatState;

            await _userRepository.UpdateAsync(user);
        }
        
        public IEnumerable<User> WhereNoTriggerOrLastTriggerWasIn(IEnumerable<User> source, TriggerType triggerType, TimeSpan timeSpan)
        {
            if (!source.Any())
            {
                return Enumerable.Empty<User>();
            }

            return source.Where(x =>
            (x?.TriggerMessages?.FirstOrDefault(x => x?.TriggerType == triggerType) == null && DateTimeOffset.Now - x?.StartDate > timeSpan)
                || DateTimeOffset.Now - x?.TriggerMessages?.Where(x => x.TriggerType == triggerType).MaxBy(x => x?.CreatedAt)?.CreatedAt > timeSpan);
        }

        public async Task SaveAsync()
        {
            await _userRepository.SaveAsync();
        }
    }
}
