﻿using Telegram.Bot;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services;

public class AdminNotifyService : IAdminNotifyService
{
    private readonly ITelegramBotClient _botClient;

    private readonly long[] Admins = {669363145, 254724042};

    public AdminNotifyService(ITelegramBotClient botClient)
    {
        _botClient = botClient;
    }

    public async Task NotifyAllAdmins(string message)
    {
        foreach (var admin in Admins)
        {
            try
            {
                await _botClient.SendTextMessageAsync(chatId: admin, message);
            }
            catch (Exception e)
            {
            }
        }
    }
}