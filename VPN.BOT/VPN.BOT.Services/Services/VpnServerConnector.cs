﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Services.ExternalService.Model;
using VPN.BOT.Services.ExternalServices;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services;

public class VpnServerConnector : IVpnServerConnector
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<IVpnServerConnector> _logger;
    private readonly IServerRepository _serverRepository;
    private readonly ICertificateRepository _certificateRepository;
    private readonly IUserRepository _userRepository;

    private readonly string defaultServerName = "/?outline=1";
    private readonly string krotServerName = "#KROTvpn";
    public VpnServerConnector(
        HttpClient httpClient,
        ILogger<IVpnServerConnector> logger,
        IServerRepository serverRepository,
        ICertificateRepository certificateRepository,
        IUserRepository userRepository
        )
    {
        _httpClient = httpClient;
        _logger = logger;
        _serverRepository = serverRepository;
        _certificateRepository = certificateRepository;
        _userRepository = userRepository;
    }

    public async Task<OutlineUserModel> CreateCertificateAsync(User userModel, Server serverModel)
    {
        var outlineService = new OutlineService(serverModel.BaseUrl);

        var newOutlineUser = await outlineService.CreateNewOutlineUsersAsync();
        newOutlineUser.AccessUrl = GetRenamedKeyServerName(newOutlineUser.AccessUrl);

        var userName = $"{userModel.TelegramId}|{userModel.Id}|{userModel.FirstName} {userModel.LastName}";

        await outlineService.RenameOutlineUserAsync(newOutlineUser.Id, userName);

        return newOutlineUser;
    }
    
    public async Task<OutlineUserModel> CreateCertificateAsync(Server serverModel)
    {
        var outlineService = new OutlineService(serverModel.BaseUrl);

        var newOutlineUser = await outlineService.CreateNewOutlineUsersAsync();
        newOutlineUser.AccessUrl = GetRenamedKeyServerName(newOutlineUser.AccessUrl);

        return newOutlineUser;
    }


    private string GetRenamedKeyServerName(string accessKey)
    {
        var result = accessKey.Replace(defaultServerName, krotServerName);
        return result;
    }

    public async Task DeleteCertificateAsync(UserConfig userConfig)
    {
        var outlineService = new OutlineService(userConfig.Server.BaseUrl);
        await outlineService.DeleteOutlineUserAsync(userConfig.OutlineUserId);
    }

    public async Task LimitKey(UserConfig userConfig)
    {
        var outlineService = new OutlineService(userConfig.Server.BaseUrl);
        await outlineService.LimitKeyByOulineUserAsync(userConfig.OutlineUserId);
    }

    public async Task UnLimitKey(UserConfig userConfig)
    {
        var outlineService = new OutlineService(userConfig.Server.BaseUrl);
        await outlineService.UnlimitKeyByOulineUserAsync(userConfig.OutlineUserId);
    }
}