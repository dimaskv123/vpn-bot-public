﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services
{
    public class RedisService : IRedisService
    {
        private readonly IDatabase _cache;
        private readonly ILogger<RedisService> _logger;

        public RedisService(IDatabase cache, ILogger<RedisService> logger)
        {
            _cache = cache;
            _logger = logger;
        }

        public async Task<T> GetAsync<T>(string key) where T : class
        {
            var cachedValue = await _cache.StringGetAsync(key);

            if (string.IsNullOrEmpty(cachedValue))
            {
                return null;
            }

            _logger.LogInformation($"Returning value from cache: {key}");
            return JsonConvert.DeserializeObject<T>(cachedValue);
        }


        public async Task AddToCollectionAsync<T>(string collectionName, T element)
        {
            try
            {

                var serializedValue = JsonConvert.SerializeObject(element, Formatting.Indented,
                                    new JsonSerializerSettings
                                    {
                                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                                    });

                await _cache.ListRightPushAsync(collectionName, serializedValue);
            }
            catch (Exception e)
            {

            }
        }

        public async Task<IEnumerable<T>> GetCollectionAsync<T>(string collectionName)
        {
            var redisValues = await _cache.ListRangeAsync(collectionName);
            List<T> result = new List<T>();

            foreach (var item in redisValues)
            {
                result.Add(JsonConvert.DeserializeObject<T>(item));
            }

            return result;
        }

        public async Task DeleteKeyAsync(string keyName)
        {
            await _cache.KeyDeleteAsync(keyName);
        }
    }

}
