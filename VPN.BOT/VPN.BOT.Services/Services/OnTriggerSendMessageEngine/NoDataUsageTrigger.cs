﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services.OnTriggerSendMessageEngine;

public class NoDataUsageTrigger : TriggerEngine
{
    private IUserService _userService;
    
    public NoDataUsageTrigger(IUserRepository userRepository, IMessageService messageService, ILogger<TriggerEngine> logger, ITriggerMessageRepository triggerMessageRepository, IUserService userService) : base(userRepository, messageService, logger, triggerMessageRepository)
    {
        _userService = userService;
        TriggerType = TriggerType.NoDataUsage;
        WorkOnlyInBusynessHours = true;
    }

    protected override async Task Fire(User user)
    {
        await _messageService.SendMessageAsync(MessageName.NoDataUsage, user);
    }

    protected override async Task<IEnumerable<User>> SetUpUsers()
    {
        return _userService.WhereNoTriggerOrLastTriggerWasIn (AllUsers
                .Where(x => x.EndDate == null)
                .Where(x => DateTimeOffset.Now - x.Configs.FirstOrDefault(x => x.IsValid && x.TotalGigabytes == 0)?.StartDate > TimeSpan.FromDays(1)),
            TriggerType,
            TimeSpan.FromDays(3));
    }
}