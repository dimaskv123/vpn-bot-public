﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services.OnTriggerSendMessageEngine;

public class NoUserConfigTrigger : TriggerEngine
{
    private IUserService _userService;
    
    public NoUserConfigTrigger(IUserRepository userRepository, IMessageService messageService, ILogger<TriggerEngine> logger, ITriggerMessageRepository triggerMessageRepository, IUserService userService) : base(userRepository, messageService, logger, triggerMessageRepository)
    {
        _userService = userService;
        TriggerType = TriggerType.NoUserConfig;
        WorkOnlyInBusynessHours = true;
    }
    protected override async Task Fire(User user)
    {
        await _messageService.SendMessageAsync(MessageName.NoUserConfigTrigger, user);
    }

    protected override async Task<IEnumerable<User>> SetUpUsers()
    {
        return _userService.WhereNoTriggerOrLastTriggerWasIn (AllUsers
            .Where(x => x.EndDate == null)
            .Where(x => !x.Configs.Any()),
            TriggerType,
            TimeSpan.FromDays(1));
    }
}