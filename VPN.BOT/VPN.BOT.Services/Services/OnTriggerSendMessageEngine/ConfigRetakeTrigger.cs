﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services.OnTriggerSendMessageEngine;

public class ConfigRetakeTrigger : TriggerEngine
{
    private IUserService _userService;

    public ConfigRetakeTrigger(IUserRepository userRepository, IMessageService messageService, ILogger<TriggerEngine> logger, ITriggerMessageRepository triggerMessageRepository, IUserService userService) : base(userRepository, messageService, logger, triggerMessageRepository)
    {
        _userService = userService;
        TriggerType = TriggerType.ConfigRetake;
        WorkOnlyInBusynessHours = true;
    }

    protected override async Task Fire(User user)
    {
        await _messageService.SendMessageAsync(MessageName.ConfigRetake, user);
    }

    protected override async Task<IEnumerable<User>> SetUpUsers()
    {
        return _userService.WhereNoTriggerOrLastTriggerWasIn (AllUsers
                .Where(x => x.Configs.All(x => !x.IsValid))
                .Where(x => DateTimeOffset.Now - x.Configs.MaxBy(x => x.EndDate)?.EndDate > TimeSpan.FromDays(1)),
            TriggerType,
            TimeSpan.FromDays(1));
    }
}