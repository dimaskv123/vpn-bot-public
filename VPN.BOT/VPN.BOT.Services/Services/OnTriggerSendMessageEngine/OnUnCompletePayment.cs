﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services.OnTriggerSendMessageEngine;

public class OnUnCompletePayment  : TriggerEngine
{
    private IUserService _userService;
    
    public OnUnCompletePayment(IUserRepository userRepository, IMessageService messageService, ILogger<TriggerEngine> logger, ITriggerMessageRepository triggerMessageRepository, IUserService userService) : base(userRepository, messageService, logger, triggerMessageRepository)
    {
        _userService = userService;
        TriggerType = TriggerType.UnCompletePayment;
    }

    protected override async Task Fire(User user)
    {
        await _messageService.SendMessageAsync(MessageName.UnCompletePayment ,user);
    }

    protected override async Task<IEnumerable<User>> SetUpUsers()
    {
        return _userService.WhereNoTriggerOrLastTriggerWasIn (AllUsers
            .Where(x => 
                !x.Configs.Any(x => x.IsValid && x.Payments.Any(x => x.Status == PaymentStatus.Succeeded)) &&
                x.Configs.SelectMany(x => x.Payments).Any(x => x.Status is PaymentStatus.Pending)
                        && DateTimeOffset.Now - x.Configs.SelectMany(x => x.Payments)
                            .Where(x => x.Status is PaymentStatus.Pending)
                            .MaxBy(x => x.CreatedAt).CreatedAt > TimeSpan.FromMinutes(30)), TriggerType, TimeSpan.FromHours(4));
    }
}