using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services.OnTriggerSendMessageEngine;

public class PaymentExpireIn3DaysTrigger: TriggerEngine
{
    private IUserService _userService;

    public PaymentExpireIn3DaysTrigger(IUserRepository userRepository, IMessageService messageService, ILogger<TriggerEngine> logger, ITriggerMessageRepository triggerMessageRepository, IUserService userService) : base(userRepository, messageService, logger, triggerMessageRepository)
    {
        _userService = userService;
        TriggerType = TriggerType.PaymentExpireIn3Day;
        WorkOnlyInBusynessHours = true;
    }

    protected override async Task Fire(User user)
    {
        await _messageService.SendMessageAsync(MessageName.PaymentExpireIn3Days, user);
    }

    protected override async Task<IEnumerable<User>> SetUpUsers()
    {
        return _userService.WhereNoTriggerOrLastTriggerWasIn (AllUsers
                .Where(x =>
                {
                    var timeSpan = x?.Configs.FirstOrDefault(x =>
                        x.IsValid && x.Payments.Any(x => x.Status == PaymentStatus.Succeeded))?.EndDate - DateTimeOffset.Now;
                    
                    return timeSpan < TimeSpan.FromDays(3) && timeSpan > TimeSpan.FromDays(2);
                }),
            TriggerType,
            TimeSpan.FromDays(25));
    }
}