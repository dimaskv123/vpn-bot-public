using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services.OnTriggerSendMessageEngine;

public class PaymentExpireIn1DayTrigger: TriggerEngine
{
    private IUserService _userService;

    public PaymentExpireIn1DayTrigger(IUserRepository userRepository, IMessageService messageService, ILogger<TriggerEngine> logger, ITriggerMessageRepository triggerMessageRepository, IUserService userService) : base(userRepository, messageService, logger, triggerMessageRepository)
    {
        _userService = userService;
        TriggerType = TriggerType.PaymentExpireIn1Day;
        WorkOnlyInBusynessHours = true;
    }

    protected override async Task Fire(User user)
    {
        await _messageService.SendMessageAsync(MessageName.PaymentExpireIn1Day, user);
    }

    protected override async Task<IEnumerable<User>> SetUpUsers()
    {
        return _userService.WhereNoTriggerOrLastTriggerWasIn (AllUsers
                .Where(x =>
                {
                    var timeSpan = x?.Configs.FirstOrDefault(x =>
                        x.IsValid && x.Payments.Any(x => x.Status == PaymentStatus.Succeeded))?.EndDate - DateTimeOffset.Now;
                    
                    return timeSpan < TimeSpan.FromDays(1) && timeSpan > TimeSpan.FromDays(0);
                }),
            TriggerType,
            TimeSpan.FromDays(25));
    }
}