﻿using Domain.Entity;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.Services.Interfaces;

namespace VPN.BOT.Services.Services.OnTriggerSendMessageEngine;

public abstract class TriggerEngine
{
    protected IEnumerable<User> TriggeredUsers = Enumerable.Empty<User>();
    protected IEnumerable<User> AllUsers;
    protected TriggerType TriggerType;
    protected bool WorkOnlyInBusynessHours = false;

    protected IUserRepository _userRepository;
    protected ITriggerMessageRepository _triggerMessageRepository;
    protected IMessageService _messageService;
    protected ILogger<TriggerEngine> _logger;

    protected TriggerEngine(IUserRepository userRepository, IMessageService messageService, ILogger<TriggerEngine> logger, ITriggerMessageRepository triggerMessageRepository)
    {
        _userRepository = userRepository;
        _messageService = messageService;
        _logger = logger;
        _triggerMessageRepository = triggerMessageRepository;
    }

    public async Task CheckTriggerCondition()
    {
        if (!BusynessHoursCheck())
        {
            return;
        }

        AllUsers = await _userRepository.GetIncludeDeepConfigsAsync(null, false, user => user.TriggerMessages);

        TriggeredUsers = await SetUpUsers();

        try
        {
            foreach (var user in TriggeredUsers)
            {
                await Fire(user);
                await _triggerMessageRepository.AddAsync(new TriggerMessage()
                {
                    TriggerType = TriggerType,
                    CreatedAt = DateTimeOffset.Now,
                    UserId = user.Id
                });
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, $"Error while fire trigger {this.GetType().Name}");
        }

        await _triggerMessageRepository.SaveAsync();
    }

    protected abstract Task Fire(User user);

    protected abstract Task<IEnumerable<User>> SetUpUsers();

    private bool BusynessHoursCheck()
    {
        return WorkOnlyInBusynessHours ? DateTime.Now.Hour > 8 && DateTime.Now.Hour < 22 : true;
    }
}