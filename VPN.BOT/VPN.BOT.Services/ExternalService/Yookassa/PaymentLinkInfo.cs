﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VPN.BOT.Common.Enums;

namespace VPN.BOT.Services.ExternalService.Yookassa
{
    public  class PaymentLinkInfo
    {
        public PaymentLinkInfo(string id, string confirmationLink)
        {
            Id = id;
            ConfirmationLink = confirmationLink;
        }

        public string Id { get; init; }
        public string ConfirmationLink { get; init; }
        public PaymentTo PaymentTo { get; set; }
    }
}
