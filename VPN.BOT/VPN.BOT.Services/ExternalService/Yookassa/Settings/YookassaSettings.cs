﻿namespace VPN.BOT.Services.ExternalService.Yookassa.Settings;

public class YookassaSettings
{
    public string ShopId { get; set; }
    public string SecretKey { get; set; }
    public string BillEmail { get; set; }
}