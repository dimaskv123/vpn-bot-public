﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using VPN.BOT.Services.ExternalService.Yookassa.Interfaces;
using VPN.BOT.Services.ExternalService.Yookassa.Settings;
using Yandex.Checkout.V3;

namespace VPN.BOT.Services.ExternalService.Yookassa;

public class YookassaClient
{
    private readonly AsyncClient _client;

    private string _botUrl { get; set; }
    private string _billEmail;

    public YookassaClient(YookassaSettings settings, string botUrl)
    {
        // todo: выексти в параметры

        var client = new Yandex.Checkout.V3.Client(
            shopId: settings.ShopId,
            secretKey: settings.SecretKey);

        _billEmail = settings.BillEmail;
        _client = client.MakeAsync();

        _botUrl = botUrl;
    }

    public async Task<PaymentLinkInfo> GenerateLinkForPayment(long telegramId, int amount, string description, string email)
    {
        var chunks = description.Split(" ");

        var price = 0;
        var duration = 0;

        foreach (var chunk in chunks)
        {
            if (chunk.StartsWith("-price"))
            {
                price = int.Parse(chunk.Split(":")[1]);
            }
            if (chunk.StartsWith("-duration"))
            {
                duration = int.Parse(chunk.Split(":")[1]);
            }
        }

        var newPayment = new NewPayment
        {
            Amount = new Amount { Value = amount, Currency = "RUB" },
            Confirmation = new Confirmation { Type = ConfirmationType.Redirect, ReturnUrl = _botUrl },
            MerchantCustomerId = telegramId.ToString(),
            Description = $"Доступ к ВПН на {duration} дней",
            Capture = true,
            Receipt = new Receipt()
            {
                Customer = new Customer()
                {
                    Email = string.IsNullOrEmpty(email) ? _billEmail : email,
                },
                Items = new List<ReceiptItem>()
                {
                    new ReceiptItem()
                    {
                        Description  = "Доступ к VPN",
                        Amount = new Amount { Value = amount, Currency = "RUB" },
                        VatCode = VatCode.NoVat,
                        Quantity = 1
                    }
                }
            }
        };

        var payment = await _client.CreatePaymentAsync(newPayment);

        return new PaymentLinkInfo(payment.Id, payment.Confirmation.ConfirmationUrl);
    }

    public async Task<Payment> CapturePayment(string paymentId)
    {
        return await _client.CapturePaymentAsync(paymentId);
    }

    public async Task<Payment> GetPayment(string paymentId)
    {
        return await _client.GetPaymentAsync(paymentId);
    }
}