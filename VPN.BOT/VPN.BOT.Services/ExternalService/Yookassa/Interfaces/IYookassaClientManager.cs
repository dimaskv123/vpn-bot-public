﻿using VPN.BOT.Common.Enums;
using Yandex.Checkout.V3;

namespace VPN.BOT.Services.ExternalService.Yookassa.Interfaces;

public interface IYookassaClientManager
{
    Task<PaymentLinkInfo> GenerateLinkForPayment(long telegramId, int amount, string description, string email);
    Task<Payment> CapturePayment(string paymentId, PaymentTo paymentTo);
    Task<Payment> GetPayment(string paymentId, PaymentTo paymentTo);
}