﻿using Microsoft.Extensions.Configuration;
using Repository.Interfaces;
using VPN.BOT.Common.Enums;
using VPN.BOT.Services.ExternalService.Yookassa.Interfaces;
using VPN.BOT.Services.ExternalService.Yookassa.Settings;
using Yandex.Checkout.V3;
using PaymentStatus = VPN.BOT.Common.Enums.PaymentStatus;


namespace VPN.BOT.Services.ExternalService.Yookassa
{
    public class YookassaClientManager : IYookassaClientManager
    {
        private readonly YookassaClient _dimaClient;
        private readonly YookassaClient _vladClient;
        private readonly IPaymentRepository _paymentRepository;

        public YookassaClientManager(IConfiguration configuration, IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;


            YookassaSettings dimaSettings = configuration.GetSection(PaymentTo.Dima + nameof(YookassaSettings)).Get<YookassaSettings>();
            YookassaSettings vladSettings = configuration.GetSection(PaymentTo.Vlad + nameof(YookassaSettings)).Get<YookassaSettings>();

            string botUrl = configuration.GetSection("BotUrl").Value;

            _dimaClient = new YookassaClient(dimaSettings, botUrl);
            _vladClient = new YookassaClient(vladSettings, botUrl);

        }

        public async Task<PaymentLinkInfo> GenerateLinkForPayment(long telegramId, int amount, string description, string email)
        {
            var dateTimeNow = DateTimeOffset.Now;
            var paymentsByReceivers = await _paymentRepository.GetPaymentsByReceiver(dateTimeNow.Month, dateTimeNow.Year);

            PaymentTo newPaymentTo;
            
            if (paymentsByReceivers.Count == 1)
            {
                newPaymentTo = paymentsByReceivers.FirstOrDefault()?.Receiver == PaymentTo.Dima
                    ? PaymentTo.Vlad
                    : PaymentTo.Dima;
            }
            else
            {          
                newPaymentTo = paymentsByReceivers.MinBy(x => x.TotalAmount)?.Receiver ?? PaymentTo.Dima;
            }
            
            var client = newPaymentTo switch
            {
                PaymentTo.Dima => _dimaClient,
                PaymentTo.Vlad => _vladClient,
                _ => _dimaClient
            };

            var linkInfo = await client.GenerateLinkForPayment(telegramId, amount, description, email);
            linkInfo.PaymentTo = newPaymentTo;
            return linkInfo;
        }

        public Task<Payment> CapturePayment(string paymentId, PaymentTo paymentTo)
        {
            var client = paymentTo switch
            {
                PaymentTo.Dima => _dimaClient,
                PaymentTo.Vlad => _vladClient,
                _ => _dimaClient
            };

            return client.CapturePayment(paymentId);
        }

        public Task<Payment> GetPayment(string paymentId, PaymentTo paymentTo)
        {
            var client = paymentTo switch
            {
                PaymentTo.Dima => _dimaClient,
                PaymentTo.Vlad => _vladClient,
                _ => _dimaClient
            };

            return client.GetPayment(paymentId);
        }
    }
}
