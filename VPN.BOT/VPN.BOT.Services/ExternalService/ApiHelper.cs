﻿using System.Net.Http.Headers;
using Polly;

namespace VPN.BOT.Services.ExternalServices
{
    public class ApiHelper : IDisposable
    {
        public HttpClient ApiClient { get; private set; }

        public Policy<HttpResponseMessage> RetryPolicy { get; private set; }

        
        public ApiHelper(int timeOutSeconds = 15)
        {
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
            {
                return true;
            };
            ApiClient = new HttpClient(httpClientHandler);
            ApiClient.Timeout = TimeSpan.FromSeconds(timeOutSeconds);
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            RetryPolicy = Policy
                .Handle<Exception>()
                .OrResult<HttpResponseMessage>(r => !r.IsSuccessStatusCode)
                .WaitAndRetryAsync(
                    3, 
                    retryAttempt => TimeSpan.FromSeconds(10 * retryAttempt) // Задержка в 10 секунд перед каждой новой попыткой.
                );
        }
        
        public void Dispose()
        {
            ApiClient.Dispose();
        }
    }
}
