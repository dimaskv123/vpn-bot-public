﻿using System.Text.RegularExpressions;

namespace VPN.BOT.Services.ExternalServices;

public class InstagramCheckService
{
    private HttpClient _client;


    public InstagramCheckService(HttpClient client)
    {
        _client = client;
        _client.DefaultRequestHeaders.Add("User-Agent", "PostmanRuntime/7.28.4");
        _client.DefaultRequestHeaders.Add("Accept", "*/*");
        _client.DefaultRequestHeaders.Add("Connection", "keep-alive");
    }

    public async Task<int> GetInstagramFollowersByUserName(string userName)
    {
        var response = await _client.GetAsync($"https://www.instagram.com/{userName}");

        response.EnsureSuccessStatusCode();

        var content = await response.Content.ReadAsStringAsync();

        var regex = new Regex("\"([^\"]*) Followers");

        var matches = regex.Matches(content);

        if (!matches.Any()) return 0;
        
        var parsed = int.TryParse(matches[0].Groups[1].Value.Replace(",", string.Empty), out var result);

        return parsed ? result : 0;

    }
}