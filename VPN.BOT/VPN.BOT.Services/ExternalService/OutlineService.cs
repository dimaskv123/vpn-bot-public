﻿using System.Net.Http.Json;
using Telegram.Bot.Exceptions;
using VPN.BOT.Services.ExternalService.Model;

namespace VPN.BOT.Services.ExternalServices
{
    public class OutlineService
    {
        private readonly string _baseUrl;

        public OutlineService(string url)
        {
            _baseUrl = url;
        }

        public async Task<OutlineUserModel> GetOutlineUserAsync(string OutlineUserId)
        {
            var users = await GetOutlineUsersAsync();

            var result = users?.FirstOrDefault(u => u.Id.Equals(OutlineUserId));

            return result;
        }

        public async Task<IEnumerable<OutlineUserModel>> GetOutlineUsersAsync()
        {
            var getUserUrl = "/access-keys";

            var api = new ApiHelper().ApiClient;

            using (HttpResponseMessage response = await api.GetAsync(_baseUrl + getUserUrl))
            {
                if (!response.IsSuccessStatusCode)
                {
                    throw new ApiRequestException(string.Format(nameof(GetOutlineUsersAsync), _baseUrl));
                }

                var result = await response.Content.ReadAsAsync<IEnumerable<OutlineUserModel>>();

                return result;
            }
        }

        public async Task<OutlineUserModel> CreateNewOutlineUsersAsync()
        {
            var getUserUrl = "/access-keys";

            var api = new ApiHelper().ApiClient;


            using (HttpResponseMessage response = await api.PostAsync(_baseUrl + getUserUrl, null))
            {
                if (!response.IsSuccessStatusCode)
                {
                    throw new ApiRequestException(string.Format(nameof(CreateNewOutlineUsersAsync), _baseUrl));
                }

                var result = await response.Content.ReadAsAsync<OutlineUserModel>();

                return result;

            }
        }

        public async Task<OutlineUserModel> RenameOutlineUserAsync(string outlineUserId, string newName)
        {
            var getUserUrl = $"/access-keys/{outlineUserId}/name";

            var api = new ApiHelper().ApiClient;

            var a = new { name = newName };

            using (HttpResponseMessage response = await api.PutAsJsonAsync(_baseUrl + getUserUrl, a))
            {
                if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    throw new ApiRequestException(string.Format(nameof(RenameOutlineUserAsync), _baseUrl));
                }

                var result = await response.Content.ReadAsAsync<OutlineUserModel>();

                return result;

            }
        }

        public async Task<bool> DeleteOutlineUserAsync(string outlineUserId)
        {
            var getUserUrl = $"/access-keys/{outlineUserId}";

            var api = new ApiHelper().ApiClient;

            using (HttpResponseMessage response = await api.DeleteAsync(_baseUrl + getUserUrl))
            {
                return response.StatusCode == System.Net.HttpStatusCode.NoContent;
            }
        }

        public async Task<Dictionary<string, long>> GetOutlineUsersBytesAsync()
        {
            var getUserUrl = $"/metrics/transfer";

            var api = new ApiHelper(120);

            using (HttpResponseMessage response = await api.RetryPolicy.ExecuteAsync(async () => await api.ApiClient.GetAsync(_baseUrl + getUserUrl)))
            {
                if (!response.IsSuccessStatusCode)
                {
                    throw new ApiRequestException(string.Format(nameof(GetOutlineUsersBytesAsync), _baseUrl));
                }

                var result = await response.Content.ReadAsAsync<UserData>();

                return result.BytesTransferredByUserId;
            }
        }

        public async Task LimitKeyByOulineUserAsync(string outlineUserId)
        {
            var putLimitUrl = $"/access-keys/{outlineUserId}/data-limit";

            var api = new ApiHelper().ApiClient;

            var limitParam = new { limit = new { bytes = 0 } };

            using (HttpResponseMessage response = await api.PutAsJsonAsync(_baseUrl + putLimitUrl, limitParam))
            {
                if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    throw new ApiRequestException(string.Format(nameof(LimitKeyByOulineUserAsync), _baseUrl));
                }
            }
        }

        public async Task UnlimitKeyByOulineUserAsync(string outlineUserId)
        {
            var deleteLimitUrl = $"/access-keys/{outlineUserId}/data-limit";

            var api = new ApiHelper(30).ApiClient;

            using (HttpResponseMessage response = await api.DeleteAsync(_baseUrl + deleteLimitUrl))
            {
                if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    throw new ApiRequestException(string.Format(nameof(UnlimitKeyByOulineUserAsync), _baseUrl));
                }
            }
        }
    }
}
