﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VPN.BOT.Services.ExternalService.Model
{
    public class OutlineUserModel
    {
        [JsonProperty("id")]
        public string? Id { get; set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("password")]
        public string? Password { get; set; }

        [JsonProperty("port")]
        public int Port { get; set; }

        [JsonProperty("method")]
        public string? Method { get; set; }

        [JsonProperty("accessUrl")]
        public string? AccessUrl { get; set; }
    }
}
