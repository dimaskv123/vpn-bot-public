﻿using Newtonsoft.Json;

namespace VPN.BOT.Services.ExternalService.Model
{
    public class UserData
    {
        public Dictionary<string, long> BytesTransferredByUserId { get; set; }
    }
}
