﻿using VPN.BOT.Common.Enums;

namespace Domain.Entity;

public class RestrictionsHistory
{
    public int Id { get; set; }
    public int UserConfigId { get; set; }
    public SubscribeTheoryMethod RestrictionType { get; set; }
    public DateTimeOffset? StartTime { get; set; }
    public DateTimeOffset? EndTime { get; set; }
    public DateTimeOffset? UnbanTime { get; set; }
    public UserConfig UserConfig { get; set; }
}