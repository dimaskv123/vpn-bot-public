namespace Domain.Entity;

public class Tag
{
    public int UserId { get; set; }
    public string Name { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public User User { get; set; }
}