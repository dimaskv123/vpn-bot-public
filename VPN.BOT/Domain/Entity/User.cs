﻿namespace Domain.Entity
{
    public record User
    {
        public int Id { get; set; }
        public long TelegramId { get; set; }
        public string? UserName { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public int ChatState { get; set; }
        public DateTimeOffset LastSentMessageDate { get; set; }
        public int? InvitedById { get; set; }
        public Referal InvitedBy { get; set; }
        public Referal Invite { get; set; }

        public DateTimeOffset? LastUserConfigChangeDate { get; set; }
        public string? Bio { get; set; }
        public DateTimeOffset? LastUpdateBioDate { get; set; }
        public UserSettings UserSettings { get; set; }
        public ICollection<UserConfig> Configs { get; set; }
        public ICollection<ScheduleMessage> ScheduleMessages { get; set; }
        public ICollection<TriggerMessage> TriggerMessages { get; set; }
        public ICollection<Tag> Tags { get; set; }
        public UserBlog UserBlogs { get; set; }
    }
}
