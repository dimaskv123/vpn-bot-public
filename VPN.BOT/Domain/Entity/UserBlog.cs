﻿namespace Domain.Entity;

public class UserBlog
{
    public int UserId { get; set; }
    public string? InstagramLogin { get; set; }
    public bool IsLoginConfidence { get; set; }
    public int InstagramSubscribers { get; set; }
    public string TgChannel { get; set; }
    public int TgChannelSubscribers { get; set; }
    public DateTimeOffset? LastAdDate { get; set; }
    
    public User User { get; set; }
}