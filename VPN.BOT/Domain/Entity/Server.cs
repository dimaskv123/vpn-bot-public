﻿namespace Domain.Entity
{
    public record Server
    {
        public int Id { get; set; }
        public string BaseUrl { get; set; }
        public string Country { get; set; }
        public bool IsPremiumServer { get; set; }
        public string Ip { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public ICollection<UserConfig> Configs { get; set; }
        public ICollection<PoolConfig> PoolConfigs { get; set; }
    }
}
