﻿namespace Domain.Entity;

public class ScheduleMessage
{
    public int Id { get; set; }

    public string TextMessage { get; set; }

    public int? ToUserId { get; set; }

    public bool UpdateKeyBoard { get; set; }

    public DateTimeOffset PlannedTimeSend { get; set; }

    public bool IsSent { get; set; }
    
    public User User { get; set; }
}