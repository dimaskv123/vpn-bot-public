﻿namespace Domain.Entity
{
    public class Referal
    {
        public int Id { get; set; }
        public string InviteCode { get; set; }
        public int? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
