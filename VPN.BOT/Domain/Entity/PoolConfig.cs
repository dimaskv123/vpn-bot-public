﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entity;

public class PoolConfig
{
    public int Id { get; set; }
    public string AccessUrl { get; set; }
    public string OutlineUserId { get; set; }
    public DateTimeOffset CreateDate { get; set; }
    
    public Server Server { get; set; }
    public int ServerId { get; set; }
    public UserConfig? UserConfig { get; set; }
    public int? UserConfigId { get; set; }
}