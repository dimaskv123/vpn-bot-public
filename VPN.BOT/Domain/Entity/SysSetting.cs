﻿using VPN.BOT.Common.Enums;

namespace Domain.Entity
{
    public class SysSetting
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
        public string Value { get; set; }
        public SysSettingValueType ValueType { get; set; }
    }
}
