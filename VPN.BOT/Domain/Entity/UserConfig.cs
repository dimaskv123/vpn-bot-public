﻿namespace Domain.Entity
{
    public record UserConfig
    {
        public int Id { get; set; }
        public string VpnKey { get; set; }
        public string OutlineUserId { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int ServerId { get; set; }
        public Server Server { get; set; }
        public PoolConfig? PoolConfig { get; set; }
        public bool IsValid { get; set; }
        public double TotalGigabytes { get; set; }
        public double TodayGigabytes { get; set; }
        public double DataDifference { get; set; }
        public double LastKeyTotalGigabytes { get; set; }
        public ICollection<RestrictionsHistory> RestrictionsHistories { get; set; }
        public ICollection<Payment> Payments { get; set; }
    }
}