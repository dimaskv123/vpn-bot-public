﻿using VPN.BOT.Common.Enums;

namespace Domain.Entity;

public class Payment
{
    public int Id { get; set; }
    public string YookassaPaymentId { get; set; }
    
    public PaymentStatus Status { get; set; }
    
    public int Amount { get; set; }
    public int DurationInDays { get; set; }
    
    public DateTimeOffset? CreatedAt { get; set; }
    
    public DateTimeOffset? UpdatedAt { get; set; }
    
    public int UserConfigId { get; set; }
    
    public UserConfig UserConfig { get; set; }

    public PaymentTo PaymentTo {get; set; }
}