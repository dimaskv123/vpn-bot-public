﻿using VPN.BOT.Common.Enums;

namespace Domain.Entity;

public class UserSettings
{
    public int UserId { get; set; }
    public bool VIP { get; set; }
    public SubscribeTheoryMethod SubscribeTheoryMethod { get; set; }
    public string SubscribeTheoryMethodError { get; set; }
    public double PersonalTodayLimit { get; set; }
    public User User { get; set; }
}