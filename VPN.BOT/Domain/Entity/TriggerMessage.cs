﻿using VPN.BOT.Common.Enums;

namespace Domain.Entity;

public class TriggerMessage
{
    public int Id { get; set; }
    public TriggerType TriggerType { get; set; }
    public int UserId { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public User User { get; set; }
}