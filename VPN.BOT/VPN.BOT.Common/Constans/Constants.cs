﻿namespace VPN.BOT.Common.Constants
{
    public class Constants
    {
        public static string Host { get; set; }

        public static string InviteUrl = "https://s3.amazonaws.com/outline-vpn/invite.html#";

        public static long[] AdminIds = new long[] { 669363145, 254724042, 6261544653 };

        public static class Price
        {
            public const int Price30Days = 189;
            public const int Price90Days = 499;
        }
        
        public static class LimitConstants
        {
            public const int TODAY_GIGABYTES = 1;
            public const int TOTAL_GIGABYTES = 12;
            public const int DEFAULT_FREE_VPN_DAYS = 14;
            public const double MIN_PERSONAL_LIMIT_GIGABYTES = 0.2;
            public const double REDUCE_STEP_GIGABYTES = 0.2;
        }
    }
}
