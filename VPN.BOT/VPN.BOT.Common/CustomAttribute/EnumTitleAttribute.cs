﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VPN.BOT.Common.CustomAttribute
{
    public class EnumTitleAttribute : Attribute
    {
        public string Title { get; private set; }

        public EnumTitleAttribute(string title)
        {
            Title = title;
        }
    }
}
