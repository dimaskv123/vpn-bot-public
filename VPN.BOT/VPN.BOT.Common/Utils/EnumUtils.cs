﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VPN.BOT.Common.CustomAttribute;
using VPN.BOT.Common.Enums;

namespace VPN.BOT.Common.Utils
{
    public static class EnumUtils
    {
        public static TAttribute GetAttribute<TAttribute>(this Enum value) where TAttribute : Attribute
        {
            var enumType = value.GetType();
            var name = Enum.GetName(enumType, value);
            return enumType.GetField(name).GetCustomAttributes(false).OfType<TAttribute>().SingleOrDefault();
        }

        public static string GetTitle(this SysSettingName sysSettingName)
        {
            return sysSettingName.GetAttribute<EnumTitleAttribute>().Title;
        }
    }
}
