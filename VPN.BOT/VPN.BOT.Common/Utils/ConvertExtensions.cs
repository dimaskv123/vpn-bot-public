﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VPN.BOT.Common.Utils
{
    public static class ConvertExtensions
    {
        public static double ToMegabytes(this double gigabytes)
        {
            return gigabytes * 1_000;
        }
    }
}
