using System.Collections.Concurrent;

namespace VPN.BOT.Common.Utils;

// https://codereview.stackexchange.com/questions/275685/c-lock-on-string-value

public static class  UserLock
{
    private static readonly ConcurrentDictionary<string, SemaphoreSlim> SemaphoreDictionary = new ConcurrentDictionary<string, SemaphoreSlim>();

    public static SemaphoreSlim AsyncLock(string key)
    {
        // Используем ConcurrentDictionary для безопасного доступа к объектам блокировки
        return SemaphoreDictionary.GetOrAdd(key, _ => new SemaphoreSlim(1, 1));
    }
}