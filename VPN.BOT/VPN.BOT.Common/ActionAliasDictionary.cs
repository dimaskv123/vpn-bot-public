﻿using Action = VPN.BOT.Common.Enums.Action;

namespace VPN.BOT.Common;

public class ActionAliasDictionary : Dictionary<string, Action>
{
    public static ActionAliasDictionary Instance;

    static ActionAliasDictionary()
    {
        Instance = new ActionAliasDictionary()
        {
            {"/start", Action.Start},
            {"Старт", Action.Start},
            {"/help", Action.Help},
            {"Помощь 🚑", Action.Help},
            {"/profile", Action.Profile},
            {"Профиль 😎", Action.Profile},
            {"/getvpn", Action.GetVpn},
            {"Получить VPN 🔑", Action.GetVpn},
            {"Заменить ключ VPN 🔑", Action.UpdateVpn},
            {"/update", Action.UpdateVpn},
            {"/referral", Action.Referral},
            {"Реферальная программа 🤝", Action.Referral},
            {"Политика использования 📱", Action.Rules},
            {"/buyVpn", Action.BuyVpn},
            {"/tst", Action.Test},
           // {"/admin", Action.Admin},
            {"/menu", Action.Menu },
            {"/unBan", Action.Unban },
            {"/changeServer", Action.ChangeServer },
            {"/setEmail", Action.SetEmail },
            {"/reshuffleUsers", Action.ReshuffleUsers }
        };
    }
}