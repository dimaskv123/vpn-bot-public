﻿namespace VPN.BOT.Common;

public static class MessagesText
{
    public static string WelcomeMessage =

        @"Добро пожаловать в одну из самых быстрых и безопасных VPN, основанную на доверенном протоколе Shadowsocks!

Наш VPN запускает приложения и социальные сети на самых высоких скоростях.

Чтобы начать:

Нажмите кнопку ""Получить VPN"" 

Следите за новостями сервиса в нашем телеграм-канале:
https://t.me/krot\_vpn

Вся информация о подписке в разделе ""Профиль"" и справка с помощью команды /help";

    public static string CertifiacateReadyMessage =

        @"Ваш ключ для подключения готов (ключ отправлен сообщением выше): 

Для подключения:

Нажмите кнопку ""Подключиться""

Часто задаваемые вопросы и справка с доступны помощью команды /help

";

    public static string PaymentComplete =
        @"*Оплата прошла успешно!
Выдан безлимитный доступ к VPN до {0}*" + Environment.NewLine + CertifiacateReadyMessage;

    public static string PaymentCompleteRepeat =
        @"*Оплата прошла успешно!
Текущий ключ доступа к VPN продлен до {0}*";

    public static string SellMessage =
        @"
*Выберите тип подписки:*

Если Вам нужен чек, то установите электронную почту.
Команда - /setEmail.";
    public static string CertifiacateUpdatedMessage =

    @"Ваш ключ для подключения обновлен.
Срок действия ключа остался прежним.
Обновление ключа возможно 1 раз в сутки.

Для подключения:

Нажмите кнопку ""Подключиться""

Часто задаваемые вопросы и справка с доступны помощью команды /help
";

    public static string CertifiacateAlreadyGrandedMessage =

        @"У Вас уже есть активный ключ для доступа к VPN. 
Подробно посмотреть информацию можно в профиле.

Команда: /profile
";

    public static string UserAlreadyPayedOnGetVpnMessage =
        "Спасибо, что вы были с нами и пользовались VPN от KROT NETWORK" +
        Environment.NewLine +
        Environment.NewLine +
        "*Чтобы продолжить пользоваться VPN, вы можете обновить вашу подписку на KROT NETWORK PRO c помощью команды /buyVpn*" +
        Environment.NewLine +
        Environment.NewLine +
        "KROT NETWORK PRO - это безлимитный VPN трафик, один ключ для подключения и никаких ограничений" +
        Environment.NewLine +
        Environment.NewLine +
        "Присоединяйтесь к нам снова и наслаждайтесь самым быстрым VPN по цене чашки кофе!";

    public static string ProfileMessage =
        @"Профиль

Имя 😍: {0} 
Ключ 🔑: {1}";
    public static string EmailInstruction = @"*Для установки электронной почты используйте команду* ✉️ - /setEmail";
    public static string ReferralMessage =
        @"*Реферальная программа* KROT VPN

Если вам нравится сервис KROT VPN вы можете посоветовать его своим друзьям!

Для этого поделитесь персональной ссылкой из сообщения выше 👆🏻

Приведено друзей:
*{0}*

Говорят, если приводить своих друзей в KROT VPN, то можно получить приятные бонусы, но об этом позже
";

    public static string HelpMessage =
        @"Что делать если не удается подключиться?

1. Убедитесь, что Ваш ключ активен и на нем нет блокировки - для этого нажмите в боте кнопку ""Профиль"";   

2. Нажмите в Outline на ключе кнопку ""Не показывать"", перезапустите приложение, вставьте ключ снова - попробуйте подключиться снова.

Чаще всего эти действия помогают решить проблемы с подключением, если проблемы все равно остались, то

*Если ничего из этого Вам не помогло*, напишите в наш аккаунт поддержки!

*ВАЖНО ❗️❗️❗️❗️❗️❗️*
*Пожалуйста, вместе с описанием Вашей проблемы присылайте Ваш ключ! (его можно скопировать нажав на кнопку ""Профиль"" в боте)*

По ссылке наш аккаунт поддержки - 
https://t.me/krot\_vpn\_support
";

    public static string HelpVipMessage =
        @"Что делать если не удается подключиться?

1. Убедитесь, что Ваш ключ активен - для этого нажмите в боте кнопку ""Профиль"".

2. Нажмите в боте кнопку ""Заменить ключ VPN"".

3. Нажмите в Outline на СТАРОМ ключе кнопку ""Не показывать"", перезапустите приложение, вставьте НОВЫЙ ключ - попробуйте подключиться.

*Если ничего из этого Вам не помогло*, напишите в наш аккаунт поддержки!

*ВАЖНО ❗️❗️❗️❗️❗️❗️*
*Пожалуйста, вместе с описанием Вашей проблемы присылайте Ваш ключ! (его можно скопировать нажав на кнопку ""Профиль"" в боте)*

По ссылке наш аккаунт поддержки - 
https://t.me/krot\_vpn\_support

Оставить отзыв о работе сервиса можно тут - 
https://t.me/krot\_vpn/11
";

    public static string RevokeSertificate =
        @"Срок действия вашего ключа истек. 
Вы можете получить новый нажав на кнопку *""Получить ВПН""*

Оставить отзыв о KROT VPN в нашей группе 👇👇👇
https://t.me/krot\_vpn/11
";

    public static string RevokePayedCertificate =
        @"Срок действия ключа истек.

Для того чтобы получить новый ключ с безлимитным доступом используйте команду /buyVpn
";

    public static string PaymentMessage =
        @"*Доступ к VPN на {0}*
К оплате {1} рублей
Для оплаты нажммите кнопку ""Оплатить в Юкасса""
Ссылка на оплату будет активна 2 часа! 
Для генерации новой ссылки отправьте команду /buyVpn";

    public static string LimitByTotalGigabytesMessage =
        "*Достигнут лимит трафика!*"
        + Environment.NewLine
        + Environment.NewLine
        + "Вы израсходовали *{0}* ГБ трафика"
        + Environment.NewLine
        + "Доступ к VPN ограничен до {1}";

    public static string LimitByTodayGigabytesMessage =
        "*Достигнут лимит трафика!*"
        + Environment.NewLine
        + Environment.NewLine
        + "Вы израсходовали *{0} МБ* трафика за сегодня"
        + Environment.NewLine
        + "Доступ к VPN ограничен до 03:00 МСК";

    public static string OfferMessage =
        "*Перейдите на KROT NETWORK PRO чтобы использовать VPN без ограничений*"
        + Environment.NewLine
        + Environment.NewLine
        + "KROT NETWORK PRO это безлимитный VPN трафик"
        + Environment.NewLine
        + "Один ключ для подключения - не нужно обновлять каждые 2 недели"
        + Environment.NewLine
        + "Безлимитный доступ к самому быстрому VPN по цене чашки кофе!";

    public static string UnBlockByTodayGigabitsMessage =
        "*Доступ к ВПН восстановлен!*" +
        Environment.NewLine +
        Environment.NewLine +
        "*Перейдите на KROT NETWORK PRO чтобы использовать VPN без ограничений с помощью команды /buyVpn* ";

    public static string LimitByTotalGigabitsRepeatNotifyMessage =
        "Достигнут лимит трафика!"
        + Environment.NewLine
        + "Доступ к VPN ограничен до {0}"
        + Environment.NewLine
        + "Перейдите на KROT NETWORK PRO чтобы использовать VPN без ограничений с помошью команды /buyVpn"
        + Environment.NewLine
        + "Безлимитный доступ к самому быстрому VPN по цене чашки кофе!";

    public static string OnNoConfigMessage =
        @"Кажется, вы так и не начали использование KROT NETWORK

Для того чтобы начать:

Нажмите кнопку ""Получить VPN""

Если у вы столкнулись с проблемами при настройке, обратитесь в поддержку

https://t.me/krot\_vpn\_support
";
    public static string OnUncompletePayment =
        @"Процесс оплаты не завершен 🥲

Ссылка на оплату действует в течение 2-х часов

Для того чтобы получить новую ссылку введите команду /buyVpn

Если у вы столкнулись с проблемами при оплате, обратитесь в поддержку

https://t.me/krot\_vpn\_support";

    public static string NoDataUsage =
        @"Мы заметили, что вы так и не начали использование KROT NETWORK

Если у вы столкнулись с проблемами при настройке следуйте инструкции ниже

1. Скачайте приложение Outline

2. Скопировать ключ из сообщения выше 

3. Нажать ""Подключиться""

Часто задаваемые вопросы и справка доступны с помощью команды /help

Или обратитесь в поддержку

https://t.me/krot\_vpn\_support";

    public static string ConfigRetake =
        @"Мы заметили, что вы не получили новый ключ доступа"
        + Environment.NewLine + Environment.NewLine
        + @"Нажмите кнопку *""Получить VPN""* чтобы продолжить использовать KROT NETWORK";

    public static string PaymentExpireIn3Days =
        @"Ваша подписка *KROT NETWORK PRO* истекает через 3 дня"
        + Environment.NewLine + Environment.NewLine
        + "Продлите подписку прямо сейчас чтобы избежать отключение ключа"
        + Environment.NewLine + Environment.NewLine
        + "Для того чтобы продлить подписку воспользуйтесь командой /buyVpn или нажмите на кнопку ниже";

    public static string PaymentExpireIn1Day =
        @"Ваша подписка *KROT NETWORK PRO* истекает *{0}*"
        + Environment.NewLine + Environment.NewLine
        + "Продлите подписку прямо сейчас чтобы избежать отключение ключа"
        + Environment.NewLine + Environment.NewLine
        + "Для того чтобы продлить подписку воспользуйтесь командой /buyVpn или нажмите на кнопку ниже ";


    public static string RulesMessage = @"
Политика использования VPN

Мы не одобряем загрузку больших объемов трафика через VPN, так как это может замедлить интернет-соединение для других пользователей.
Пожалуйста, не качайте через VPN фильмы, игры и другие объемные файлы!
*Если мы обнаружим, что пользователь злоупотребляет нашими услугами, мы ограничим его доступ.*

_Желаем вам только положительных эмоций при использовании KROT VPN!_
";

    public static string InstructionAfterKeyUpdate = @"Ваш ключ был обновлен администратором!
- Нажмите в Outline """"Не показывать"""" на всех добавленных ключах
- Перезагрузите телефон
- Вставьте новый ключ";
    public static string TransitMessage = $@"Важное сообщение!{Environment.NewLine}В связи с переездом на другой сервер мы отправляем Вам новый ключ, он будет в сообщении ниже. Срок работы ключа не изменился.";
    public static string tst = "sts";
}