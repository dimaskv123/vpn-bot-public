﻿namespace VPN.BOT.Common.Enums;

public enum SubscribeTheoryMethod
{
    NoSubscribeTheoryMethod = 0,
    LimitByTotalGigabytes = 1,
    LimitByTodayGigabytes = 2,
    HasActiveSubscription = 99,
    Error = 100,
    UnbanError = 101
}