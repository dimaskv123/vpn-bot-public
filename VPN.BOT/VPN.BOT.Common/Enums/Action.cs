﻿namespace VPN.BOT.Common.Enums;

public enum Action
{
    Start,
    GetVpn,
    UpdateVpn,
    Profile,
    Help,
    Referral,
    Test,
    BuyVpn,
    ProcessPayment,
    Rules,
    Admin,
    Menu,
    Unban,
    ChangeServer,
    SetEmail,
    ReshuffleUsers,
    Unknown
}