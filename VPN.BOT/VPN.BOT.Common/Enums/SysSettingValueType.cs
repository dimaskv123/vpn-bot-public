﻿namespace VPN.BOT.Common.Enums
{
    public enum SysSettingValueType
    {
        None = 0,
        Int = 1, 
        String = 2,
        Bool = 3,
        Date = 4
    }
}
