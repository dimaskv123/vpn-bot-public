﻿namespace VPN.BOT.Common.Enums;

public enum TriggerType
{
    NoTrigger = 0,
    NoUserConfig = 1,
    UnCompletePayment = 2,
    ConfigRetake = 3,
    NoDataUsage = 4,
    PaymentExpireIn3Day = 5,
    PaymentExpireIn1Day = 6,
}