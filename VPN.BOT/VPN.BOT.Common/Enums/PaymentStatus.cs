﻿namespace VPN.BOT.Common.Enums;

public enum PaymentStatus
{
    Pending,
    WaitingForCapture,
    Succeeded,
    Canceled
}