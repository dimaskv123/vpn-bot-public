﻿namespace VPN.BOT.Common.Enums
{
    public enum MessageName
    {
        LimitByToday,
        LimitByTotal,
        Help,
        NoUserConfigTrigger,
        UnCompletePayment,
        NoDataUsage,
        ConfigRetake,
        PaymentExpireIn3Days,
        PaymentExpireIn1Day,
        CertificateExpire,
        PayedCertificateExpire,
        Start,
        TransitKeyFromEndServer,
        GetVpn
    }
}
