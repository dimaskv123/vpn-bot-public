﻿using VPN.BOT.Common.CustomAttribute;

namespace VPN.BOT.Common.Enums
{
    public enum SysSettingName
    {
        [EnumTitle("LastCalcDataUpdateDate")]
        LastCalcDataUpdateDate,
        [EnumTitle("TransitExpiredServerKeys")]
        TransitExpiredServerKeys,
        [EnumTitle("DefaultHttpTimeOut")]
        DefaultHttpTimeOut,
        [EnumTitle("ReshuffleKeysCount")]
        ReshuffleKeysCount,
        [EnumTitle("LimitPersonalTraffic")]
        LimitPersonalTraffic
    }
}
