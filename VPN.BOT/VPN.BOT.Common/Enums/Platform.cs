﻿namespace VPN.BOT.Common.Enums;

public enum Platform
{
    IOS,
    Android,
    Windows,
    MacOS,
    Other
}