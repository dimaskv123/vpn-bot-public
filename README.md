Добавить миграцию 

``dotnet ef migrations add --project DAL --startup-project VPN.BOT --context  DataContext <Имя миграции>
``

Запуск в Docker 

```docker-compose up -d --no-deps server```

Настройка нового VPN сервиса

```
sudo apt update
sudo apt upgrade

sudo apt install curl

sudo bash -c "$(wget -qO- https://raw.githubusercontent.com/Jigsaw-Code/outline-server/master/src/server_manager/install_scripts/install_server.sh)"r
```
